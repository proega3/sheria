<?php
/*
Plugin Name: Barristar Core
Plugin URI: http://themeforest.net/user/wpoceans
Description: Plugin to contain shortcodes and custom post types of the barristar theme.
Author: wpoceans
Author URI: http://themeforest.net/user/wpoceans/portfolio
Version: 1.0
Text Domain: barristar-core
*/

if( ! function_exists( 'barristar_block_direct_access' ) ) {
	function barristar_block_direct_access() {
		if( ! defined( 'ABSPATH' ) ) {
			exit( 'Forbidden' );
		}
	}
}

// Plugin URL
define( 'BARRISTAR_PLUGIN_URL', plugins_url( '/', __FILE__ ) );

// Plugin PATH
define( 'BARRISTAR_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'BARRISTAR_PLUGIN_ASTS', BARRISTAR_PLUGIN_URL . 'assets' );
define( 'BARRISTAR_PLUGIN_IMGS', BARRISTAR_PLUGIN_ASTS . '/images' );
define( 'BARRISTAR_PLUGIN_INC', BARRISTAR_PLUGIN_PATH . 'include' );

// DIRECTORY SEPARATOR
define ( 'DS' , DIRECTORY_SEPARATOR );

// Edefy Elementor Shortcode Path
define( 'BARRISTAR_EM_SHORTCODE_BASE_PATH', BARRISTAR_PLUGIN_PATH . 'elementor/' );
define( 'BARRISTAR_EM_SHORTCODE_PATH', BARRISTAR_EM_SHORTCODE_BASE_PATH . 'widgets/' );

/**
 * Check if Codestar Framework is Active or Not!
 */
function barristar_framework_active() {
  return ( defined( 'CS_VERSION' ) ) ? true : false;
}

/* BARRISTAR_THEME_NAME_PLUGIN */
define('BARRISTAR_THEME_NAME_PLUGIN', 'Barristar');

// Initial File
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('barristar-core/barristar-core.php')) {

	// Custom Post Type
  require_once( BARRISTAR_PLUGIN_INC . '/custom-post-type.php' );

  if ( is_plugin_active('kingcomposer/kingcomposer.php') ) {

    define( 'BARRISTAR_KC_SHORTCODE_BASE_PATH', BARRISTAR_PLUGIN_PATH . 'kc/' );
    define( 'BARRISTAR_KC_SHORTCODE_PATH', BARRISTAR_KC_SHORTCODE_BASE_PATH . 'shortcodes/' );
    // Shortcodes
    require_once( BARRISTAR_KC_SHORTCODE_BASE_PATH . '/kc-setup.php' );
    require_once( BARRISTAR_KC_SHORTCODE_BASE_PATH . '/library.php' );
  }

  // Theme Custom Shortcode
  require_once( BARRISTAR_PLUGIN_INC . '/custom-shortcodes/theme-shortcodes.php' );
  require_once( BARRISTAR_PLUGIN_INC . '/custom-shortcodes/custom-shortcodes.php' );

  // Importer
  require_once( BARRISTAR_PLUGIN_INC . '/demo/importer.php' );

  if (class_exists('WP_Widget') && is_plugin_active('codestar-framework/cs-framework.php') ) {
    // Widgets
    require_once( BARRISTAR_PLUGIN_INC . '/widgets/nav-widget.php' );
    require_once( BARRISTAR_PLUGIN_INC . '/widgets/recent-posts.php' );
    require_once( BARRISTAR_PLUGIN_INC . '/widgets/footer-posts.php' );
    require_once( BARRISTAR_PLUGIN_INC . '/widgets/text-widget.php' );
    require_once( BARRISTAR_PLUGIN_INC . '/widgets/widget-extra-fields.php' );
    
    // Elementor
    if(file_exists( BARRISTAR_EM_SHORTCODE_BASE_PATH . '/em-setup.php' ) ){
      require_once( BARRISTAR_EM_SHORTCODE_BASE_PATH . '/em-setup.php' );
      require_once( BARRISTAR_EM_SHORTCODE_BASE_PATH . 'lib/fields/icons.php' );
    }

  }

  add_action('wp_enqueue_scripts', 'regulation_plugin_enqueue_scripts');
  function regulation_plugin_enqueue_scripts() {
    wp_enqueue_script('plugin-scripts', BARRISTAR_PLUGIN_ASTS.'/plugin-scripts.js', array('jquery'), '', true);
  }

}

// Extra functions
require_once( BARRISTAR_PLUGIN_INC . '/theme-functions.php' );