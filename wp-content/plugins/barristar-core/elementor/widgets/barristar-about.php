<?php
/*
 * Elementor Barristar About Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_About extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_about';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'About', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-address-card';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar About widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_about'];
	}
	
	/**
	 * Register Barristar About widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_about',
			[
				'label' => esc_html__( 'About Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'about_style',
			[
				'label' => esc_html__( 'About Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your about style.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'about_image',
			[
				'label' => esc_html__( 'About Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'description' => esc_html__( 'Set your image.', 'barristar-core'),
			]
		);
		$this->add_control(
			'about_subtitle',
			[
				'label' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type sub title text here', 'barristar-core' ),
				'label_block' => true,
				'condition' => [
					'about_style' => array('style-two'),
				 ],
			]
		);
		$this->add_control(
			'about_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'about_content',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'default' => esc_html__( 'your content text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'barristar-core' ),
				'type' => Controls_Manager::WYSIWYG,
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn_text',
			[
				'label' => esc_html__( 'Button/Link Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Button Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type btn text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn_link',
			[
				'label' => esc_html__( 'Button Link', 'barristar-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$this->add_control(
			'video_link',
			[
				'label' => esc_html__( 'Video Link', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Video Link', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type video link here', 'barristar-core' ),
				'label_block' => true,
				'condition' => [
					'about_style' => array('style-two'),
				 ],
			]
		);
		$this->add_control(
			'signature_image',
			[
				'label' => esc_html__( 'Signature Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'condition' => [
					'about_style' => array('style-one'),
				 ],
				'description' => esc_html__( 'Set your signature image.', 'barristar-core'),
			]
		);
		$this->end_controls_section();// end: Section
		

		// Section
		$this->start_controls_section(
			'section_section_style',
			[
				'label' => esc_html__( 'Section', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'border_color',
			[
				'label' => esc_html__( 'Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-title .img-holder:before' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		//Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'about_style' => array('style-two'),
				 ],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-section .about-text span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => esc_html__( 'Sub Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-section .about-text h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'section_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-section .about-text p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .about-section .about-text p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// button
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'btn_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .btn-style a.about-btn' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'btn_bg',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => ['classic','gradient'],
				'selector' => '{{WRAPPER}} .about-section .btn-style a.about-btn',
			]
		);
		$this->end_controls_section();// end: Section

		// Video button
		$this->start_controls_section(
			'section_video_style',
			[
				'label' => esc_html__( 'Video Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'about_style' => array('style-two'),
				 ],
			]
		);
		$this->add_control(
			'video_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-title .social-1st i' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'video_border_color',
			[
				'label' => esc_html__( 'Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-title .social-1st i' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'video_btn_bg',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => ['classic', 'gradient'],
				'selector' => '{{WRAPPER}} .about-title .social-1st i',
			]
		);
		$this->end_controls_section();// end: Section

	}

	/**
	 * Render About widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();

		$about_style = !empty( $settings['about_style'] ) ? $settings['about_style'] : '';
		$about_subtitle = !empty( $settings['about_subtitle'] ) ? $settings['about_subtitle'] : '';
		$about_title = !empty( $settings['about_title'] ) ? $settings['about_title'] : '';
		$about_content = !empty( $settings['about_content'] ) ? $settings['about_content'] : '';
		$video_link = !empty( $settings['video_link'] ) ? $settings['video_link'] : '';
		$bg_image = !empty( $settings['about_image']['id'] ) ? $settings['about_image']['id'] : '';	
		$signature_image = !empty( $settings['signature_image']['id'] ) ? $settings['signature_image']['id'] : '';	

		// Image
		$image_url = wp_get_attachment_url( $bg_image );
		$image_alt = get_post_meta( $settings['about_image']['id'], '_wp_attachment_image_alt', true);
		// Image
		$signature_url = wp_get_attachment_url( $signature_image );
		$signature_alt = get_post_meta( $settings['signature_image']['id'], '_wp_attachment_image_alt', true);

		$btn_text = !empty( $settings['btn_text'] ) ? $settings['btn_text'] : '';

		$btn_link = !empty( $settings['btn_link']['url'] ) ? $settings['btn_link']['url'] : '';
		$btn_external = !empty( $settings['btn_link']['is_external'] ) ? 'target="_blank"' : '';
		$btn_nofollow = !empty( $settings['btn_link']['nofollow'] ) ? 'rel="nofollow"' : '';
		$btn_link_attr = !empty( $btn_link ) ?  $btn_external.' '.$btn_nofollow : '';


		if ( $about_style === 'style-one') {
			$section_class = ' about-area about-area2';
		} else {
			$section_class = ' about-area';
		}
		
		$button = $btn_link ? '<a href="'.esc_url($btn_link).'" '.esc_attr( $btn_link_attr ).' class="about-btn" >'.esc_html( $btn_text ).'</a>' : '';

		// Turn output buffer on
		ob_start(); ?>
		<div class="about-section <?php echo esc_attr( $section_class ); ?>">
			<div class="container">
	    	<div class="row">
	      <?php if ( $about_style == 'style-one'  ) { ?>
	       <div class="col col-lg-6 col-md-6">
	            <div class="about-title">
	                <div class="img-holder">
	                   <?php  if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_url( $image_alt ).'">'; } ?>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-6 col-md-6">
	            <div class="about-text">
	            	<?php 
	              	if( $about_title ) { echo '<h2>'.esc_html( $about_title ).'</h2>'; }
	               	if( $about_content ) { echo wp_kses_post( $about_content ); } 
	             	?>
	              <div class="btns">
	                  <div class="btn-style">
	                  	<?php echo $button; ?>
	                  </div>
	              </div>
	                <div class="signature">
	                 <?php  if( $signature_url ) { echo '<img src="'.esc_url( $signature_url ).'" alt="'.esc_url( $signature_alt ).'">'; } ?>
	                </div>
	            </div>
	        </div>
	     <?php } else { ?>
	      <div class="col-lg-6">
	          <div class="about-text title">
	          		<?php 
	              	if( $about_subtitle ) { echo '<span>'.esc_html( $about_subtitle ).'</span>'; }
	              	if( $about_title ) { echo '<h2>'.esc_html( $about_title ).'</h2>'; }
	               	if( $about_content ) { echo wp_kses_post( $about_content ); } 
	             	?>
	              <div class="btns-2">
	                  <div class="btn-style">
	                  	<?php echo $button; ?>
	                  </div>
	              </div>
	          </div>
	      </div>
	      <div class="col col-lg-6">
	          <div class="about-title">
	              <div class="img-holder">
	                  <div class="overlay">
	                       <?php  if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_url( $image_alt ).'">'; } ?>
	                      <div class="social-1st">
	                      	<?php if ( $video_link ) { ?>
	                      		<ul>
	                              <li>
	                                <a href="<?php echo esc_url( $video_link ); ?>" class="video-btn" data-type="iframe">
	                                  <i class="fa fa-play"></i>
	                                </a>
	                              </li>
	                          </ul>
	                      	<?php } ?>
	                      </div>
	                  </div>
	              </div>
	          </div>
	      </div>
	      <?php } ?>
	    	</div>
    	</div>
  	</div>
		<?php // Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render About widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_About() );