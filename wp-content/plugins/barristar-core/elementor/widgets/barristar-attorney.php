<?php
/*
 * Elementor Barristar Attorney Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Attorney extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_attorney';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Attorney', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-users';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Attorney widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_attorney'];
	}
	
	/**
	 * Register Barristar Attorney widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){

		$posts = get_posts( 'post_type="attorney"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'barristar' ) ] = 0;
    }
		
			
		$this->start_controls_section(
			'section_attorney',
			[
				'label' => __( 'Attorney Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'attorney_style',
			[
				'label' => __( 'Attorney Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One(Carousel)', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two(Grid)', 'barristar-core' ),
				],
				'default' => 'style-two',
				'description' => esc_html__( 'Select your attorney style.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'attorney_column',
			[
				'label' => __( 'Columns', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'condition' => [
					'attorney_style' => 'style-two',
				],
				'frontend_available' => true,
				'options' => [
					'col-2' => esc_html__( 'Column Two', 'barristar-core' ),
					'col-3' => esc_html__( 'Column Three', 'barristar-core' ),
				],
				'default' => 'col-3',
				'description' => esc_html__( 'Select your attorney column.', 'barristar-core' ),
			]
		);		
		$this->add_control(
			'carousel_nav',
			[
				'label' => esc_html__( 'Next Preview', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'condition' => [
					'attorney_style' => 'style-one',
				],
				'default' => 'true',
			]
		);
		$this->add_control(
			'carousel_items',
			[
				'label' => esc_html__( 'Attorney Limit', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'condition' => [
					'attorney_style' => 'style-one',
				],
				'description' => esc_html__( 'Enter the number of items to show.', 'barristar-core' ),
			]
		);
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_attorney_listing',
			[
				'label' => esc_html__( 'Listing Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'attorney_limit',
			[
				'label' => esc_html__( 'Attorney Limit', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'attorney_order',
			[
				'label' => __( 'Order', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'barristar-core' ),
					'DESC' => esc_html__( 'Desending', 'barristar-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'attorney_orderby',
			[
				'label' => __( 'Order By', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'barristar-core' ),
					'ID' => esc_html__( 'ID', 'barristar-core' ),
					'author' => esc_html__( 'Author', 'barristar-core' ),
					'title' => esc_html__( 'Title', 'barristar-core' ),
					'date' => esc_html__( 'Date', 'barristar-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'attorney_show_category',
			[
				'label' => __( 'Certain Categories?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'attorney_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .expert-content h3 a' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .expert-content h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Meta
		$this->start_controls_section(
			'section_meta_style',
			[
				'label' => esc_html__( 'Subtitle', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_meta_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .expert-content span',
			]
		);
		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .expert-content span' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'meta_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .expert-content span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Button
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'attorney_style' => array('style-one'),
					],
			]
		);
	
		$this->add_control(
			'button_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .expert-active.owl-theme .owl-controls .owl-nav i' => 'color: {{VALUE}};'
				],
			]
		);

		$this->add_control(
			'button_bgcolor',
			[
				'label' => esc_html__( 'Background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .expert-active.owl-theme .owl-controls .owl-nav div' => 'background-color: {{VALUE}};'
				],
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .expert-content ul li a' => 'color: {{VALUE}};'
				],
			]
		);
		
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Attorney widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();

		$attorney_style = !empty( $settings['attorney_style'] ) ? $settings['attorney_style'] : '';
		$attorney_column = !empty( $settings['attorney_column'] ) ? $settings['attorney_column'] : '';

		$attorney_limit = !empty( $settings['attorney_limit'] ) ? $settings['attorney_limit'] : '';

		$carousel_items = !empty( $settings['carousel_items'] ) ? $settings['carousel_items'] : '';
		$carousel_nav  = ( isset( $settings['carousel_nav'] ) && ( 'true' == $settings['carousel_nav'] ) ) ? true : false;

		$attorney_order = !empty( $settings['attorney_order'] ) ? $settings['attorney_order'] : '';
		$attorney_orderby = !empty( $settings['attorney_orderby'] ) ? $settings['attorney_orderby'] : '';
		$attorney_show_category = !empty( $settings['attorney_show_category'] ) ? $settings['attorney_show_category'] : [];
		$attorney_show_id = !empty( $settings['attorney_show_id'] ) ? $settings['attorney_show_id'] : [];


		$carousel_nav = $carousel_nav ? ' data-nav="true"' : ' data-nav="false"';
    $carousel_items = $carousel_items ? ' data-items="'.esc_attr( $carousel_items ).'"' : ' data-items="3"';

		 if ( $attorney_style === 'style-one' ) {
		  	$attorney_class = 'expert-active owl-carousel';
		  	$attorney_content = 'attorney-content';
		  } else {
		  	$attorney_class = 'expert-sub';
		  	$attorney_content = '';
		  }


		  if ( $attorney_column == 'col-2' ) {
				$attorney_col = ' col-md-6 col-sm-6';
			} else {
				$attorney_col = ' col-md-4 col-sm-6 ';
			}
		// Turn output buffer on
		ob_start();


		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($attorney_show_id) {
			$attorney_show_id = json_encode( $attorney_show_id );
			$attorney_show_id = str_replace(array( '[', ']' ), '', $attorney_show_id);
			$attorney_show_id = str_replace(array( '"', '"' ), '', $attorney_show_id);
      $attorney_show_id = explode(',',$attorney_show_id);
    } else {
      $attorney_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'attorney',
		  'posts_per_page' => (int)$attorney_limit,
		  'category_name' => implode(',', $attorney_show_category),
		  'orderby' => $attorney_orderby,
		  'order' => $attorney_order,
      'post__in' => $attorney_show_id,
		);

		$barristar_post = new \WP_Query( $args ); ?>

			<div class="expert-area">
		   <div class="<?php echo esc_attr( $attorney_class ); ?>" <?php if ( $attorney_style == 'style-one' ) {
        echo  $carousel_items.' '.$carousel_nav;
       } ?>>
		   <?php  if ( $attorney_style == 'style-two' ) { ?>
		    <div class="expert-item">
		      <div class="row">
			   <?php } ?>
			   <?php if ($barristar_post->have_posts()) : while ($barristar_post->have_posts()) : $barristar_post->the_post();
					$attorney_options = get_post_meta( get_the_ID(), 'attorney_options', true );
		      $subtitle = isset($attorney_options['subtitle']) ? $attorney_options['subtitle'] : '';
		      $attorney_socials = isset($attorney_options['attorney_socials']) ? $attorney_options['attorney_socials'] : '';
		      $attorney_image = isset($attorney_options['attorney_image']) ? $attorney_options['attorney_image'] : '';
		      global $post;
		      $image_url = wp_get_attachment_url( $attorney_image );
		      $image_alt = get_post_meta( $attorney_image, '_wp_attachment_image_alt', true);

		      ?>
		       <div class="<?php echo esc_attr( $attorney_content ); if ( $attorney_style == 'style-two' ) { echo $attorney_col; }  ?>">
		         <div class="expert-single">
		            <div class="expert-img">
		                 <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
		            </div>
		            <div class="expert-content text-center">
		               <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo the_title(); ?></a></h3>
		               <span><?php echo esc_html( $subtitle ); ?></span>
		               <ul class="d-flex">
		               	<?php   if ( $attorney_socials ) {
		                foreach ( $attorney_socials as $key => $attorney_social ) {
		                  echo'<li><a href="'.esc_url( $attorney_social['link'] ).'"><i class="'.esc_attr( $attorney_social['icon']  ).'" aria-hidden="true"></i></a></li>';
		                } } ?>
		               </ul>
		            </div>
		         </div>
		      </div>
				 <?php endwhile;
				  endif;
				  wp_reset_postdata();
				   if ( $attorney_style == 'two' ) { ?>
		      </div>
		      </div>
		     <?php } ?>
		  </div>
		</div>

		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Attorney widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Attorney() );