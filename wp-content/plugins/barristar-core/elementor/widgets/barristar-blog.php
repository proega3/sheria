<?php
/*
 * Elementor Barristar Blog Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Blog extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_blog';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Blog', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Blog widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_blog'];
	}
	
	/**
	 * Register Barristar Blog widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){

		$posts = get_posts( 'post_type="post"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'barristar' ) ] = 0;
    }
		
			
		$this->start_controls_section(
			'section_blog',
			[
				'label' => __( 'Blog Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'blog_style',
			[
				'label' => __( 'Blog Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One(List)', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two(Grid)', 'barristar-core' ),
				],
				'default' => 'style-two',
				'description' => esc_html__( 'Select your blog style.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'blog_column',
			[
				'label' => __( 'Columns', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'condition' => [
					'blog_style' => 'style-two',
				],
				'frontend_available' => true,
				'options' => [
					'col-2' => esc_html__( 'Column Two', 'barristar-core' ),
					'col-3' => esc_html__( 'Column Three', 'barristar-core' ),
				],
				'default' => 'col-3',
				'description' => esc_html__( 'Select your blog column.', 'barristar-core' ),
			]
		);		
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_blog_metas',
			[
				'label' => esc_html__( 'Meta\'s Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'blog_image',
			[
				'label' => esc_html__( 'Image', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_date',
			[
				'label' => esc_html__( 'Date', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_author',
			[
				'label' => esc_html__( 'Author', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_comments',
			[
				'label' => esc_html__( 'Comments', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_tags',
			[
				'label' => esc_html__( 'Tags', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
				'condition' => [
						'blog_style' => array('style-two'),
					],
			]
		);
		$this->add_control(
			'read_more_txt',
			[
				'label' => esc_html__( 'Read More Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Type your Read More text here', 'barristar-core' ),
				'condition' => [
					'blog_style' => array('style-one'),
				],
			]
		);
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_blog_listing',
			[
				'label' => esc_html__( 'Listing Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'blog_limit',
			[
				'label' => esc_html__( 'Blog Limit', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'blog_order',
			[
				'label' => __( 'Order', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'barristar-core' ),
					'DESC' => esc_html__( 'Desending', 'barristar-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'blog_orderby',
			[
				'label' => __( 'Order By', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'barristar-core' ),
					'ID' => esc_html__( 'ID', 'barristar-core' ),
					'author' => esc_html__( 'Author', 'barristar-core' ),
					'title' => esc_html__( 'Title', 'barristar-core' ),
					'date' => esc_html__( 'Date', 'barristar-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'blog_show_category',
			[
				'label' => __( 'Certain Categories?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'blog_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->add_control(
			'short_content',
			[
				'label' => esc_html__( 'Excerpt Length', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'step' => 1,
				'default' => 55,
				'description' => esc_html__( 'How many words you want in short content paragraph.', 'barristar-core' ),
				'condition' => [
						'blog_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'blog_pagination',
			[
				'label' => esc_html__( 'Pagination', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-content h3 a,' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .blog-content h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Meta
		$this->start_controls_section(
			'section_meta_style',
			[
				'label' => esc_html__( 'Meta', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_meta_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .blog-area.blog-shortcode .blog-item .post-meta li,.blog-area.blog-shortcode .blog-item .post-meta li a,.blog-single-section .post .meta li, .blog-single-section .post .meta li a,.blog-single-section .post .meta li i',
			]
		);
		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .blog-area.blog-shortcode .blog-item .post-meta li,.blog-area.blog-shortcode .blog-item .post-meta li a,.blog-single-section .post .meta li, .blog-single-section .post .meta li a,.blog-single-section .post .meta li i' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'meta_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}}  .blog-area.blog-shortcode .blog-item .post-meta li,.blog-single-section .post .meta li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Button
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'blog_style' => array('style-one'),
					],
			]
		);
	
		$this->add_control(
			'button_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} blog-single-section a.blog-btn' => 'color: {{VALUE}};'
				],
			]
		);
		
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Blog widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();

		$blog_style = !empty( $settings['blog_style'] ) ? $settings['blog_style'] : '';
		$blog_column = !empty( $settings['blog_column'] ) ? $settings['blog_column'] : '';
		$read_more_txt = !empty( $settings['read_more_txt'] ) ? $settings['read_more_txt'] : '';

		$blog_limit = !empty( $settings['blog_limit'] ) ? $settings['blog_limit'] : '';
		$blog_image  = ( isset( $settings['blog_image'] ) && ( 'true' == $settings['blog_image'] ) ) ? true : false;
		$blog_date  = ( isset( $settings['blog_date'] ) && ( 'true' == $settings['blog_date'] ) ) ? true : false;
		$blog_author  = ( isset( $settings['blog_author'] ) && ( 'true' == $settings['blog_author'] ) ) ? true : false;
		$blog_comments  = ( isset( $settings['blog_comments'] ) && ( 'true' == $settings['blog_comments'] ) ) ? true : false;
		$blog_tags  = ( isset( $settings['blog_tags'] ) && ( 'true' == $settings['blog_tags'] ) ) ? true : false;

		$blog_order = !empty( $settings['blog_order'] ) ? $settings['blog_order'] : '';
		$blog_orderby = !empty( $settings['blog_orderby'] ) ? $settings['blog_orderby'] : '';
		$blog_show_category = !empty( $settings['blog_show_category'] ) ? $settings['blog_show_category'] : [];
		$blog_show_id = !empty( $settings['blog_show_id'] ) ? $settings['blog_show_id'] : [];
		$short_content = !empty( $settings['short_content'] ) ? $settings['short_content'] : '';
		$blog_pagination  = ( isset( $settings['blog_pagination'] ) && ( 'true' == $settings['blog_pagination'] ) ) ? true : false;
		
	
		$excerpt_length = $short_content ? $short_content : '55';
		$read_more_txt = $read_more_txt ? $read_more_txt : esc_html__( 'Read More', 'barristar-core' );

		 if ( $blog_style === 'style-one' ) {
		  	$post_wrapper = 'blog-single-section shortcode-list-blog';
		  	$post_content = 'blog-content';
		  } else {
		  	$post_wrapper = 'blog-area blog-shortcode';
		  	$post_content = 'container';
		  }


		  if ( $blog_column == 'col-2' ) {
				$blog_col = 'col-md-6 col-sm-6';
			} else {
				$blog_col = 'col-md-4 col-sm-6 ';
			}
		// Turn output buffer on
		ob_start();


		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($blog_show_id) {
			$blog_show_id = json_encode( $blog_show_id );
			$blog_show_id = str_replace(array( '[', ']' ), '', $blog_show_id);
			$blog_show_id = str_replace(array( '"', '"' ), '', $blog_show_id);
      $blog_show_id = explode(',',$blog_show_id);
    } else {
      $blog_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'post',
		  'posts_per_page' => (int)$blog_limit,
		  'category_name' => implode(',', $blog_show_category),
		  'orderby' => $blog_orderby,
		  'order' => $blog_order,
      'post__in' => $blog_show_id,
		);

		$barristar_post = new \WP_Query( $args ); ?>

			<div class="<?php echo esc_attr( $post_wrapper ); ?>">
				<div class="<?php echo esc_attr( $post_content ); ?>">
				<div class="row">
	       <?php 
				  if ($barristar_post->have_posts()) : while ($barristar_post->have_posts()) : $barristar_post->the_post();
				  $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
				  $large_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()) , '_wp_attachment_image_alt', true); 
				  $large_image = $large_image[0];  

				  $post_options = get_post_meta( get_the_ID(), 'post_options', true );
					$grid_image = isset( $post_options['grid_image'] ) ? $post_options['grid_image'] : '';
					$image_url = wp_get_attachment_url( $grid_image );
          $image_alt = get_post_meta( $grid_image , '_wp_attachment_image_alt', true); 


				  if ( $blog_style === 'style-one' ) {
				  	$featured_img = $large_image;
				  	$featured_alt = $large_alt;
				  } else {
						$featured_img = $image_url;
						$featured_alt = $image_alt;
				  }

				  if ( $blog_style === 'style-one' ) { ?>
		        <div class="post format-standard-image">
		        		<?php if ($large_image && $blog_image) { ?>
			            <div class="entry-media">
			                <img src="<?php echo esc_url($featured_img); ?>" alt="<?php echo esc_attr( $featured_alt); ?>">
			            </div>
	           		<?php } ?>
		            <ul  class="meta">
                	<?php if ( $blog_date ) { ?><li><i class="ti-time"></i><?php echo get_the_date('d M, Y'); ?></li><?php } 
                 	
                 	 if ( $blog_tags ) {
			               $last_tag = get_the_tags();
											if ( !empty( $last_tag ) ) {
											  $last_tag = end( $last_tag );
											  echo '<li><i class="ti-book"></i> '.esc_html( $last_tag->name ).'</li>';
										}
		              }
			            if (  $blog_comments ) { ?>
									  <li>
											 <a class="barristar-comment" href="<?php echo esc_url( get_comments_link() ); ?>">
											 	<i class="ti-comment-alt"></i>
						              <?php printf( esc_html( _nx( 'Comment (%1$s)', 'Comments (%1$s)', get_comments_number(), 'comments title', 'aequity' ) ), '<span class="comment">'.number_format_i18n( get_comments_number() ).'</span>','<span>' . get_the_title() . '</span>' ); ?>
						          </a>
			              </li>
								 <?php	} ?>
                </ul>
		           <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h3>
		            <p><?php echo wp_trim_words( get_the_content(), $excerpt_length ); ?></p>
		            <a href="<?php echo esc_url( get_permalink() ); ?>" class="blog-btn"><?php echo esc_html( $read_more_txt ); ?></a>
		        </div>
	       	<?php } else { ?>
	       	<div class="<?php echo esc_attr( $blog_col ); ?>">
	            <div class="blog-item b-0">
	            		<?php if ($grid_image && $blog_image) { ?>
							    <div class="blog-img">
							    	 <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt); ?>">
							    </div>
							    <?php } ?>
							    <div class="blog-content">
							        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_title(); ?></a></h3>
							        <ul class="post-meta">
							        		<li><?php if ( $blog_author ) { echo get_avatar( get_the_author_meta( 'ID' ), 125 ); } ?></li>
							        		<li>
							           <?php
							                if ( $blog_author ) {
							             		printf( esc_html__(' By:','barristar') .' <a href="%1$s" rel="author">%2$s</a>',
							                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
							                get_the_author()
							             ); 
							            } ?>
							           </li>
							          <?php 
							       		 if ( $blog_date ) { ?>
							          	<li><?php echo get_the_date('M d Y'); ?></li>
									      <?php } ?>
							        </ul>
							    </div>
							</div>
	        </div>
	       	<?php } 
				  endwhile;
				  endif;
				  wp_reset_postdata();
					if ($blog_pagination) { ?>
					  <div class="page-pagination-wrap">
					  <?php 	echo '<div class="paginations">';
							$big = 999999999;
							echo paginate_links( array(
                'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                'format'    => '?paged=%#%',
                'total'     => $barristar_post->max_num_pages,
                'show_all'  => false,
                'current'   => max( 1, $my_page ),
								'prev_text'    => '<div class="fi flaticon-back"></div>',
								'next_text'    => '<div class="fi flaticon-next"></div>',
                'mid_size'  => 1,
                'type'      => 'list'
              ) );
	        	echo '</div>'; ?>
					  </div>
					<?php } ?>
		   </div>
		   </div>
		</div>

		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Blog widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Blog() );