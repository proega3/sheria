<?php
/*
 * Elementor Barristar Contact Form 7 Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Contact_Form extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_contact_form';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Contact Form', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-wpforms';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Contact Form widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	/*
	public function get_script_depends() {
		return ['wpo-barristar_contact_form'];
	}
	 */
	
	/**
	 * Register Barristar Contact Form widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_contact_form',
			[
				'label' => esc_html__( 'Form Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'contact_style',
			[
				'label' => esc_html__( 'Contact Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your contact style.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'form_subtitle',
			[
				'label' => esc_html__( 'Sub Title', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Default sub title', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type your sub title here', 'barristar-core' ),
				'condition' => [
					'contact_style' => array('style-one'),
				 ],
			]
		);
		$this->add_control(
			'form_title',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Default title', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type your title here', 'barristar-core' ),
			]
		);
		$this->add_control(
			'form_content',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Default content', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'barristar-core' ),
				'condition' => [
					'contact_style' => array('style-one'),
				 ],
			]
		);
		
		$this->add_control(
			'form_id',
			[
				'label' => esc_html__( 'Select contact form', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => Controls_Helper_Output::get_posts('wpcf7_contact_form'),
			]
		);
		$this->end_controls_section();// end: Section

		// Title Style

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact .contact-text .title h2,.contact.contact-page-area h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact .contact-text .title h2,.contact.contact-page-area h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_pad',
			[
				'label' => __( 'Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact .contact-text .title h2,.contact.contact-page-area h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Sub Title Style

		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'contact_style' => array('style-one'),
				 ],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'barristar_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-text .date',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-text .date' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_pad',
			[
				'label' => __( 'Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-text .date' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		//Content Style

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'contact_style' => array('style-one'),
				 ],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact .contact-text p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact .contact-text p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_pad',
			[
				'label' => __( 'Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact .contact-text p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		$this->start_controls_section(
			'section_form_style',
			[
				'label' => esc_html__( 'Form', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-area .contact-form input[type="text"], 
				{{WRAPPER}} .contact-area .contact-form input[type="email"], 
				{{WRAPPER}} .contact-area .contact-form input[type="date"], 
				{{WRAPPER}} .contact-area .contact-form input[type="time"], 
				{{WRAPPER}} .contact-area .contact-form input[type="number"], 
				{{WRAPPER}} .contact-area .contact-form textarea, 
				{{WRAPPER}} .contact-area .contact-form select, 
				{{WRAPPER}} .contact-area .contact-form .form-control, 
				{{WRAPPER}} .contact-area .contact-form .nice-select',
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'form_border',
				'label' => esc_html__( 'Border', 'barristar-core' ),
				'selector' => '{{WRAPPER}} .contact-area .contact-form input[type="text"], 
				{{WRAPPER}} .contact-area .contact-form input[type="email"], 
				{{WRAPPER}} .contact-area .contact-forminput[type="date"], 
				{{WRAPPER}} .contact-area .contact-form input[type="time"], 
				{{WRAPPER}} .contact-area .contact-form input[type="number"], 
				{{WRAPPER}} .contact-area .contact-form textarea, 
				{{WRAPPER}} .contact-area .contact-form select, 
				{{WRAPPER}} .contact-area .contact-form .form-control, 
				{{WRAPPER}} .contact-area .contact-form .nice-select',
			]
		);
		$this->add_control(
			'placeholder_text_color',
			[
				'label' => __( 'Placeholder Text Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form input:not([type="submit"])::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form input:not([type="submit"])::-moz-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form input:not([type="submit"])::-ms-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form input:not([type="submit"])::-o-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form textarea::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form textarea::-moz-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form textarea::-ms-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-area .contact-form textarea::-o-placeholder' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'label_color',
			[
				'label' => __( 'Label Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form label' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form input[type="text"], 
					{{WRAPPER}} .contact-area .contact-form input[type="email"], 
					{{WRAPPER}} .contact-area .contact-form input[type="date"], 
					{{WRAPPER}} .contact-area .contact-form input[type="time"], 
					{{WRAPPER}} .contact-area .contact-form input[type="number"], 
					{{WRAPPER}} .contact-area .contact-form textarea, 
					{{WRAPPER}} .contact-area .contact-form select, 
					{{WRAPPER}} .contact-area .contact-form .form-control, 
					{{WRAPPER}} .contact-area .contact-form .nice-select' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'input_border_color',
			[
				'label' => __( 'Input Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form input[type="text"], 
					{{WRAPPER}} .contact-area .contact-form input[type="email"], 
					{{WRAPPER}} .contact-area .contact-form input[type="date"], 
					{{WRAPPER}} .contact-area .contact-form input[type="time"], 
					{{WRAPPER}} .contact-area .contact-form input[type="number"], 
					{{WRAPPER}} .contact-area .contact-form textarea, 
					{{WRAPPER}} .contact-area .contact-form select, 
					{{WRAPPER}} .contact-area .contact-form .form-control, 
					{{WRAPPER}} .contact-area .contact-form .nice-select' => 'border-color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'input_bg_color',
			[
				'label' => __( 'Input Background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form input[type="text"], 
					{{WRAPPER}} .contact-area .contact-form input[type="email"], 
					{{WRAPPER}} .contact-area .contact-form input[type="date"], 
					{{WRAPPER}} .contact-area .contact-form input[type="time"], 
					{{WRAPPER}} .contact-area .contact-form input[type="number"], 
					{{WRAPPER}} .contact-area .contact-form textarea, 
					{{WRAPPER}} .contact-area .contact-form select, 
					{{WRAPPER}} .contact-area .contact-form .form-control, 
					{{WRAPPER}} .contact-area .contact-form .nice-select' => 'background-color: {{VALUE}} !important;',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit',
			]
		);
		$this->add_responsive_control(
			'btn_width',
			[
				'label' => esc_html__( 'Width', 'barristar-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
				],
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit' => 'min-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'btn_margin',
			[
				'label' => __( 'Margin', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_border_radius',
			[
				'label' => __( 'Border Radius', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs( 'button_style' );
			$this->start_controls_tab(
				'button_normal',
				[
					'label' => esc_html__( 'Normal', 'barristar-core' ),
				]
			);
			$this->add_control(
				'button_color',
				[
					'label' => esc_html__( 'Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_border',
					'label' => esc_html__( 'Border', 'barristar-core' ),
					'selector' => '{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit',
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_hover',
				[
					'label' => esc_html__( 'Hover', 'barristar-core' ),
				]
			);
			$this->add_control(
				'button_hover_color',
				[
					'label' => esc_html__( 'Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit:hover' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_hover_color',
				[
					'label' => esc_html__( 'Background Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_hover_border',
					'label' => esc_html__( 'Border', 'barristar-core' ),
					'selector' => '{{WRAPPER}} .contact-area .contact-form .wpcf7-form-control.wpcf7-submit:hover',
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render Contact Form widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$contact_style = !empty( $settings['contact_style'] ) ? $settings['contact_style'] : '';
		$form_id = !empty( $settings['form_id'] ) ? $settings['form_id'] : '';
		$form_title = !empty( $settings['form_title'] ) ? $settings['form_title'] : '';
		$form_subtitle = !empty( $settings['form_subtitle'] ) ? $settings['form_subtitle'] : '';
		$form_content = !empty( $settings['form_content'] ) ? $settings['form_content'] : '';

		// Turn output buffer on
		ob_start();

		if ( $contact_style == 'style-one' ) { ?>
			<div class="contact contact-area">
			  <div class="container">
			    <div class="row">
			        <div class="col-lg-5 col-md-12">
			            <div class="contact-text">
			               <div class="title">
			                   <span></span>
			                   <?php if( $form_subtitle ) { echo '<h2>'.esc_html( $form_subtitle ).'</h2>'; } ?>
			               </div>
			               <span class="date"><?php if( $form_title ) { echo esc_html( $form_title ); } ?></span>
			                <?php if( $form_content ) { echo '<p>'.esc_html( $form_content ).'</p>'; } ?>
			            </div>
			        </div>
			        <div class="col col-lg-7 col-md-12 col-sm-12">
			            <div class="contact-content">
			                <div class="contact-form">
			                   <?php echo do_shortcode( '[contact-form-7 id="'. $form_id .'"]' ); ?>  
			                </div>
			            </div>
			        </div>
			    </div>
			  </div>
			</div>
		<?php } else { ?>
			<div class="contact contact-page-area">
			    <div class="contact-area contact-area-2 contact-area-3">
			      <?php if( $form_title ) { echo '<h2>'.esc_html( $form_title ).'</h2>'; } ?>
			      <div class="contact-form">
			       <?php echo do_shortcode( '[contact-form-7 id="'. $form_id .'"]' ); ?>  
			      </div>
			    </div>
			</div>
		<?php } 

		// Return outbut buffer
		echo ob_get_clean();
		
		}
		


	/**
	 * Render Contact Form widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Contact_Form() );