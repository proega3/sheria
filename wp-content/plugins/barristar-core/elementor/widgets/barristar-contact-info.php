<?php
/*
 * Elementor Barristar ContactInfo Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_ContactInfo extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_contactinfo';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Contact Info', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-telegram';
	}
	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar ContactInfo widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_contactinfo'];
	}
	
	/**
	 * Register Barristar ContactInfo widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_contactinfo',
			[
				'label' => esc_html__( 'ContactInfo Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'section_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'section_desc',
			[
				'label' => esc_html__( 'Section Desctription Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'desctription Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type desctription text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'barristar-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'eicon' => esc_html__('Elegant Icon', 'barristar-core'),
                'ticon' => esc_html__('Themify Icon', 'barristar-core'),
                'flaticon' => esc_html__('Flaticon', 'barristar-core'),
                'slicon' => esc_html__('Simple Line Icon', 'barristar-core'),
            ],
            'default' => 'eicon',
        ]
    );
    $repeater->add_control(
          'eicon',
          [
              'label' => esc_html__( 'Elegant Icon', 'barristar-core' ),
              'type' => Controls_Manager::ICON,
              'options' => barristar_elegant_icons(),
              'include' => barristar_include_elegant_icons(),
              'default' => 'arrow_right',
              'condition' => [
                  'icon_type' => 'eicon'
              ]
          ]
      );
   $repeater->add_control(
          'slicon',
          [
              'label'     => esc_html__( 'Simple Line Icon', 'barristar-core' ),
              'type'      => Controls_Manager::ICON,
              'options'   => barristar_simple_line_icons(),
              'include'   => barristar_include_simple_line_icons(),
              'default'   => 'icon-volume-2',
              'condition' => [
                  'icon_type' => 'slicon'
              ]
          ]
      );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'barristar-core' ),
            'type' => Controls_Manager::ICON,
            'options' => barristar_themify_icons(),
            'include' => barristar_include_themify_icons(),
            'default' => 'et-map',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'barristar-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => barristar_flaticons(),
            'include'    => barristar_include_flaticons(),
            'default'    => 'flaticon-mortarboard',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'contact_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'contact_address',
			[
				'label' => esc_html__( 'Contact Address', 'barristar-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Contact Info Address', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type contact Address here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'contactinfoItems_groups',
			[
				'label' => esc_html__( 'Contact Info Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'contact_title' => esc_html__( 'Contact Info', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ contact_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-page-item .single-info .info-con i' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-page-item .single-info h3',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-page-item .single-info h3' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-page-item .single-info h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-page-item .single-info p, .contact-page-item .single-info span',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-page-item .single-info p, .contact-page-item .single-info span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-page-item .single-info p, .contact-page-item .single-info span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render ContactInfo widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$contactinfoItems_groups = !empty( $settings['contactinfoItems_groups'] ) ? $settings['contactinfoItems_groups'] : [];
		$section_title = !empty( $settings['section_title'] ) ? $settings['section_title'] : '';
		$section_desc = !empty( $settings['section_desc'] ) ? $settings['section_desc'] : '';
		
		// Turn output buffer on
		ob_start();
		?>
		<div class="contact-page-area">
				<div class="contact-page-item">
           <?php 
            	if( $section_title ) { echo '<h2>'.esc_html( $section_title ).'</h2>'; }
            	if( $section_desc ) { echo '<P>'.esc_html( $section_desc ).'</P>'; }

            	// Group Param Output
							if( is_array( $contactinfoItems_groups ) && !empty( $contactinfoItems_groups ) ){
							foreach ( $contactinfoItems_groups as $each_items ) { 

							$contact_icon = !empty( $each_items['contact_icon'] ) ? $each_items['contact_icon'] : '';
							$contact_title = !empty( $each_items['contact_title'] ) ? $each_items['contact_title'] : '';
							$contact_address = !empty( $each_items['contact_address'] ) ? $each_items['contact_address'] : '';

							switch ($each_items['icon_type']) {
							    case 'ticon':
							        $icon = !empty($each_items['ticon']) ? $each_items['ticon'] : '';
							        break;
							    case 'slicon':
							        $icon = !empty($each_items['slicon']) ? $each_items['slicon'] : '';
							        break;
							    case 'eicon':
							        $icon = !empty($each_items['eicon']) ? $each_items['eicon'] : '';
							        break;
							    case 'flaticon':
							        $icon = !empty($each_items['flaticon']) ? $each_items['flaticon'] : '';
							        break;
							}

							$icon_class = (!empty($icon)) ? "class='fi $icon'" : '';

						?>
              <div class="single-info">
                <div class="info-con">
                  <?php if( $icon_class ) { echo '<i '.$icon_class.'></i>'; }  ?>
                </div>
               <div class="phone">
               	  <?php 
               	  	if( $contact_title ) { echo '<h3>'.esc_html( $contact_title ).'</h3>'; }
               	  	if( $contact_address ) { echo wp_kses_post( $contact_address ); }
               	   ?>
               </div>
             </div>
						<?php }
						} ?>
	     </div>
	  </div>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render ContactInfo widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_ContactInfo() );