<?php
/*
 * Elementor Barristar Feature Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Feature extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_feature';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Feature', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-file-signature';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Feature widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_feature'];
	}
	
	/**
	 * Register Barristar Feature widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_feature',
			[
				'label' => esc_html__( 'Feature Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'feature_style',
			[
				'label' => esc_html__( 'Feature Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your feature style.', 'barristar-core' ),
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'barristar-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'barristar-core'),
                'flaticon' => esc_html__('Flaticon', 'barristar-core'),
            ],
            'default' => 'flaticon',
        ]
    );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'barristar-core' ),
            'type' => Controls_Manager::ICON,
            'options' => barristar_themify_icons(),
            'include' => barristar_include_themify_icons(),
            'default' => 'ti-panel',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'barristar-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => barristar_flaticons(),
            'include'    => barristar_include_flaticons(),
            'default'    => 'flaticon-lawyer',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'feature_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'feature_content',
			[
				'label' => esc_html__( 'Content Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Content Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type content text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'featureItems_groups',
			[
				'label' => esc_html__( 'Feature Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'feature_title' => esc_html__( 'Feature', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ feature_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// feature item
		$this->start_controls_section(
			'section_feature_style',
			[
				'label' => esc_html__( 'feature', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'border_color',
			[
				'label' => esc_html__( 'border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'feature_style' => array('style-two'),
				 ],
				'selectors' => [
					'{{WRAPPER}} .service-area .service-item, .service-area .service-item .service-text' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'feature_background',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => [ 'classic','gradient'],
				'selector' => '{{WRAPPER}} .service-area .service-item',
			]
		);
		$this->end_controls_section();// end: Section

		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_icon_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .service-area .service-icon i:before',
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .service-area .service-icon i:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'icon_background',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => [ 'classic','gradient'],
				'condition' => [
					'feature_style' => array('style-one'),
				 ],
				'selector' => '{{WRAPPER}} .service-area .service-item .icon-c',
			]
		);
		$this->end_controls_section();// end: Section
		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .service-area .service-item .service-text span',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .service-area .service-item .service-text span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .service-area .service-item .service-text span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .service-area .service-item .service-text h3',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .service-area .service-item .service-text h3' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Content  Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .service-area .service-item .service-text h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Feature widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$featureItems_groups = !empty( $settings['featureItems_groups'] ) ? $settings['featureItems_groups'] : [];
		$feature_style = !empty( $settings['feature_style'] ) ? $settings['feature_style'] : '';

		if ( $feature_style === 'style-one') {
			$feature_area = ' service-area2';
			$feature_class = ' icon-c';
			$feature_col = ' col-xs-6 ';
		} else {
			$feature_area = ' service-area3';
			$feature_class = ' icon-b';
			$feature_col = ' col-xs-12 ';
		}
		
		// Turn output buffer on
		ob_start();
		?>
		<div class="service-area <?php echo esc_attr( $feature_area ); ?>">
			<div class="container">
		  <div class="row">
			<?php 	// Group Param Output
				if( is_array( $featureItems_groups ) && !empty( $featureItems_groups ) ){
					foreach ( $featureItems_groups as $each_item ) { 

					$feature_icon = !empty( $each_item['feature_icon'] ) ? $each_item['feature_icon'] : '';
					$feature_title = !empty( $each_item['feature_title'] ) ? $each_item['feature_title'] : '';
					$feature_content = !empty( $each_item['feature_content'] ) ? $each_item['feature_content'] : '';
					$icon_type = !empty( $each_item['icon_type'] ) ? $each_item['icon_type'] : '';

					switch ($each_item['icon_type']) {
		        case 'ticon':
		            $icon = !empty($each_item['ticon']) ? $each_item['ticon'] : '';
		            break;
		        case 'flaticon':
		            $icon = !empty($each_item['flaticon']) ? $each_item['flaticon'] : '';
		            break;
			    }

			    $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';

					?>
		      <div class="col-lg-4 col-md-4 col-sm-6 <?php echo esc_attr( $feature_col ); ?> feature-col">
		          <div class="service-item">
		              <div class="row">
									  <div class="grid">
										  <div class="col-lg-3 col-md-3 col-sm-3 col-3 <?php echo esc_attr( $feature_class ); ?>">
											  <div class="service-icon">
												 <?php if( $icon_class ) { echo '<i '.$icon_class.'></i>'; }  ?>
											  </div>
										  </div>
										  <div class="col-lg-9 col-md-9 col-sm-9 col-9">
											  <div class="service-text">
												   <?php
		                         if( $feature_title ) { echo '<span>'.esc_html( $feature_title ).'</span>'; }
		                         if( $feature_content ) { echo '<h3>'.esc_html( $feature_content ).'</h3>'; }
		                        ?>
											  </div>
										  </div>
									  </div>
		              </div>
		          </div>
		      </div>
  			<?php }
				} ?>
		    </div>
		   </div>
		</div>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Feature widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Feature() );