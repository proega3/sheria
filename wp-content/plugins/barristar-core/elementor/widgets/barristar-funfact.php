<?php
/*
 * Elementor Barristar Funfact Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Funfact extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_funfact';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Funfact', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-sort-numeric-asc';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Funfact widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_funfact'];
	}
	
	/**
	 * Register Barristar Funfact widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_funfact',
			[
				'label' => esc_html__( 'Funfact Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'funfact_style',
			[
				'label' => esc_html__( 'Funfact Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your funfact style.', 'barristar-core' ),
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'funfact_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'funfact_number',
			[
				'label' => esc_html__( 'Funfact Number', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '95', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type funfact Number here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'funfact_plus',
			[
				'label' => esc_html__( 'Funfact Plus/Percentage', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '%', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type funfact Plus/Percentage here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'funfactItems_groups',
			[
				'label' => esc_html__( 'Funfact Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'funfact_title' => esc_html__( 'Funfact', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ funfact_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		
		// Title
		$this->start_controls_section(
			'section_counter_style',
			[
				'label' => esc_html__( 'Counter', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'counter_color',
			[
				'label' => esc_html__( 'Background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-area .counter-grids .grid' => 'background-color: {{VALUE}};',
				],
			]
		);
		
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .counter-area .counter-grids .grid p',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-area .counter-grids .grid p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .counter-area .counter-grids .grid p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Funfact Number
		$this->start_controls_section(
			'funfact_number_style',
			[
				'label' => esc_html__( 'Number', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_number_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .counter-area .counter-grids .grid h2',
			]
		);
		$this->add_control(
			'number_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-area .counter-grids .grid h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'number_padding',
			[
				'label' => __( 'Number Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .counter-area .counter-grids .grid h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		
	}

	/**
	 * Render Funfact widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$funfactItems_groups = !empty( $settings['funfactItems_groups'] ) ? $settings['funfactItems_groups'] : [];

		$section_title = !empty( $settings['section_title'] ) ? $settings['section_title'] : '';
		$section_subtitle = !empty( $settings['section_subtitle'] ) ? $settings['section_subtitle'] : '';
		$funfact_style = !empty( $settings['funfact_style'] ) ? $settings['funfact_style'] : '';

    if ( $funfact_style == 'style-one' ) {
      $wrap_col = '';
      $col_class = 'col-lg-6';
    } else {
      $col_class = 'col-lg-12';
      $wrap_col = ' counter-area2';
    }

		// Turn output buffer on
		ob_start(); ?>
		<div class="counter-area <?php echo esc_attr(  $wrap_col ) ?>">
			<div class="container">
		    <div class="row">
		        <div class="<?php echo esc_attr( $col_class ); ?>">
		            <div class="counter-grids">
		            <?php 	// Group Param Output
									if( is_array( $funfactItems_groups ) && !empty( $funfactItems_groups ) ){
									foreach ( $funfactItems_groups as $each_item ) { 

									$funfact_title = !empty( $each_item['funfact_title'] ) ? $each_item['funfact_title'] : '';
									$funfact_number = !empty( $each_item['funfact_number'] ) ? $each_item['funfact_number'] : '';
									$funfact_plus = !empty( $each_item['funfact_plus'] ) ? $each_item['funfact_plus'] : '';

									?>
		                <div class="grid">
		                    <div>
		                       <?php 
							            		if( $funfact_number ) { echo '<h2><span class="odometer" data-count="'.esc_html( $funfact_number ).'">'.esc_html__( '00','barristar-core' ).'</span>'.esc_html( $funfact_plus ).'</h2>'; } 
							            	?>
		                    </div>
		                    <?php if( $funfact_title ) { echo '<p>'.esc_html( $funfact_title ).'</p>'; }  ?>
		                </div>
		          		<?php }
									} ?>
		            </div>
		        </div>
		    </div>
	    </div>
	  </div>
		<?php 
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Funfact widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Funfact() );