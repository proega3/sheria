<?php
/*
 * Elementor Barristar Pricing Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Pricing extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_pricing';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Pricing', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-money';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Pricing widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_pricing'];
	}
	
	/**
	 * Register Barristar Pricing widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_pricing',
			[
				'label' => esc_html__( 'Pricing Options', 'barristar-core' ),
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'barristar-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'barristar-core'),
                'flaticon' => esc_html__('Flaticon', 'barristar-core'),
            ],
            'default' => 'flaticon',
        ]
    );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'barristar-core' ),
            'type' => Controls_Manager::ICON,
            'options' => barristar_themify_icons(),
            'include' => barristar_include_themify_icons(),
            'default' => 'ti-panel',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'barristar-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => barristar_flaticons(),
            'include'    => barristar_include_flaticons(),
            'default'    => 'flaticon-scale',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'pricing_title',
			[
				'label' => esc_html__( 'Price Title', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Price Title', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type Price title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'pricing_amount',
			[
				'label' => esc_html__( 'Price Amount', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '99', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type Price Amount here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'price_curency',
			[
				'label' => esc_html__( 'Price Curency', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '$', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type Price Curency here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'pricing_items',
			[
				'label' => esc_html__( 'Price Items', 'barristar-core' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => esc_html__( 'Price Items', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type Price Items here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'btn_text',
			[
				'label' => esc_html__( 'Button Text', 'barristar-core' ),
				'default' => esc_html__( 'button text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type button Text here', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'btn_link',
			[
				'label' => esc_html__( 'Button Link', 'barristar-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$this->add_control(
			'pricingItems_groups',
			[
				'label' => esc_html__( 'Pricing Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'pricing_title' => esc_html__( 'Pricing', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ pricing_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Icon Style
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'icon_size',
			[
				'label' => esc_html__( 'Font Size', 'barristar-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 30,
						'max' => 100,
						'step' => 1,
					],
				],
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-icon .fi:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-icon .fi:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'icon_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-icon .fi:before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-icon span',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-icon span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-icon span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Price
		$this->start_controls_section(
			'section_price_style',
			[
				'label' => esc_html__( 'Price', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_price_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text h2',
			]
		);
		$this->add_control(
			'price_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'price_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Curency
		$this->start_controls_section(
			'section_price_curency_style',
			[
				'label' => esc_html__( 'Curency', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_price_curency_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text h2 span.curency',
			]
		);
		$this->add_control(
			'price_curency_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text h2 span.curency' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

	
		// Price item
		$this->start_controls_section(
			'section_price_item_style',
			[
				'label' => esc_html__( 'Price Item', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_price_item_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text p',
			]
		);
		$this->add_control(
			'price_item_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text p' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'price_item_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

 // Price Button
		$this->start_controls_section(
			'section_price_btn_style',
			[
				'label' => esc_html__( 'Price Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_price_btn_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text a.price-btn',
			]
		);
		$this->add_control(
			'price_btn_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text a.price-btn' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'price_btn_bg_color',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => [ 'classic','gradient' ],
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text a.price-btn',
			]
		);
		$this->end_controls_section();// end: Section

	// Price Button Hover
		$this->start_controls_section(
			'section_price_btn_hover_style',
			[
				'label' => esc_html__( 'Price Button Hover', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'price_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .pricing-item .pricing-text a.price-btn:hover' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'price_btn_bg_hover_color',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => [ 'gradient' ],
				'selector' => '{{WRAPPER}} .prising-area .pricing-item .pricing-text a.price-btn:hover',
			]
		);
		$this->end_controls_section();// end: Section

	// Price active
		$this->start_controls_section(
			'section_price_btn_active_style',
			[
				'label' => esc_html__( 'Price Active', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'price_active_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .prising-area .row div:nth-child(2) .pricing-item i:before,.prising-area .row div:nth-child(2) .pricing-item .pricing-icon span,.prising-area .row div:nth-child(2) .pricing-item .pricing-text h2,.prising-area .row div:nth-child(2) .pricing-item .pricing-text p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'price_active_bg_color',
				'label' => esc_html__( 'Background', 'barristar-core' ),
				'types' => [ 'gradient' ],
				'selector' => '{{WRAPPER}} .prising-area .row div:nth-child(2) .pricing-item',
			]
		);
		$this->end_controls_section();// end: Section


		
	}

	/**
	 * Render Pricing widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$pricingItems_groups = !empty( $settings['pricingItems_groups'] ) ? $settings['pricingItems_groups'] : [];

		// Turn output buffer on
		ob_start();
		?>
		<div class="prising-area">
	    <div class="container">
	        <div class="row">
						<?php 	// Group Param Output
							if( is_array( $pricingItems_groups ) && !empty( $pricingItems_groups ) ){
							foreach ( $pricingItems_groups as $each_items ) { 

							$pricing_title = !empty( $each_items['pricing_title'] ) ? $each_items['pricing_title'] : '';
							$pricing_amount = !empty( $each_items['pricing_amount'] ) ? $each_items['pricing_amount'] : '';
							$price_curency = !empty( $each_items['price_curency'] ) ? $each_items['price_curency'] : '';
							$pricing_items = !empty( $each_items['pricing_items'] ) ? $each_items['pricing_items'] : '';

							$button_text = !empty( $each_items['btn_text'] ) ? $each_items['btn_text'] : '';	
							$button_link = !empty( $each_items['btn_link']['url'] ) ? $each_items['btn_link']['url'] : '';
							$button_link_external = !empty( $each_items['btn_link']['is_external'] ) ? 'target="_blank"' : '';
							$button_link_nofollow = !empty( $each_items['btn_link']['nofollow'] ) ? 'rel="nofollow"' : '';
							$button_link_attr = !empty( $button_link ) ?  $button_link_external.' '.$button_link_nofollow : '';

							$pricing_button = $button_link ? '<a href="'.esc_url($button_link).'" '.$button_link_attr.' class="price-btn" >'.esc_html( $button_text ).'</a>' : '';

							
							switch ($each_items['icon_type']) {
					        case 'ticon':
					            $icon = !empty($each_items['ticon']) ? $each_items['ticon'] : '';
					            break;
					        case 'flaticon':
					            $icon = !empty($each_items['flaticon']) ? $each_items['flaticon'] : '';
					            break;
					    }

					    $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';

						?>
	            <div class="col-lg-4 col-md-6 col-sm-12">
	                <div class="pricing-item">
	                    <div class="pricing-icon">
	                      <?php 
	                        if( $icon_class ) { echo '<i '.$icon_class.'></i>'; }  
				            			if( $pricing_title ) { echo '<span>'.esc_html( $pricing_title ).'</span>'; } 
				            		?>
	                    </div>
	                    <div class="pricing-text">
	                        <?php 
					            			if( $pricing_amount ) { echo '<h2><span class="curency">'.esc_html( $price_curency ).'</span>'.esc_html( $pricing_amount ).'</h2>'; } 
					            			if( $pricing_items ) { echo wp_kses_post( $pricing_items ); } 
					            		?>
	                        <div class="btns text-center">
	                            <div class="btn-style">
	                                 <?php echo $pricing_button;  ?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
						<?php }
						} ?>
	        </div>
	    </div>
	</div>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Pricing widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Pricing() );