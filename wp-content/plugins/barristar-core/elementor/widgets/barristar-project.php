<?php
/*
 * Elementor Barristar Project Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Project extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_project';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Project', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Project widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo_barristar_project_filter'];
	}
	
	/**
	 * Register Barristar Project widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){


		$posts = get_posts( 'post_type="project"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'barristar' ) ] = 0;
    }
		
		
		$this->start_controls_section(
			'section_project',
			[
				'label' => esc_html__( 'Project Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'project_style',
			[
				'label' => esc_html__( 'Project Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your project style.', 'barristar-core' ),
			]
		);

		$this->add_control(
			'project_filter',
			[
				'label' => esc_html__( 'Project Filter', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);	
		$this->end_controls_section();// end: Section
		

		$this->start_controls_section(
			'section_project_listing',
			[
				'label' => esc_html__( 'Listing Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'project_limit',
			[
				'label' => esc_html__( 'Project Limit', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'project_order',
			[
				'label' => __( 'Order', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'barristar-core' ),
					'DESC' => esc_html__( 'Desending', 'barristar-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'project_orderby',
			[
				'label' => __( 'Order By', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'barristar-core' ),
					'ID' => esc_html__( 'ID', 'barristar-core' ),
					'author' => esc_html__( 'Author', 'barristar-core' ),
					'title' => esc_html__( 'Title', 'barristar-core' ),
					'date' => esc_html__( 'Date', 'barristar-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'project_show_category',
			[
				'label' => __( 'Certain Categories?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'project_category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'project_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->add_control(
			'short_content',
			[
				'label' => esc_html__( 'Excerpt Length', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'step' => 1,
				'default' => 16,
				'description' => esc_html__( 'How many words you want in short content paragraph.', 'barristar-core' ),
				'condition' => [
						'project_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'all_project',
			[
				'label' => esc_html__( 'All project Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Type your all project text here', 'barristar-core' ),
			]
		);
		
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .case-project .grid .overlay-text .text-inner h3 a',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .case-project .grid .overlay-text .text-inner h3 a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .case-project .grid .overlay-text .text-inner h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Details
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .case-project .grid .overlay-text .text-inner p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .case-project .grid .overlay-text .text-inner p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Overly
		$this->start_controls_section(
			'section_overly_style',
			[
				'label' => esc_html__( 'Overly', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => esc_html__( 'Overly Background', 'barristar-core' ),
				'description' => esc_html__( 'Project Overly Color', 'barristar-core' ),
				'types' => [ 'gradient','classic' ],
				'selector' => '{{WRAPPER}} .case-project .grid .overlay-text',
			]
		);
		$this->add_control(
			'content_border_color',
			[
				'label' => esc_html__( 'Overly Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .studies-item .overlay-text .text-inner' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Navigation
		$this->start_controls_section(
			'section_nav_style',
			[
				'label' => esc_html__( 'Navigation', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'project_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'nav_color',
			[
				'label' => esc_html__( 'Navigation Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .case-filters ul li a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Navigation Hover
		$this->start_controls_section(
			'section_nav_hover_style',
			[
				'label' => esc_html__( 'Navigation Hover', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'project_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'nav_hover_color',
			[
				'label' => esc_html__( 'Navigation Hover Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .case-filters ul li a:hover' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

	}

	/**
	 * Render Project widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$project_style = !empty( $settings['project_style'] ) ? $settings['project_style'] : '';
		$project_column = !empty( $settings['project_column'] ) ? $settings['project_column'] : '';

		$project_limit = !empty( $settings['project_limit'] ) ? $settings['project_limit'] : '';
		$project_order = !empty( $settings['project_order'] ) ? $settings['project_order'] : '';
		$project_orderby = !empty( $settings['project_orderby'] ) ? $settings['project_orderby'] : '';
		$project_show_category = !empty( $settings['project_show_category'] ) ? $settings['project_show_category'] : [];
		$project_show_id = !empty( $settings['project_show_id'] ) ? $settings['project_show_id'] : [];
		$all_project = !empty( $settings['all_project'] ) ? $settings['all_project'] : '';

		$project_filter  = ( isset( $settings['project_filter'] ) && ( 'true' == $settings['project_filter'] ) ) ? true : false;
	
		$all_project = $all_project ? $all_project : esc_html__( 'All Project', 'barristar-core' );

		// Turn output buffer on
		ob_start();

		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($project_show_id) {
			$project_show_id = json_encode( $project_show_id );
			$project_show_id = str_replace(array( '[', ']' ), '', $project_show_id);
			$project_show_id = str_replace(array( '"', '"' ), '', $project_show_id);
      $project_show_id = explode(',',$project_show_id);
    } else {
      $project_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'project',
		  'posts_per_page' => (int)$project_limit,
		  'category_name' => implode(',', $project_show_category),
		  'orderby' => $project_orderby,
		  'order' => $project_order,
      'post__in' => $project_show_id,
		);

		$barristar_project = new \WP_Query( $args );

		if ( $project_style === 'style-one' ) {
			$project_grid_wrap = 'col-c';
			$project_container = 'container-fluid';
		} else {
			$project_grid_wrap = 'col-lg-4 col-md-6 col-sm-6';
			$project_container = 'container';
		}

		if ( $barristar_project->have_posts() ) :
     $terms = get_terms( 'project_category' ); ?>
     <div class="case-project">
       <div class="row">
       <div class="col col-xs-12 sortable-gallery">
         <div class="case-filters case-menu">
            <?php if ( $project_filter ) { ?>
              <ul>
                  <li><a data-filter="*" href="#" class="current"><?php echo esc_html( $all_project ) ?></a></li>
                   <?php foreach( $terms as $term ):  ?>
                     <li><a data-filter=".<?php echo esc_attr( $term->slug ); ?>" href="#"><?php echo esc_html(  $term->name ); ?></a></li>
                   <?php  endforeach; ?>
              </ul>
              <?php } ?>
          </div>
        <div class="case-grids gallery-container clearfix">
	 			<?php 
					while ( $barristar_project->have_posts()) : $barristar_project->the_post();
					
					$project_options = get_post_meta( get_the_ID(), 'project_options', true );
          $project_subtitle = isset( $project_options['project_subtitle']) ? $project_options['project_subtitle'] : '';
          $project_title = isset( $project_options['project_title']) ? $project_options['project_title'] : '';
          $project_image = isset( $project_options['project_image']) ? $project_options['project_image'] : '';

          global $post;
          $image_url = wp_get_attachment_url( $project_image );
          $image_alt = get_post_meta( $project_image , '_wp_attachment_image_alt', true);

          $terms = wp_get_post_terms( get_the_ID(), 'project_category');

          foreach ($terms as $term) {
            $cat_class = $term->slug;
          }
          $count = count($terms);
          $i=0;
          $cat_class = '';
          if ($count > 0) {
            foreach ($terms as $term) {
              $i++;
              $cat_class .= $term->slug .' ';
              if ($count != $i) {
                $cat_class .= '';
              } else {
                $cat_class .= '';
              }
            }
          }

					?>
          <div class="grid <?php echo esc_attr( $project_grid_wrap.' '.$cat_class ); ?>">
               <div class="studies-item">
                  <div class="studies-single">
                     <?php if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; } ?>
                  </div>
                  <div class="overlay-text">
                      <div class="text-inner">
                          <?php 
					            			if( $project_subtitle ) { echo '<p class="sub">'.esc_html( $project_subtitle ).'</p>'; }
					            			if( $project_title ) { echo '<h3><a href="'.esc_url( get_permalink() ).'">'.esc_html( $project_title ).'</a></h3>'; }
					            		?>
                      </div>
                  </div>  
              </div>
          </div>
		       <?php endwhile;
          	wp_reset_postdata(); ?>
		    		</div>
	        </div>
	      </div>
	    </div>
			<?php endif;
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Project widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Project() );