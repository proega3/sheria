<?php
/*
 * Elementor Dustar Service Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Dustar_Service extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_service';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Service', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-telegram';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Dustar Service widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_service'];
	}
	
	/**
	 * Register Dustar Service widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){


		$posts = get_posts( 'post_type="service"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'barristar' ) ] = 0;
    }
		
		
		$this->start_controls_section(
			'section_service',
			[
				'label' => esc_html__( 'Service Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'service_style',
			[
				'label' => esc_html__( 'Service Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
					'style-three' => esc_html__( 'Style Three', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your service style.', 'barristar-core' ),
			]
		);
		$this->end_controls_section();// end: Section
		

		$this->start_controls_section(
			'section_service_listing',
			[
				'label' => esc_html__( 'Listing Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'service_limit',
			[
				'label' => esc_html__( 'Service Limit', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'barristar-core' ),
			]
		);
		$this->add_control(
			'service_order',
			[
				'label' => __( 'Order', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'barristar-core' ),
					'DESC' => esc_html__( 'Desending', 'barristar-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'service_orderby',
			[
				'label' => __( 'Order By', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'barristar-core' ),
					'ID' => esc_html__( 'ID', 'barristar-core' ),
					'author' => esc_html__( 'Author', 'barristar-core' ),
					'title' => esc_html__( 'Title', 'barristar-core' ),
					'date' => esc_html__( 'Date', 'barristar-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'service_show_category',
			[
				'label' => __( 'Certain Categories?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'service_category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'service_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'barristar-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->add_control(
			'short_content',
			[
				'label' => esc_html__( 'Excerpt Length', 'barristar-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'step' => 1,
				'default' => 16,
				'description' => esc_html__( 'How many words you want in short content paragraph.', 'barristar-core' ),
			]
		);
		
		$this->end_controls_section();// end: Section

		$this->start_controls_section(
			'section_btn',
			[
				'label' => esc_html__( 'Service Read More', 'barristar-core' ),
			]
		);
		$this->add_control(
			'service_more',
			[
				'label' => esc_html__( 'Read More On/Off', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'barristar-core' ),
				'label_off' => esc_html__( 'Hide', 'barristar-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'service_btn_text',
			[
				'label' => esc_html__( 'Read More', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Read More', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type Read More Text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->end_controls_section();// end: Section


		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'icon_size',
			[
				'label' => esc_html__( 'Font Size', 'barristar-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 100,
						'step' => 1,
					],
				],
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-icon i:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-icon i:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'icon_border_color',
			[
				'label' => esc_html__( 'Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-icon,.practice-area.practice-area2 .service-item' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .practice-area .service-text:before,.practice-area.practice-area2 .service-icon:after' => 'background-color: {{VALUE}};'
				],
			]
		);
		
		$this->end_controls_section();// end: Section
		
		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .practice-area .service-item .service-text h3 a',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-text h3 a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-text h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Conent
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .practice-area .service-item .service-text p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-text p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => esc_html__( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .practice-area .service-item .service-text p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Read More
		$this->start_controls_section(
			'section_readmore_style',
			[
				'label' => esc_html__( 'Read More', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'more_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .barristar-service a.more' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Service widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$service_style = !empty( $settings['service_style'] ) ? $settings['service_style'] : '';
		$service_limit = !empty( $settings['service_limit'] ) ? $settings['service_limit'] : '';


		$service_more = !empty( $settings['service_more'] ) ? $settings['service_more'] : '';
		$service_btn_text = !empty( $settings['service_btn_text'] ) ? $settings['service_btn_text'] : '';

		$service_order = !empty( $settings['service_order'] ) ? $settings['service_order'] : '';
		$service_orderby = !empty( $settings['service_orderby'] ) ? $settings['service_orderby'] : '';
		$service_show_category = !empty( $settings['service_show_category'] ) ? $settings['service_show_category'] : [];
		$service_show_id = !empty( $settings['service_show_id'] ) ? $settings['service_show_id'] : [];
		$short_content = !empty( $settings['short_content'] ) ? $settings['short_content'] : '';
	
		$excerpt_length = $short_content ? $short_content : '16';
		$service_btn_text = $service_btn_text ? $service_btn_text : esc_html__( 'Read More', 'barristar-core' );

		$service_more  = ( isset( $settings['service_more'] ) && ( 'true' == $settings['service_more'] ) ) ? true : false;

		// Turn output buffer on
		ob_start();

		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($service_show_id) {
			$service_show_id = json_encode( $service_show_id );
			$service_show_id = str_replace(array( '[', ']' ), '', $service_show_id);
			$service_show_id = str_replace(array( '"', '"' ), '', $service_show_id);
      $service_show_id = explode(',',$service_show_id);
    } else {
      $service_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'service',
		  'posts_per_page' => (int)$service_limit,
		  'category_name' => implode(',', $service_show_category),
		  'orderby' => $service_orderby,
		  'order' => $service_order,
      'post__in' => $service_show_id,
		);

		$barristar_service = new \WP_Query( $args );

		if ( $service_style == 'style-three' ) {
			$service_wrap = 'practice-area2 practice-area-3';
		} elseif( $service_style == 'style-two' ) {
			$service_wrap = '';
		} else {
			$service_wrap = 'practice-style-1';
		}

		?>
		<div class="practice-area <?php echo esc_attr( $service_wrap ); ?>">	    
			<div class="row clearfix">
	 			<?php 
					if ( $barristar_service->have_posts()) : while ( $barristar_service->have_posts()) : $barristar_service->the_post();
						$service_options = get_post_meta( get_the_ID(), 'service_options', true );
		        $service_title = isset($service_options['service_title']) ? $service_options['service_title'] : '';
		        $service_icon = isset($service_options['service_icon']) ? $service_options['service_icon'] : '';

		       if ( $service_style == 'style-three' ) { ?>
			        <div class="col-lg-4 col-md-6 col-sm-6">
			          <div class="service-item">
			              <div class="row">
			                  <div class="service-icon">
			                      <i class="<?php echo esc_attr( $service_icon ); ?>"></i>
			                  </div>
			                  <div class="service-text">
			                      <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $service_title ); ?></a></h3>
			                      <p><?php echo wp_trim_words( get_the_content(), $short_content , '') ?></p>
			                      <?php if ( $service_more ) { ?>
			                      	<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $service_btn_text ); ?></a>
			                      <?php } ?>
			                  </div>
			              </div>
			          </div>
			        </div>
			      <?php } else { ?>
			        <div class="col-lg-4 col-md-6 col-sm-6">
			            <div class="service-item">
			                <div class="row">
			                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
			                        <div class="service-icon">
			                            <i class="<?php echo esc_attr( $service_icon ); ?>"></i>
			                        </div>
			                    </div>
			                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
			                        <div class="service-text">
			                            <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $service_title ); ?></a></h3>
			                            <p><?php echo wp_trim_words( get_the_content(), $short_content, '' ) ?></p>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			      <?php } 
						  endwhile;
						  endif;
						  wp_reset_postdata();
						 ?>
	    	</div>
			</div>
			<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Service widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Dustar_Service() );