<?php
/*
 * Elementor Barristar Blog Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Slider extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_slider';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Slider', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-sliders';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Slider widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/

	public function get_script_depends() {
		return ['wpo-barristar_slider'];
	}
	
	
	/**
	 * Register Barristar Slider widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_slider',
			[
				'label' => __( 'Slider Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'slide_style',
			[
				'label' => esc_html__( 'Slide Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
					'style-three' => esc_html__( 'Style Thee', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your slide style.', 'barristar-core' ),
			]
		);
		$repeater = new Repeater();
		 $repeater->add_control(
			'slide_color',
			[
				'label' => esc_html__( 'Slide background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#bbbbbb',
				'selectors' => [
					'{{WRAPPER}} .hero-slider' => 'background-color: {{VALUE}};',
				],
			]
		);
		$repeater->add_control(
			'slider_image',
			[
				'label' => esc_html__( 'Slider Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'slider_title',
			[
				'label' => esc_html__( 'Slider title', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Type slide title here', 'barristar-core' ),
			]
		);
		$repeater->add_control(
			'slider_content',
			[
				'label' => esc_html__( 'Slider content', 'barristar-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__( 'Type slide content here', 'barristar-core' ),
			]
		);
		$repeater->add_control(
			'btn_txt',
			[
				'label' => esc_html__( 'Button/Link Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Button Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type btn text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'button_link',
			[
				'label' => esc_html__( 'Button Link', 'barristar-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$repeater->end_controls_tabs();		

		$this->add_control(
			'swipeSliders_groups',
			[
				'label' => esc_html__( 'Slider Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slider_title' => esc_html__( 'Item #1', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ slider_title }}}',
			]
		);		
		$this->end_controls_section();// end: Section

		$this->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel Options', 'barristar-core' ),
			]
		);
		
		$this->add_control(
			'carousel_nav',
			[
				'label' => esc_html__( 'Navigation', 'barristar-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'barristar-core' ),
				'label_off' => esc_html__( 'No', 'barristar-core' ),
				'return_value' => 'true',
				'description' => esc_html__( 'If you want Carousel Navigation, enable it.', 'barristar-core' ),
			]
		);
		
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-slider .slide-caption p',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero-slider .slide-caption p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'slider_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-slider .slide-caption h2',
			]
		);
		$this->add_control(
			'slider_content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero-slider .slide-caption h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Navigation
		$this->start_controls_section(
			'section_navigation_style',
			[
				'label' => esc_html__( 'Navigation', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'slider_border_color',
			[
				'label' => esc_html__( 'Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero .slick-prev, .hero .slick-next' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'slider_nav_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero .slick-prev:before,.hero .slick-prev:after,.hero .slick-next:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'slider_nav_bg_color',
			[
				'label' => esc_html__( 'Hover Background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero .slick-prev:hover, .hero .slick-next:hover' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'slider_nav_hover_color',
			[
				'label' => esc_html__( 'Hover Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero .slick-prev:hover:before,.hero .slick-prev:hover:after' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'slider_nav_hover_border_color',
			[
				'label' => esc_html__( 'Hover Border Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .hero .slick-prev:hover, .hero .slick-next:hover' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Button Style
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-slider .btn-style a.slide-btn',
			]
		);
		$this->add_responsive_control(
			'button_min_width',
			[
				'label' => esc_html__( 'Width', 'barristar-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 500,
						'step' => 1,
					],
				],
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .hero-slider .btn-style a.slide-btn' => 'min-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'btn_style' => array('style-one'),
				],
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .hero-slider .btn-style a.slide-btn' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_border_radius',
			[
				'label' => __( 'Border Radius', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'btn_style' => array('style-one'),
				],
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .hero-slider .btn-style a.slide-btn' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs( 'button_style' );
			$this->start_controls_tab(
				'button_normal',
				[
					'label' => esc_html__( 'Normal', 'barristar-core' ),
				]
			);
			$this->add_control(
				'button_color',
				[
					'label' => esc_html__( 'Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .hero-slider .btn-style a.slide-btn' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .hero-slider .btn-style a.slide-btn,.btn-style-3 a:before' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_border',
					'label' => esc_html__( 'Border', 'barristar-core' ),
					'selector' => '{{WRAPPER}} .hero-slider .btn-style a.slide-btn',
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_hover',
				[
					'label' => esc_html__( 'Hover', 'barristar-core' ),
				]
			);
			$this->add_control(
				'button_hover_color',
				[
					'label' => esc_html__( 'Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .hero-slider .btn-style a.slide-btn:hover' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_hover_color',
				[
					'label' => esc_html__( 'Background Color', 'barristar-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .hero-slider .btn-style a.slide-btn:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_hover_border',
					'label' => esc_html__( 'Border', 'barristar-core' ),
					'selector' => '{{WRAPPER}} .hero-slider .btn-style a.slide-btn:hover',
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section
	
	}

	/**
	 * Render Blog widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();

		// Carousel Options
		$swipeSliders_groups = !empty( $settings['swipeSliders_groups'] ) ? $settings['swipeSliders_groups'] : [];
		$carousel_nav  = ( isset( $settings['carousel_nav'] ) && ( 'true' == $settings['carousel_nav'] ) ) ? true : false;
		$slide_style = !empty( $settings['slide_style'] ) ? $settings['slide_style'] : '';

		if ( $slide_style == 'style-one' ) {
      $slide_class = ' hero-2 hero-style-2 hero-style-3';
    } elseif ( $slide_style == 'style-two' ) {
      $slide_class = ' hero-style-2';
    } else {
      $slide_class = ' hero-style-1';
    }

		$uniqid = uniqid( '-', false);
  	$inline_style = '';

    if ($carousel_nav != 'yes') {
      $inline_style .= '.hero'.$uniqid.'.hero .slick-prev, .hero'.$uniqid.'.hero .slick-next {';
      $inline_style .= 'display: none !important;';
      $inline_style .= '}';
    } 

  	// integrate css
  	add_inline_style( $inline_style );
  	$inline_class = ' hero'.$uniqid;
		// Turn output buffer on
		ob_start();

		 ?>
		<div class="hero hero-slider-wrapper <?php echo esc_attr( $slide_class.$inline_class ); ?>">
    	<div class="hero-slider">
    	<?php
			if( is_array( $swipeSliders_groups ) && !empty( $swipeSliders_groups ) ){
				foreach ( $swipeSliders_groups as $each_item ) {

				$image_url = wp_get_attachment_url( $each_item['slider_image']['id'] );
				$image_alt = get_post_meta( $each_item['slider_image']['id'], '_wp_attachment_image_alt', true);

				$slider_title = !empty( $each_item['slider_title'] ) ? $each_item['slider_title'] : '';
				$slider_content = !empty( $each_item['slider_content'] ) ? $each_item['slider_content'] : '';
				

				$button_text = !empty( $each_item['btn_txt'] ) ? $each_item['btn_txt'] : '';
				$button_link = !empty( $each_item['button_link']['url'] ) ? $each_item['button_link']['url'] : '';
				$button_link_external = !empty( $each_item['button_link']['is_external'] ) ? 'target="_blank"' : '';
				$button_link_nofollow = !empty( $each_item['button_link']['nofollow'] ) ? 'rel="nofollow"' : '';
				$button_link_attr = !empty( $button_link ) ?  $button_link_external.' '.$button_link_nofollow : '';

				$button = $button_link ? '<a href="'.esc_url( $button_link ).'" '.esc_attr( $button_link_attr ).' class="slide-btn" >'. $button_text .'</a>' : '';

				$button_actual = ( $button ) ? '<div class="btn-style btn-style-3">'.$button.'</div>' : '';

				?>
         <div class="slide">
            <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" class="slider-bg">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-8 slide-caption">
	                    	<?php 
				                	if( $slider_title ) { echo '<p>'.esc_html( $slider_title ).'</p>'; } 
			                   	if( $slider_content ) { echo '<h2>'.esc_html( $slider_content ).'</h2>'; } 
			                 	?>
                        <div class="btns">
                            <?php echo $button_actual; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
				<?php }
			} ?>
    	</div>
		</div>
		<?php
		// Return outbut buffer
		echo ob_get_clean();
		
	}

	/**
	 * Render Blog widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/

	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Slider() );
