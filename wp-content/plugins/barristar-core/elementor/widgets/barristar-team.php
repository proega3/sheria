<?php
/*
 * Elementor Barristar Team Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Team extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_team';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Team', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-users';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Team widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_team'];
	}
	
	/**
	 * Register Barristar Team widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_team',
			[
				'label' => esc_html__( 'Team Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'team_style',
			[
				'label' => esc_html__( 'Team Style', 'barristar-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'barristar-core' ),
					'style-two' => esc_html__( 'Style Two', 'barristar-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your team style.', 'barristar-core' ),
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'team_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'team_subtitle',
			[
				'label' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type sub title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'bg_image',
			[
				'label' => esc_html__( 'Team Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'description' => esc_html__( 'Set your image.', 'barristar-core'),
			]
		);
		$repeater->add_control(
			'facebook_icon',
			[
				'label' => esc_html__( 'Facebook', 'barristar-core' ),
				'type' => Controls_Manager::ICON,
				'options' => Controls_Helper_Output::get_include_icons(),
				'frontend_available' => true,
				'default' => 'ti-facebook',
			]
		);
		$repeater->add_control(
			'facebook_link',
			[
				'label' => esc_html__( 'Facebook Link', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Facebook Link', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type facebook link here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'twitter_icon',
			[
				'label' => esc_html__( 'Twitter', 'barristar-core' ),
				'type' => Controls_Manager::ICON,
				'options' => Controls_Helper_Output::get_include_icons(),
				'frontend_available' => true,
				'default' => 'ti-twitter-alt',
			]
		);
		$repeater->add_control(
			'twitter_link',
			[
				'label' => esc_html__( 'Twitter Link', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Twitter Link', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type twitter link here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'linkedin_icon',
			[
				'label' => esc_html__( 'Linkedin', 'barristar-core' ),
				'type' => Controls_Manager::ICON,
				'options' => Controls_Helper_Output::get_include_icons(),
				'frontend_available' => true,
				'default' => 'ti-linkedin',
			]
		);
		$repeater->add_control(
			'linkedin_link',
			[
				'label' => esc_html__( 'Linkedin Link', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Linkedin Link', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type linkedin link here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'pinterest_icon',
			[
				'label' => esc_html__( 'Pinterest', 'barristar-core' ),
				'type' => Controls_Manager::ICON,
				'options' => Controls_Helper_Output::get_include_icons(),
				'frontend_available' => true,
				'default' => 'ti-pinterest',
			]
		);
		$repeater->add_control(
			'pinterest_link',
			[
				'label' => esc_html__( 'Pinterest Link', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Pinterest Link', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type pinterest link here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'teamItems_groups',
			[
				'label' => esc_html__( 'Team Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'team_title' => esc_html__( 'Team', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ team_title }}}',
			]
		);

		$this->end_controls_section();// end: Section
		

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-section .team-grids h3',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-section .team-grids h3' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .team-section .team-grids h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-section .team-grids h3 + p',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-section .team-grids h3 + p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .team-section .team-grids h3 + p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_icon_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-section .team-grids ul li a',
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-section .team-grids ul li a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Nav Style
		$this->start_controls_section(
			'section_nav_style',
			[
				'label' => esc_html__( 'Navigation Style', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'nav_color',
			[
				'label' => esc_html__( 'Navigation Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-section .owl-theme .owl-controls .owl-nav [class*=owl-]' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'nav_bg_color',
			[
				'label' => esc_html__( 'Navigation Background Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-section .owl-theme .owl-controls .owl-nav [class*=owl-]' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render Team widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$teamItems_groups = !empty( $settings['teamItems_groups'] ) ? $settings['teamItems_groups'] : [];
		$team_style = !empty( $settings['team_style'] ) ? $settings['team_style'] : '';

		if ( $team_style == 'style-one' ) {
      $attorney_class = 'expert-active owl-carousel';
      $attorney_item = 'attorney-item';
    } else{
      $attorney_class = 'expert-sub';
      $attorney_item = 'col-lg-4 col-md-6 col-p';
    }

		// Turn output buffer on
		ob_start();
		?>
		<section class="team-section">
   	 	<div class="team-grids team-slider">
			<?php 	// Group Param Output
				if( is_array( $teamItems_groups ) && !empty( $teamItems_groups ) ){
				foreach ( $teamItems_groups as $each_items) { 

				$team_title = !empty( $each_items['team_title'] ) ? $each_items['team_title'] : '';
				$team_subtitle = !empty( $each_items['team_subtitle'] ) ? $each_items['team_subtitle'] : '';
				$bg_image = !empty( $each_items['bg_image']['id'] ) ? $each_items['bg_image']['id'] : '';

				$facebook_icon = !empty( $each_items['facebook_icon'] ) ? $each_items['facebook_icon'] : '';
				$facebook_link = !empty( $each_items['facebook_link'] ) ? $each_items['facebook_link'] : '';

				$twitter_icon = !empty( $each_items['twitter_icon'] ) ? $each_items['twitter_icon'] : '';
				$twitter_link = !empty( $each_items['twitter_link'] ) ? $each_items['twitter_link'] : '';

				$linkedin_icon = !empty( $each_items['linkedin_icon'] ) ? $each_items['linkedin_icon'] : '';
				$linkedin_link = !empty( $each_items['linkedin_link'] ) ? $each_items['linkedin_link'] : '';
				
				$pinterest_icon = !empty( $each_items['pinterest_icon'] ) ? $each_items['pinterest_icon'] : '';
				$pinterest_link = !empty( $each_items['pinterest_link'] ) ? $each_items['pinterest_link'] : '';

				$image_url = wp_get_attachment_url( $each_items['bg_image']['id'] );
				$image_alt = get_post_meta( $each_items['bg_image']['id'], '_wp_attachment_image_alt', true);

				?>
        <div class="grid">
            <div class="img-holder">
             <?php 
              	if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; }
              ?>
            </div>
            <div class="details">
	              <?php 
	              	if( $team_title ) { echo '<h3>'.esc_html( $team_title ).'</h3>'; } 
	              	if( $team_subtitle ) { echo '<p>'.esc_html( $team_subtitle ).'</p>'; }
	              ?>
                <ul>
                	<?php 
		              	if( $facebook_icon ) { echo '<li><a href="'.esc_url( $facebook_link ).'"><i class="'.esc_attr( $facebook_icon ).'"></i></a></li>'; } 
		              	if( $twitter_icon ) { echo '<li><a href="'.esc_url( $twitter_link ).'"><i class="'.esc_attr( $twitter_icon ).'"></i></a></li>'; } 
		              	if( $linkedin_icon ) { echo '<li><a href="'.esc_url( $linkedin_icon ).'"><i class="'.esc_attr( $linkedin_icon ).'"></i></a></li>'; } 
		              	if( $pinterest_icon ) { echo '<li><a href="'.esc_url( $pinterest_link ).'"><i class="'.esc_attr( $pinterest_icon ).'"></i></a></li>'; } 
		              ?>
                </ul>
            </div>
        </div>
				<?php }
				} ?>
		  </div>
		</section>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Team widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Team() );