<?php
/*
 * Elementor Barristar Testimonial Widget
 * Author & Copyright: wpoceans
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Barristar_Testimonial extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'wpo-barristar_testimonial';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Testimonial', 'barristar-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-quote-right';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['wpoceans-category'];
	}

	/**
	 * Retrieve the list of scripts the Barristar Testimonial widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['wpo-barristar_testimonial'];
	}
	
	/**
	 * Register Barristar Testimonial widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_testimonial',
			[
				'label' => esc_html__( 'Testimonial Options', 'barristar-core' ),
			]
		);
		$this->add_control(
			'section_subtitle',
			[
				'label' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type sub title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'section_title',
			[
				'label' => esc_html__( 'Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'bg_image',
			[
				'label' => esc_html__( 'Testimonial Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'testimonial_title',
			[
				'label' => esc_html__( 'Testimonial Title Text', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'testimonial_subtitle',
			[
				'label' => esc_html__( 'Testimonial Sub Title', 'barristar-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Testimonial Sub Title', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type testimonial Sub title here', 'barristar-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'testimonial_content',
			[
				'label' => esc_html__( 'Testimonial Content', 'barristar-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Testimonial Content', 'barristar-core' ),
				'placeholder' => esc_html__( 'Type testimonial Content here', 'barristar-core' ),
				'label_block' => true,
			]
		);
	  $repeater->add_control(
			'testi_image',
			[
				'label' => esc_html__( 'Testimonial Image', 'barristar-core' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				
			]
		);
		$this->add_control(
			'testimonial_groups',
			[
				'label' => esc_html__( 'Testimonial Items', 'barristar-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'testimonial_title' => esc_html__( 'Testimonial', 'barristar-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ testimonial_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial-area .testimonial-text span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .testimonial-text span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Sub Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .testimonial-text span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ..testimonial-area .testimonial-text h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .testimonial-text h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .testimonial-text h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'barristar-core' ),
				'name' => 'barristar_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial-slide .slide-item p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-slide .slide-item p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'barristar-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonial-slide .slide-item p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Quote Icon Color
		$this->start_controls_section(
			'section_quote_style',
			[
				'label' => esc_html__( 'Quote', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'quote_color',
			[
				'label' => esc_html__( 'Quote Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-slide .slide-item p:before ' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Active Color
		$this->start_controls_section(
			'testi_section_info_style',
			[
				'label' => esc_html__( 'Info', 'barristar-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'info_color',
			[
				'label' => esc_html__( 'Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-slide .img-content h4' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'info_title_color',
			[
				'label' => esc_html__( 'Title Color', 'barristar-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonial-slide .img-content span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render Testimonial widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$testimonial_groups = !empty( $settings['testimonial_groups'] ) ? $settings['testimonial_groups'] : [];

		$section_subtitle = !empty( $settings['section_subtitle'] ) ? $settings['section_subtitle'] : '';
		$section_title = !empty( $settings['section_title'] ) ? $settings['section_title'] : '';

		$bg_image = !empty( $settings['bg_image']['id'] ) ? $settings['bg_image']['id'] : '';

		// Image
		$image_url = wp_get_attachment_url( $bg_image );
		$image_alt = get_post_meta( $settings['bg_image']['id'], '_wp_attachment_image_alt', true);

		// Turn output buffer on
		ob_start();

	 	?>
		<div class="testimonial-area">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-4 col-md-6">
	                <div class="testimonial-img">
	                  <?php 
                    	if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; }
                    ?>
	                </div>
	            </div>
	            <div class="col-lg-7 col-lg-offset-1 col-md-6">
	                <div class="testimonial-text">
	                    <div class="title">
	                    	<?php 
                        	if( $section_subtitle ) { echo '<span>'.esc_html( $section_subtitle ).'</span>'; } 
                        	if( $section_title ) { echo '<h2>'.esc_html( $section_title ).'</h2>'; }
	                       ?>
	                    </div>
	                    <div class="testimonial-slide owl-carousel">
												<?php 	// Group Param Output
													if( is_array( $testimonial_groups ) && !empty( $testimonial_groups ) ){
													foreach ( $testimonial_groups as $each_items ) { 

													$testimonial_title = !empty( $each_items['testimonial_title'] ) ? $each_items['testimonial_title'] : '';
													$testimonial_subtitle = !empty( $each_items['testimonial_subtitle'] ) ? $each_items['testimonial_subtitle'] : '';
													$testimonial_content = !empty( $each_items['testimonial_content'] ) ? $each_items['testimonial_content'] : '';

													$testi_url = wp_get_attachment_url( $each_items['testi_image']['id'] );
													$testi_alt = get_post_meta( $each_items['testi_image']['id'], '_wp_attachment_image_alt', true);

													?>
	                        <div class="slide-item">
	                             <?php if( $testimonial_content ) { echo '<p>'.esc_html( $testimonial_content ).'</p>'; } ?>
	                            <div class="thumb-img">
	                             <?php 
			                        	if( $testi_url ) { echo '<img src="'.esc_url( $testi_url ).'" alt="'.esc_attr( $testi_alt ).'">'; }
			                        	?>
	                            </div>
	                            <div class="img-content">
	                            	<?php 
				                        	if( $testimonial_title ) { echo '<h4>'.esc_html( $testimonial_title ).'</h4>'; } 
				                        	if( $testimonial_subtitle ) { echo '<span>'.esc_html( $testimonial_subtitle ).'</span>'; }
				                        ?>
	                            </div>
	                        </div>
												<?php }
												} ?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		</div>
		<?php  
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Testimonial widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Barristar_Testimonial() );