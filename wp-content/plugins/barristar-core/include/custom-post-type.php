<?php

/**
 * Initialize Custom Post Type - Barristar Theme
 */

function barrister_custom_post_type() {
	// Team - Start
	$service_slug = 'service';
	$services = esc_html__('Services', 'barrister-core');

	// Register custom post type - Team
	register_post_type('service',
		array(
			'labels' => array(
				'name' => $services,
				'singular_name' => sprintf(esc_html__('%s Post', 'barrister-core' ), $services),
				'all_items' => sprintf(esc_html__('%s', 'barrister-core' ), $services),
				'add_new' => esc_html__('Add New', 'barrister-core') ,
				'add_new_item' => sprintf(esc_html__('Add New %s', 'barrister-core' ), $services),
				'edit' => esc_html__('Edit', 'barrister-core') ,
				'edit_item' => sprintf(esc_html__('Edit %s', 'barrister-core' ), $services),
				'new_item' => sprintf(esc_html__('New %s', 'barrister-core' ), $services),
				'view_item' => sprintf(esc_html__('View %s', 'barrister-core' ), $services),
				'search_items' => sprintf(esc_html__('Search %s', 'barrister-core' ), $services),
				'not_found' => esc_html__('Nothing found in the Database.', 'barrister-core') ,
				'not_found_in_trash' => esc_html__('Nothing found in Trash', 'barrister-core') ,
				'parent_item_colon' => ''
			) ,
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 11,
			'menu_icon' => 'dashicons-welcome-add-page',
			'rewrite' => array(
				'slug' => $service_slug,
				'with_front' => false
			),
			'has_archive' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
				'revisions',
			)
		)
	);
	// Service - End

  // Attorneys - Start
  $attorney_slug = 'attorney';
  $attorneys = esc_html__('Attorneys', 'barrister-core');

  // Register custom post type - Attorneys
  register_post_type('attorney',
    array(
      'labels' => array(
        'name' => $attorneys,
        'singular_name' => sprintf(esc_html__('%s Post', 'barrister-core' ), $attorneys),
        'all_items' => sprintf(esc_html__('%s', 'barrister-core' ), $attorneys),
        'add_new' => esc_html__('Add New', 'barrister-core') ,
        'add_new_item' => sprintf(esc_html__('Add New %s', 'barrister-core' ), $attorneys),
        'edit' => esc_html__('Edit', 'barrister-core') ,
        'edit_item' => sprintf(esc_html__('Edit %s', 'barrister-core' ), $attorneys),
        'new_item' => sprintf(esc_html__('New %s', 'barrister-core' ), $attorneys),
        'view_item' => sprintf(esc_html__('View %s', 'barrister-core' ), $attorneys),
        'search_items' => sprintf(esc_html__('Search %s', 'barrister-core' ), $attorneys),
        'not_found' => esc_html__('Nothing found in the Database.', 'barrister-core') ,
        'not_found_in_trash' => esc_html__('Nothing found in Trash', 'barrister-core') ,
        'parent_item_colon' => ''
      ) ,
      'public' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'show_ui' => true,
      'query_var' => true,
      'menu_position' => 10,
      'menu_icon' => 'dashicons-groups',
      'rewrite' => array(
        'slug' => $attorney_slug,
        'with_front' => false
      ),
      'has_archive' => true,
      'capability_type' => 'post',
      'hierarchical' => true,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
      )
    )
  );
  // Attorneys - End
  // Case - Start
  $project_slug = 'project';
  $projects = esc_html__('Project', 'barrister-core');

  // Register custom post type - project
  register_post_type('project',
    array(
      'labels' => array(
        'name' => $projects,
        'singular_name' => sprintf(esc_html__('%s Post', 'barrister-core' ), $projects),
        'all_items' => sprintf(esc_html__('%s', 'barrister-core' ), $projects),
        'add_new' => esc_html__('Add New', 'barrister-core') ,
        'add_new_item' => sprintf(esc_html__('Add New %s', 'barrister-core' ), $projects),
        'edit' => esc_html__('Edit', 'barrister-core') ,
        'edit_item' => sprintf(esc_html__('Edit %s', 'barrister-core' ), $projects),
        'new_item' => sprintf(esc_html__('New %s', 'barrister-core' ), $projects),
        'view_item' => sprintf(esc_html__('View %s', 'barrister-core' ), $projects),
        'search_items' => sprintf(esc_html__('Search %s', 'barrister-core' ), $projects),
        'not_found' => esc_html__('Nothing found in the Database.', 'barrister-core') ,
        'not_found_in_trash' => esc_html__('Nothing found in Trash', 'barrister-core') ,
        'parent_item_colon' => ''
      ) ,
      'public' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'show_ui' => true,
      'query_var' => true,
      'menu_position' => 12,
      'menu_icon' => 'dashicons-portfolio',
      'rewrite' => array(
        'slug' => $project_slug,
        'with_front' => false
      ),
      'has_archive' => true,
      'capability_type' => 'post',
      'hierarchical' => true,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
      )
    )
  );
  // project - End
  // Add Category Taxonomy for our Custom Post Type - Project
  register_taxonomy(
    'project_category',
    'project',
    array(
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'labels' => array(
        'name' => sprintf(esc_html__( '%s Categories', 'barrister-core' ), $projects),
        'singular_name' => sprintf(esc_html__('%s Category', 'barrister-core'), $projects),
        'search_items' =>  sprintf(esc_html__( 'Search %s Categories', 'barrister-core'), $projects),
        'all_items' => sprintf(esc_html__( 'All %s Categories', 'barrister-core'), $projects),
        'parent_item' => sprintf(esc_html__( 'Parent %s Category', 'barrister-core'), $projects),
        'parent_item_colon' => sprintf(esc_html__( 'Parent %s Category:', 'barrister-core'), $projects),
        'edit_item' => sprintf(esc_html__( 'Edit %s Category', 'barrister-core'), $projects),
        'update_item' => sprintf(esc_html__( 'Update %s Category', 'barrister-core'), $projects),
        'add_new_item' => sprintf(esc_html__( 'Add New %s Category', 'barrister-core'), $projects),
        'new_item_name' => sprintf(esc_html__( 'New %s Category Name', 'barrister-core'), $projects)
      ),
      'rewrite' => array( 'slug' => $project_slug . '_cat' ),
    )
  );
 


}


// After Theme Setup
function barrister_custom_flush_rules() {
	// Enter post type function, so rewrite work within this function
	barrister_custom_post_type();
	// Flush it
	flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'barrister_custom_flush_rules');
add_action('init', 'barrister_custom_post_type');


/* ---------------------------------------------------------------------------
 * Custom columns - Service
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-service_columns", "barrister_service_edit_columns");
function barrister_service_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = esc_html__('Title', 'barrister-core' );
  $new_columns['thumbnail'] = esc_html__('Image', 'barrister-core' );
  $new_columns['date'] = esc_html__('Date', 'barrister-core' );

  return $new_columns;
}

add_action('manage_service_posts_custom_column', 'barrister_manage_service_columns', 10, 2);
function barrister_manage_service_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}


/* ---------------------------------------------------------------------------
 * Custom columns - Attorneys
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-attorney_columns", "barrister_attorney_edit_columns");
function barrister_attorney_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = esc_html__('Title', 'barrister-core' );
  $new_columns['thumbnail'] = esc_html__('Image', 'barrister-core' );
  $new_columns['date'] = esc_html__('Date', 'barrister-core' );

  return $new_columns;
}

add_action('manage_attorney_posts_custom_column', 'barrister_manage_attorney_columns', 10, 2);
function barrister_manage_attorney_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}

/* ---------------------------------------------------------------------------
 * Custom columns - case
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-project_columns", "barrister_project_edit_columns");
function barrister_project_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = esc_html__('Title', 'barrister-core' );
  $new_columns['thumbnail'] = esc_html__('Image', 'barrister-core' );
  $new_columns['date'] = esc_html__('Date', 'barrister-core' );

  return $new_columns;
}

add_action('manage_project_posts_custom_column', 'barrister_manage_project_columns', 10, 2);
function barrister_manage_project_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}
