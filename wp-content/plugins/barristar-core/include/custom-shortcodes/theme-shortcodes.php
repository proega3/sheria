<?php
/*
 * All Custom Shortcode for [theme_name] theme.
 * Author & Copyright: WPOceans
 * URL: http://themeforest.net/user/wpoceans
 */

if( ! function_exists( 'barristar_shortcodes' ) ) {
  function barristar_shortcodes( $options ) {

    $options       = array();

    /* Topbar Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Topbar Shortcodes', 'barristar'),
      'shortcodes' => array(

        // Topbar item
        array(
          'name'          => 'barristar_widget_topbars',
          'title'         => esc_html__('Topbar info', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_widget_topbar',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Header Info Title', 'barristar')
            ),
            array(
              'id'        => 'topbar_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Header Info Description', 'barristar')
            ),

          ),

        ),

      ),
    );

    /* Header Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Header Shortcodes', 'barristar'),
      'shortcodes' => array(

        // header Social
        array(
          'name'          => 'barristar_header_socials',
          'title'         => esc_html__('Header Social', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_header_social',
          'clone_title'   => esc_html__('Add New Social', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),
          'clone_fields'  => array(
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'barristar')
            ),
            array(
              'id'        => 'social_icon_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Color', 'barristar'),
            ),
            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'title'     => esc_html__('Social Link', 'barristar')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'barristar'),
              'yes'     => esc_html__('Yes', 'barristar'),
              'no'     => esc_html__('No', 'barristar'),
            ),

          ),

        ),
        // header Social End

      ),
    );

    /* Content Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Content Shortcodes', 'barristar'),
      'shortcodes' => array(

        // Spacer
        array(
          'name'          => 'vc_empty_space',
          'title'         => esc_html__('Spacer', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'height',
              'type'      => 'text',
              'title'     => esc_html__('Height', 'barristar'),
              'attributes' => array(
                'placeholder'     => '20px',
              ),
            ),

          ),
        ),
        // Spacer

        // Social Icons
        array(
          'name'          => 'barristar_socials',
          'title'         => esc_html__('Social Icons', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_social',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

            // Colors
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Colors', 'barristar')
            ),
            array(
              'id'        => 'icon_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Color', 'barristar'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'icon_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Hover Color', 'barristar'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '!=', 'style-three'),
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Backrgound Color', 'barristar'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '!=', 'style-one'),
            ),
            array(
              'id'        => 'bg_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Backrgound Hover Color', 'barristar'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '==', 'style-two'),
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'barristar'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '==', 'style-three'),
            ),

            // Icon Size
            array(
              'id'        => 'icon_size',
              'type'      => 'text',
              'title'     => esc_html__('Icon Size', 'barristar'),
              'wrap_class' => 'column_full',
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => esc_html__('Link', 'barristar')
            ),
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'barristar')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'barristar'),
              'on_text'     => esc_html__('Yes', 'barristar'),
              'off_text'     => esc_html__('No', 'barristar'),
            ),

          ),

        ),
        // Social Icons

        // Useful Links
        array(
          'name'          => 'barristar_useful_links',
          'title'         => esc_html__('Useful Links', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_useful_link',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'column_width',
              'type'      => 'select',
              'title'     => esc_html__('Column Width', 'barristar'),
              'options'        => array(
                'full-width' => esc_html__('One Column', 'barristar'),
                'half-width' => esc_html__('Two Column', 'barristar'),
                'third-width' => esc_html__('Three Column', 'barristar'),
              ),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'title_link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'barristar')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'barristar'),
              'on_text'     => esc_html__('Yes', 'barristar'),
              'off_text'     => esc_html__('No', 'barristar'),
            ),
            array(
              'id'        => 'link_title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'barristar')
            ),

          ),

        ),
        // Useful Links

        // Simple Image List
        array(
          'name'          => 'barristar_image_lists',
          'title'         => esc_html__('Simple Image List', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_image_list',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'get_image',
              'type'      => 'upload',
              'title'     => esc_html__('Image', 'barristar')
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => esc_html__('Link', 'barristar')
            ),
            array(
              'id'    => 'open_tab',
              'type'  => 'switcher',
              'std'   => false,
              'title' => esc_html__('Open link to new tab?', 'barristar')
            ),

          ),

        ),
        // Simple Image List

        // Simple Link
        array(
          'name'          => 'barristar_simple_link',
          'title'         => esc_html__('Simple Link', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'link_style',
              'type'      => 'select',
              'title'     => esc_html__('Link Style', 'barristar'),
              'options'        => array(
                'link-underline' => esc_html__('Link Underline', 'barristar'),
                'link-arrow-right' => esc_html__('Link Arrow (Right)', 'barristar'),
                'link-arrow-left' => esc_html__('Link Arrow (Left)', 'barristar'),
              ),
            ),
            array(
              'id'        => 'link_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Icon', 'barristar'),
              'value'      => 'fa fa-caret-right',
              'dependency'  => array('link_style', '!=', 'link-underline'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link Text', 'barristar'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'barristar'),
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'barristar'),
              'on_text'     => esc_html__('Yes', 'barristar'),
              'off_text'     => esc_html__('No', 'barristar'),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

            // Normal Mode
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Normal Mode', 'barristar')
            ),
            array(
              'id'        => 'text_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Text Color', 'barristar'),
              'wrap_class' => 'column_half el-hav-border',
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'barristar'),
              'wrap_class' => 'column_half el-hav-border',
              'dependency'  => array('link_style', '==', 'link-underline'),
            ),
            // Hover Mode
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Hover Mode', 'barristar')
            ),
            array(
              'id'        => 'text_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Text Hover Color', 'barristar'),
              'wrap_class' => 'column_half el-hav-border',
            ),
            array(
              'id'        => 'border_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Hover Color', 'barristar'),
              'wrap_class' => 'column_half el-hav-border',
              'dependency'  => array('link_style', '==', 'link-underline'),
            ),

            // Size
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Font Sizes', 'barristar')
            ),
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => esc_html__('Text Size', 'barristar'),
              'attributes' => array(
                'placeholder'     => 'Eg: 14px',
              ),
            ),

          ),
        ),
        // Simple Link

        // Blockquotes
        array(
          'name'          => 'barristar_blockquote',
          'title'         => esc_html__('Blockquote', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'blockquote_style',
              'type'      => 'select',
              'title'     => esc_html__('Blockquote Style', 'barristar'),
              'options'        => array(
                '' => esc_html__('Select Blockquote Style', 'barristar'),
                'style-one' => esc_html__('Style One', 'barristar'),
                'style-two' => esc_html__('Style Two', 'barristar'),
              ),
            ),
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => esc_html__('Text Size', 'barristar'),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),
            array(
              'id'        => 'content_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Content Color', 'barristar'),
            ),
            array(
              'id'        => 'left_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Left Border Color', 'barristar'),
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'barristar'),
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Background Color', 'barristar'),
            ),
            // Content
            array(
              'id'        => 'content',
              'type'      => 'textarea',
              'title'     => esc_html__('Content', 'barristar'),
            ),

          ),

        ),
        // Blockquotes

      ),
    );

    /* Widget Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Widget Shortcodes', 'barristar'),
      'shortcodes' => array(

        // widget Contact info
        array(
          'name'          => 'barristar_widget_contact_info',
          'title'         => esc_html__('Contact info', 'barristar'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),
            array(
              'id'        => 'image_url',
              'type'      => 'image',
              'title'     => esc_html__('Background Images', 'barristar'),
            ),
            array(
              'id'        => 'subtitle',
              'type'      => 'text',
              'title'     => esc_html__('Sub Title', 'barristar'),
            ),
            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'barristar'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link text', 'barristar'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'barristar'),
            ),

          ),
        ),
        // widget download-widget
        array(
          'name'          => 'barristar_download_widgets',
          'title'         => esc_html__('Download Widget', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_download_widget',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),
          ),
          'clone_fields'  => array(

            array(
              'id'        => 'download_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Download Icon', 'barristar')
            ),
            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Download Title', 'barristar')
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Download Link', 'barristar')
            ),

          ),

        ),

      ),
    );

    /* Footer Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Footer Shortcodes', 'barristar'),
      'shortcodes' => array(

        // Footer Menus
        array(
          'name'          => 'barristar_footer_menus',
          'title'         => esc_html__('Footer Menu Links', 'barristar'),
          'view'          => 'clone',
          'clone_id'      => 'barristar_footer_menu',
          'clone_title'   => esc_html__('Add New', 'barristar'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'menu_title',
              'type'      => 'text',
              'title'     => esc_html__('Menu Title', 'barristar')
            ),
            array(
              'id'        => 'menu_link',
              'type'      => 'text',
              'title'     => esc_html__('Menu Link', 'barristar')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'barristar'),
              'on_text'     => esc_html__('Yes', 'barristar'),
              'off_text'     => esc_html__('No', 'barristar'),
            ),

          ),

        ),
        // Footer Menus
        array(
          'name'          => 'footer-infos',
          'title'         => esc_html__('footer logo and Text', 'barristar'),
          'fields'        => array(
            array(
              'id'        => 'footer_logo',
              'type'      => 'image',
              'title'     => esc_html__('Footer logo', 'barristar'),
            ),
            array(
              'id'        => 'desc',
              'type'      => 'textarea',
              'title'     => esc_html__('Description', 'barristar'),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'barristar'),
            ),

          ),

        ),
      // footer contact info
      array(
        'name'          => 'barristar_footer_contact_infos',
        'title'         => esc_html__('Contact info', 'barristar'),
        'view'          => 'clone',
        'clone_id'      => 'barristar_footer_contact_info',
        'clone_title'   => esc_html__('Add New', 'barristar'),
        'fields'        => array(

          array(
            'id'        => 'custom_class',
            'type'      => 'text',
            'title'     => esc_html__('Custom Class', 'barristar'),
          ),
          array(
            'id'        => 'title',
            'type'      => 'textarea',
            'title'     => esc_html__('Heading Title', 'barristar'),
          ),

        ),
        'clone_fields'  => array(

          array(
            'id'        => 'item',
            'type'      => 'text',
            'title'     => esc_html__('Contact info item', 'barristar')
          ),
          array(
            'id'        => 'desc',
            'type'      => 'text',
            'title'     => esc_html__('Widget Contact item description', 'barristar')
          ),

        ),

      ),

      ),
    );

  return $options;

  }
  add_filter( 'cs_shortcode_options', 'barristar_shortcodes' );
}