<?php
/**
 * King Composer Library
 * Common Fields
 */
class BarristarLibrary {

	// Get Theme Name
	public static function barristar_kc_cat_name() {
		return esc_html__( "by Barristar", 'barristar-core' );
	}

	// Extra Class
	public static function barristar_class_option() {
		return array(
		  "type" => "text",
		  "label" => esc_html__( "Extra class name", 'barristar-core' ),
		  "name" => "class",
		  "description" => esc_html__( "Custom styled class name.", 'barristar-core')
		);
	}

	// ID
	public static function barristar_id_option() {
		return array(
		  "type" => "text",
		  "label" => esc_html__( "Element ID", 'barristar-core' ),
		  "name" => "id",
		  'value' => '',
		  "description" => esc_html__( "Enter your ID for this element. If you want.", 'barristar-core')
		);
	}

	// Open Link in New Tab
	public static function barristar_open_link_tab() {
		return array(
			"type" => "toggle",
			"label" => esc_html__( "Open New Tab? (Links)", 'barristar-core' ),
			"name" => "open_link",
		);
	}

	/**
	 * Carousel Default Options
	 */

	// Loop
	public static function barristar_carousel_loop() {
		return array(
			"type" => "toggle",
			"label" => esc_html__( "Disable Loop?", 'barristar-core' ),
			"name" => "carousel_loop",
			"description" => esc_html__( "Continuously moving carousel, if enabled.", 'barristar-core')
		);
	}
	// Items
	public static function barristar_carousel_items() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Items", 'barristar-core' ),
		  "name" => "carousel_items",
		  "description" => esc_html__( "Enter the numeric value of how many items you want in per slide.", 'barristar-core')
		);
	}
	// Margin
	public static function barristar_carousel_margin() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Margin", 'barristar-core' ),
		  "name" => "carousel_margin",
		  "description" => esc_html__( "Enter the numeric value of how much space you want between each carousel item.", 'barristar-core')
		);
	}
	// Dots
	public static function barristar_carousel_dots() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Dots", 'barristar-core' ),
		  "name" => "carousel_dots",
		  "description" => esc_html__( "If you want Carousel Dots, enable it.", 'barristar-core')
		);
	}
	// Nav
	public static function barristar_carousel_nav() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Navigation", 'barristar-core' ),
		  "name" => "carousel_nav",
		  "description" => esc_html__( "If you want Carousel Navigation, enable it.", 'barristar-core')
		);
	}
	// Autoplay Timeout
	public static function barristar_carousel_autoplay_timeout() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Autoplay Timeout", 'barristar-core' ),
		  "group" => esc_html__( "Carousel", 'barristar-core' ),
		  "name" => "carousel_autoplay_timeout",
		  "description" => esc_html__( "Change carousel Autoplay timing value. Default : 5000. Means 5 seconds.", 'barristar-core')
		);
	}
	// Autoplay
	public static function barristar_carousel_autoplay() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Autoplay", 'barristar-core' ),
		  "name" => "carousel_autoplay",
		  "description" => esc_html__( "If you want to start Carousel automatically, enable it.", 'barristar-core')
		);
	}
	// Animate Out
	public static function barristar_carousel_animateout() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Animate Out", 'barristar-core' ),
		  "name" => "carousel_animate_out",
		  "description" => esc_html__( "CSS3 animation out.", 'barristar-core')
		);
	}
	// Mouse Drag
	public static function barristar_carousel_mousedrag() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Disable Mouse Drag?", 'barristar-core' ),
		  "name" => "carousel_mousedrag",
		  "description" => esc_html__( "If you want to disable Mouse Drag, check it.", 'barristar-core')
		);
	}
	// Auto Width
	public static function barristar_carousel_autowidth() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Auto Width", 'barristar-core' ),
		  "name" => "carousel_autowidth",
		  "description" => esc_html__( "Adjust Auto Width automatically for each carousel items.", 'barristar-core')
		);
	}
	// Auto Height
	public static function barristar_carousel_autoheight() {
		return array(
		  "type" => "toggle",
			"label" => esc_html__( "Auto Height", 'barristar-core' ),
		  "name" => "carousel_autoheight",
		  "description" => esc_html__( "Adjust Auto Height automatically for each carousel items.", 'barristar-core')
		);
	}
	// Tablet
	public static function barristar_carousel_tablet() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Tablet", 'barristar-core' ),
		  "name" => "carousel_tablet",
		  "description" => esc_html__( "Enter number of items to show in tablet.", 'barristar-core')
		);
	}
	// Mobile
	public static function barristar_carousel_mobile() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Mobile", 'barristar-core' ),
		  "name" => "carousel_mobile",
		  "description" => esc_html__( "Enter number of items to show in mobile.", 'barristar-core')
		);
	}
	// Small Mobile
	public static function barristar_carousel_small_mobile() {
		return array(
		  "type" => "text",
			"label" => esc_html__( "Small Mobile", 'barristar-core' ),
		  "name" => "carousel_small_mobile",
		  "description" => esc_html__( "Enter number of items to show in small mobile.", 'barristar-core')
		);
	}
}