<?php
add_action('init', 'barristar_about_kc_map', 99 );
function barristar_about_kc_map() {

    if (function_exists('kc_add_map')){
        kc_add_map(
            array(
                'med_about' => array(
                    'name' =>  esc_html__('Barristar About','barristar-core'),
                    'description' => esc_html__('Barristar Abou Section', 'barristar-core'),
                    'icon' => 'cpicon kc-icon-box',
                    'category' => BarristarLibrary::barristar_kc_cat_name(),
                    'params' => array(
                    'Content' => array(
                    array(
                        'name' => 'about_style',
                        'label' => esc_html__('About Style', 'barristar-core'),
                        'type' => 'select',
                        'options' => array(
                          'standard' => esc_html__( 'Standard', 'barristar-core' ),
                          'classic' => esc_html__( 'Classic', 'barristar-core' ),
                        ),
                      ),
                      array(
                          'name' => 'title',
                          'label' => esc_html__( 'About Title','barristar-core'),
                          'type' => 'text',
                          'admin_label' => true,
                          'description' => esc_html__('Write About Title ', 'barristar-core'),
                        ),
                      array(
                          'name' => 'sub_title',
                          'label' => esc_html__( 'Sub Title','barristar-core'),
                          'type' => 'text',
                          'admin_label' => true,
                          'description' => esc_html__('Write About Sub Title ', 'barristar-core'),
                          'relation' => array(
                            'parent'    => 'about_style',
                            'show_when' => 'classic'
                          ),
                        ),
                      array(
                          'name' => 'desc',
                          'label' => esc_html__( 'About Description','barristar-core'),
                          'type' => 'editor',
                          'admin_label' => true,
                          'description' => esc_html__('Write About About Description', 'barristar-core'),
                        ),
                       array(
                          'name' => 'button_title',
                          'label' => esc_html__('Button Title', 'barristar-core'),
                          'type' => 'text',
                          'admin_label' => true
                        ),
                        array(
                          'name' => 'link',
                          'label' => esc_html__('Button Link', 'barristar-core'),
                          'type' => 'text',
                          'admin_label' => true,
                          'description' => esc_html__( 'Add your relative URL. Each URL contains link, anchor text and target attributes.', 'barristar-core' ),
                        ),
                        array(
                          'name' => 'singnature_url',
                          'type' => 'attach_image',
                          'label' => esc_html__('Set About Signature ', 'barristar'),
                          'relation' => array(
                            'parent'    => 'about_style',
                            'show_when' => 'standard'
                          ),
                        ),
                        array(
                          'name' => 'image_url',
                          'type' => 'attach_image',
                          'label' => esc_html__('Set About Image ', 'barristar'),
                        ),
                         array(
                          'name' => 'video_link',
                          'type' => 'text',
                          'label' => esc_html__('Set About Video Link ', 'barristar'),
                          'relation' => array(
                            'parent'    => 'about_style',
                            'show_when' => 'classic'
                          ),
                        ),
                        array(
                          'name' => 'class',
                          'label' => esc_html__('Extra Class','barristar-core'),
                          'type' => 'text',
                          'admin_label' => true,
                          'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                        ),
                      ),
                    'Style' => array(
                     array(
                      'name' => 'title_size',
                      'label' => esc_html__('Title Size','barristar-core'),
                      'type' => 'number_slider',
                      'options' => array(
                        'min' => 15,
                        'max' => 60,
                        'unit' => 'px',
                        'show_input' => true
                      ),
                      'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                    ),
                    array(
                        'name' => 'title_color',
                        'label' => esc_html__('About Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                     array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('subtitle Size','barristar-core'),
                       'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 30,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for subtitle such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'subtitle_color',
                        'label' => esc_html__('subtitle Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for subtitle', 'barristar-core')
                      ),
                    array(
                        'name' => 'desc_size',
                        'label' => esc_html__('About Description Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Description such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                    array(
                        'name' => 'desc_color',
                        'label' => esc_html__('About Description Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Description', 'barristar-core')
                    ),
                     array(
                        'name' => 'border_color',
                        'label' => esc_html__('About Image Border Color', 'barristar-core'),
                        'type' => 'color_picker'
                      ),
                     array(
                        'name' => 'button_color',
                        'label' => esc_html__('Button Text Color', 'barristar-core'),
                        'type' => 'color_picker'
                      ),
                      array(
                        'name' => 'button_size',
                        'label' => esc_html__('Button Text Size', 'barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 14,
                          'max' =>24,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                      ),
                      array(
                        'name' => 'background',
                        'label' => esc_html__('Background Color', 'barristar-core'),
                        'type' => 'color_picker'
                      ),
                      array(
                        'name' => 'hover_color',
                        'label' => esc_html__('Hover Color', 'barristar-core'),
                        'type' => 'color_picker'
                      ),
                      array(
                        'name' => 'hover_bg',
                        'label' => esc_html__('Hover Background', 'barristar-core'),
                        'type' => 'color_picker'
                      ),

                    )
                  )
                ),  // End of elemnt kc_icon

            )
        ); // End add map

    } // End if

}
