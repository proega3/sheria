<?php
if (!function_exists('barristar_about_function')) {
    add_shortcode( 'med_about', 'barristar_about_function' );
    function barristar_about_function($atts, $content = NULL){
      extract($atts);
      $e_uniqid       = uniqid();
      $inline_style   = '';

       if ( $title_size || $title_color ) {
        $inline_style .= '.about-section-'.$e_uniqid .'.about-section .about-text h2  {';
        $inline_style .= $title_size ? 'font-size: '. barristar_plugin_check_px($title_size) .';': '';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
        $inline_style .= '}';
      }
       if ( $subtitle_size || $subtitle_color ) {
        $inline_style .= '.about-section-'.$e_uniqid .'.about-section .about-text span {';
        $inline_style .= $subtitle_size ? 'font-size: '. barristar_plugin_check_px($subtitle_size) .';': '';
        $inline_style .= ( $subtitle_color ) ? 'color:'. $subtitle_color .';' : '';
        $inline_style .= '}';
      }
      if ( $desc_size || $desc_color  ) {
        $inline_style .= '.about-section-'.$e_uniqid .'.about-section .about-text p {';
        $inline_style .= ( $desc_size ) ? 'font-size:'.barristar_core_check_px($desc_size) .';' : '';
        $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
        $inline_style .= '}';
      }
      if ( $border_color  ) {
        $inline_style .= '.about-section-'.$e_uniqid .'.about-section .about-title .img-holder:before {';
        $inline_style .= ( $border_color ) ? 'background-color:'. $border_color .';' : '';
        $inline_style .= '}';
      }

     if ( $button_color || $button_size || $background  ) {
        $inline_style .= '.about-section-'.$e_uniqid.'.about-section .about-text .btn-style a {';
        $inline_style .= $background ? 'background-color: '.$background.'; ' : '';
        $inline_style .= $button_color ? 'color: '.$button_color.'; ' : '';
        $inline_style .= $button_size ? 'font-size: '.$button_size.'; ' : '';
        $inline_style .= '}';
      }

      if ( $hover_color || $hover_bg  ) {
        $inline_style .= '.about-section-'.$e_uniqid.'.about-section .about-text .btn-style a:hover {';
        $inline_style .= $hover_color ? 'color: '.$hover_color.'; ' : '';
        $inline_style .= $hover_bg ? 'background-color: '.$hover_bg.'; ' : '';
        $inline_style .= $border_hover_color ? 'border-color: '.$border_hover_color.'; ' : '';
        $inline_style .= '}';
      }

      // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' about-section-'.$e_uniqid.' ';

    $singnature_url = wp_get_attachment_url( $singnature_url );
    $singnature_alt = get_post_meta( $singnature_url, '_wp_attachment_image_alt', true);

    $image_url = wp_get_attachment_url( $image_url );
    $image_alt = get_post_meta( $image_url, '_wp_attachment_image_alt', true);

    if ( $about_style == 'standard' ) {
      $section_class = ' about-area about-area2 ';
    } else {
      $section_class = ' about-area ';
    }

  ob_start(); ?>
  <div class="about-section <?php echo esc_attr( $section_class.$class.$styled_class ); ?>">
    <div class="row">
    <?php if ( $about_style == 'standard'  ) { ?>
       <div class="col col-lg-6 col-md-6">
            <div class="about-title">
                <div class="img-holder">
                    <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="about-text">
                <h2><?php echo esc_html( $title ); ?></h2>
                <p><?php echo wp_kses_post( $desc ); ?></p>
                <div class="btns">
                    <div class="btn-style"><a href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $button_title ); ?></a></div>
                </div>
                <div class="signature">
                   <img src="<?php echo esc_url( $singnature_url ); ?>" alt="<?php echo esc_attr( $singnature_alt ); ?>">
                </div>
            </div>
        </div>
     <?php } else { ?>
      <div class="col-lg-6">
          <div class="about-text title">
              <span><?php echo esc_html( $sub_title ); ?></span>
              <h2><?php echo esc_html( $title ); ?></h2>
              <p><?php echo wp_kses_post( $desc ); ?></p>
              <div class="btns-2">
                  <div class="btn-style"><a href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $button_title ); ?></a></div>
              </div>
          </div>
      </div>
      <div class="col col-lg-6">
          <div class="about-title">
              <div class="img-holder">
                  <div class="overlay">
                       <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
                      <div class="social-1st">
                          <ul>
                              <li>
                                <a href="<?php echo esc_url( $video_link ); ?>" class="video-btn" data-type="iframe">
                                  <i class="fa fa-play"></i>
                                </a>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <?php return ob_get_clean();
  }
}