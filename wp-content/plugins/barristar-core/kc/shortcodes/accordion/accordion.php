<?php
/* ==========================================================
  Accordion
=========================================================== */
if ( !function_exists('barristar_accordion_function')) {
  function barristar_accordion_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_color || $title_size || $title_bg ) {
      $inline_style .= '.faq-section-'. $e_uniqid .' .theme-accordion-s1 .panel-heading .collapsed {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_bg ) ? 'background-color:'. $title_bg .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $desc_color  ) {
      $inline_style .= '.faq-section-'. $e_uniqid .' .theme-accordion-s1 .panel-heading + .panel-collapse > .panel-body p {';
      $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
      $inline_style .= '}';
    }
    if ( $active_bg  ) {
      $inline_style .= '.faq-section-'. $e_uniqid .' .theme-accordion-s1 .panel-heading a {';
      $inline_style .= ( $active_bg ) ? 'background-color:'. $active_bg .';' : '';
      $inline_style .= '}';
    }
  // add inline style
  add_inline_style( $inline_style );
  $styled_class  = ' faq-section-'. $e_uniqid.' ';
  $accordion_items = ( $accordion_items ) ? (array) $accordion_items : array();
  $output = '<div class="faq-section '.esc_attr( $styled_class.''.$class ).'">';
  $output .= '<div class="panel-group faq-accordion theme-accordion-s1" id="accordion">';
  if ( $accordion_items ) {
    $id = 1;
    foreach ( $accordion_items as $key => $accordion_item ) {
      $id++;
      if ( $accordion_item->active_tabs == 'yes') {
        $active_class = 'in';
      } else {
        $active_class = '';
      }
    $output .= '<div class="panel panel-default"><div class="panel-heading">';
    $output .= '<a data-toggle="collapse" data-parent="#accordion" href="#collapse-'.esc_attr( $id ).'" aria-expanded="true">'.esc_html( $accordion_item->tab_title ).'</a>';
    $output .= '</div>';
    $output .= ' <div id="collapse-'.esc_attr( $id ).'" class="panel-collapse collapse '.esc_attr( $active_class ).'">';
    $output .= ' <div class="panel-body">';
    $output .= wp_kses_post( $accordion_item->tab_desc );
    $output .= '</div></div></div>';
   }
  }
  $output .= ' </div></div>';
     return $output;
  }
}
add_shortcode( 'med_accordion', 'barristar_accordion_function' );
