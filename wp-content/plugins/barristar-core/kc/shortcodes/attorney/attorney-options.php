<?php
add_action('init', 'barrister_attorney_kc_map', 99 );
function barrister_attorney_kc_map() {

  if (function_exists('kc_add_map')){
      kc_add_map(
          array(
              'brs_attorney' => array(
                  'name' => esc_html__('Barrister Attorney','barrister-core'),
                  'description' => esc_html__('Display single icon', 'barrister-core'),
                  'icon' => 'cpicon kc-icon-team',
                  'category' => BarristarLibrary::barristar_kc_cat_name(),
                  'params' => array(
                  'Content' => array(
                  array(
                      'name' => 'attorney_style',
                      'label' => esc_html__('Attorney Style', 'barristar-core'),
                      'type' => 'select',
                      'options' => array(
                        'carousel' => esc_html__( 'Carousel', 'barristar-core' ),
                        'grid' => esc_html__( 'Geid', 'barristar-core' ),
                      ),
                    ),
                  array(
                      'name' => 'attorney_colunm',
                      'label' => esc_html__('Attorney Column','barrister-core'),
                      'type' => 'select',
                        'options' => array(
                          '' => esc_html__('Attorney Column Style','barrister-core'),
                          'two_col' => esc_html__('Two Colunm','barrister-core'),
                          'three_col' => esc_html__('Three Colunm','barrister-core'),
                          'four_col' => esc_html__('Four Colunm','barrister-core'),
                        ),
                      'admin_label' => true,
                    ),
                    array(
                        'name' => 'attorney_limit',
                        'label' => esc_html__( 'Attorney Limit','barrister-core'),
                        'type' => 'text',
                        'description' => esc_html__('Write Attorney Limit ', 'barrister-core'),
                      ),
                     array(
                        'name' => 'carousel_items',
                        'label' => esc_html__( 'Attorney Carousel Limit','barrister-core'),
                        'type' => 'text',
                        'description' => esc_html__('Write Attorney Carousel Limit ', 'barrister-core'),
                        'relation' => array(
                          'parent'    => 'attorney_style',
                          'show_when' => 'carousel'
                        ),
                      ),
                     array(
                        'name' => 'carousel_autoplay',
                        'label' => esc_html__('Carousel Autoplay','barrister-core'),
                        'type' => 'toggle',
                        'admin_label' => true,
                        'description' => esc_html__('Turn On if you want to show Carousel Autoplay.', 'barrister-core'),
                        'relation' => array(
                          'parent'    => 'attorney_style',
                          'show_when' => 'carousel'
                        ),
                      ),
                    array(
                        'name' => 'carousel_nav',
                        'label' => esc_html__('Carousel Navigation','barrister-core'),
                        'type' => 'toggle',
                        'admin_label' => true,
                        'description' => esc_html__('Turn On if you want to show Carousel Navigation.', 'barrister-core'),
                        'relation' => array(
                          'parent'    => 'attorney_style',
                          'show_when' => 'carousel'
                        ),
                      ),
                    array(
                        'name' => 'particular_item',
                        'type' => 'autocomplete',
                        'label' => esc_html__( 'Particular Attorney', 'barrister-core' ),
                        'options'       => array(
                          'multiple'      => true,
                          'post_type'     => 'attorney',
                        ),
                        'description' => esc_html__('Type Attorney Name, and select after auto detect', 'barrister-core'),
                      ),
                    array(
                        'name' => 'attorney_order',
                        'label' => esc_html__( 'Orderby','barrister-core'),
                        'type' => 'select',
                          'options' => array(
                            '' => esc_html__('Order','barrister-core'),
                            'ASC' => esc_html__('Asending','barrister-core'),
                            'DESC' => esc_html__('Desending','barrister-core'),
                          ),
                        'admin_label' => true,
                        'description' => esc_html__('Select Attorney Orderby ', 'barrister-core'),
                      ),
                    array(
                        'name' => 'attorney_orderby',
                        'label' => esc_html__( 'Order By','barrister-core'),
                        'type' => 'select',
                          'options' => array(
                            'none' => esc_html__('None','barrister-core'),
                            'ID' => esc_html__('ID','barrister-core'),
                            'author' => esc_html__('Author','barrister-core'),
                            'title' => esc_html__('Title','barrister-core'),
                            'date' => esc_html__('Date','barrister-core'),
                            'menu_order' => esc_html__('Menu order','barrister-core'),
                          ),
                        'admin_label' => true,
                      ),
                  ),
                    'Style' => array(
                      array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barrister-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barrister-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barrister-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barrister-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barrister-core')
                      ),
                      array(
                        'name' => 'subtitle_color',
                        'label' => esc_html__('Sub Title Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Sub title', 'barrister-core')
                      ),
                      array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('Sub Title Size','barrister-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Sub title such as: 15px, 1em ..etc..', 'barrister-core')
                      ),
                      array(
                        'name' => 'bg_color',
                        'label' => esc_html__('Attorney Background Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Background', 'barrister-core')
                      ),
                      array(
                        'name' => 'bg_hover',
                        'label' => esc_html__('Attorney Background Hover Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Background Hover', 'barrister-core')
                      ),
                    )
                  )
              ),  // End of elemnt kc_icon

          )
      ); // End add map

  } // End if

}
