<?php
/* ==========================================================
  Team Info
=========================================================== */
if ( !function_exists('barrister_attorney_function')) {
  function barrister_attorney_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_color || $title_size ) {
      $inline_style .= '.attorneys-section-'.$e_uniqid .' .attorneys-grids .grid .details h3 a {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barrister_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $subtitle_color || $subtitle_size ) {
      $inline_style .= '.attorneys-section-'.$e_uniqid .' .attorneys-grids .grid .details p {';
      $inline_style .= ( $subtitle_color ) ? 'color:'. $subtitle_color .';' : '';
      $inline_style .= ( $subtitle_size ) ? 'font-size:'.barrister_core_check_px($subtitle_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $bg_color ) {
      $inline_style .= '.attorneys-section-'.$e_uniqid .' .attorneys-grids .grid .details {';
      $inline_style .= ( $bg_color ) ? 'background:'. $bg_color .';' : '';
      $inline_style .= '}';
    }
    if ( $bg_hover ) {
      $inline_style .= '.attorneys-section-'.$e_uniqid .' .attorneys-grids .grid:hover .details {';
      $inline_style .= ( $bg_hover ) ? 'background:'. $bg_hover .';' : '';
      $inline_style .= '}';
    }
     if ( $attorney_colunm ) {
      $inline_style .= '.attorneys-section-'.$e_uniqid .' .attorneys-grids .grid {';
      $inline_style .= ( $attorney_colunm ) ? 'width:'. $attorney_colunm .';' : '';
      $inline_style .= '}';
    }
    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' attorneys-section-'.$e_uniqid.' ';

    $aparticular_item = explode(',', $particular_item);
    $perticular_items = array();
    foreach ( $aparticular_item as $item ) {
      $perticular_items[] = substr($item, 0, strpos($item, ":"));;
    }
    $perticular_items = ($particular_item) ? $perticular_items : '';

    if ( $attorney_style == 'carousel' ) {
      $attorney_class = 'expert-active owl-carousel';
      $attorney_item = 'attorney-item';
    } else{
      $attorney_class = 'expert-sub';
      $attorney_item = 'col-lg-4 col-md-6 col-p';
    }

    ob_start();
    $args = array(
      'post_type' => 'attorney',
      'posts_per_page' => (int) $attorney_limit,
      'orderby' => $attorney_orderby,
      'order' => $attorney_order,
      'post__in' => $perticular_items,
    );
    $barrister_attorney = new WP_Query( $args );
    if ($barrister_attorney->have_posts()) : 
      
    $carousel_autoplay = $carousel_autoplay == 'yes' ? ' data-autoplay="true"' : ' data-autoplay="false"';
    $carousel_nav = $carousel_nav == 'yes' ? ' data-nav="true"' : ' data-nav="false"';
    $carousel_items = $carousel_items ? ' data-items="'.esc_attr( $carousel_items ).'"' : ' data-items="3"';
     ?>
    <div class="expert-area <?php echo esc_attr( $styled_class.$class ) ?>">
       <div class="<?php echo esc_attr( $attorney_class ); ?>"  <?php if ( $attorney_style == 'carousel' ) {
        echo  $carousel_items.$carousel_autoplay;
       } ?>>
       <?php  if ( $attorney_style == 'grid' ) { ?>
        <div class="expert-item">
          <div class="row">
       <?php } 
        while ($barrister_attorney->have_posts()) : $barrister_attorney->the_post();
          $attorney_options = get_post_meta( get_the_ID(), 'attorney_options', true );
          $subtitle = isset($attorney_options['subtitle']) ? $attorney_options['subtitle'] : '';
          $attorney_socials = isset($attorney_options['attorney_socials']) ? $attorney_options['attorney_socials'] : '';
          $attorney_image = isset($attorney_options['attorney_image']) ? $attorney_options['attorney_image'] : '';
          global $post;
          $image_url = wp_get_attachment_url( $attorney_image );
          $image_alt = get_post_meta( $attorney_image, '_wp_attachment_image_alt', true);
          ?>
           <div class="<?php echo esc_attr( $attorney_item ); ?>">
             <div class="expert-single">
                <div class="expert-img">
                     <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
                </div>
                <div class="expert-content text-center">
                   <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo the_title(); ?></a></h3>
                   <span><?php echo esc_html( $subtitle ); ?></span>
                   <ul class="d-flex">
                    <?php foreach ( $attorney_socials as $key => $attorney_social ) {
                      echo'<li><a href="'.esc_url( $attorney_social['link'] ).'"><i class="'.esc_attr( $attorney_social['icon']  ).'" aria-hidden="true"></i></a></li>';
                    } ?>
                   </ul>
                </div>
             </div>
          </div>
         <?php endwhile;
          wp_reset_postdata();
         if ( $attorney_style == 'grid' ) { ?>
          </div>
          </div>
         <?php } ?>
      </div>
    </div>
    <?php endif;
    return ob_get_clean();
  }
}
add_shortcode( 'brs_attorney', 'barrister_attorney_function' );
