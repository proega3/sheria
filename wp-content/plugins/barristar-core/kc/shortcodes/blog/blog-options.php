<?php
add_action('init', 'barristar_blog_kc_map', 99 );
function barristar_blog_kc_map() {
  if (function_exists('kc_add_map')){
      kc_add_map(
        array(
        'blog_section' => array(
        'name' => esc_html__('Barristar Blog', 'barristar-core'),
        'description' => esc_html__( 'Blog content', 'barristar-core' ),
        'category' => BarristarLibrary::barristar_kc_cat_name(),
        'icon' => 'cpicon kc-icon-blog-posts',
        'title' => esc_html__('Blog Settings', 'barristar-core'),
        'is_container' => true,
        'priority'  => 130,
        'params' => array(
          esc_html__( 'General', 'barristar-core' )  => array(
            array(
              'name' => 'blog_colunm',
              'label' => esc_html__('Blog Column', 'barristar-core'),
              'type' => 'select',
              'options' => array(
                  '' => esc_html__('Blog Column Style','barristar-core'),
                  'col_2' => esc_html__('Two Colunm','barristar-core'),
                  'col_3' => esc_html__('Three Colunm','barristar-core'),
                  'col_4' => esc_html__('Four Colunm','barristar-core'),
                ),
              'admin_label' => true,
            ),
            array(
              "label"     =>esc_html__('Post Limit', 'barristar-core'),
              "name"  => "blog_limit",
              'type' => 'number_slider',
                'options' => array(
                    'min' => 1,
                    'max' => 50,
                    'unit' => 'px',
                    'show_input' => true
                ),
              "description" => esc_html__( "Enter the number of items to show.", 'barristar-core'),
            ),
            array(
              'type' => 'select',
              'label' => esc_html__( 'Order', 'barristar-core' ),
              'options' => array(
                '' => esc_html__( 'Select Order', 'barristar-core' ),
                'ASC' => esc_html__( 'Asending', 'barristar-core' ),
                'DESC' => esc_html__( 'Desending', 'barristar-core' ),
              ),
              'name' => 'blog_order',
            ),
            array(
              'name' => 'blog_orderby',
              'type' => 'select',
              'label' => esc_html__( 'Order By', 'barristar-core' ),
              'options' => array(
                '' => esc_html__( 'Select Order', 'barristar-core' ),
                'none' => esc_html__( 'None', 'barristar-core' ),
                'ID' => esc_html__( 'ID', 'barristar-core' ),
                'author' => esc_html__( 'Author', 'barristar-core' ),
                'title' => esc_html__( 'Title', 'barristar-core' ),
                'date' => esc_html__( 'Date', 'barristar-core' ),
                'menu_order' => esc_html__( 'Menu Order', 'barristar-core' ),
              ),
            ),
            array(
              'name' => 'particular_item',
              'type' => 'post_taxonomy',
              'label' => esc_html__( 'Set particular category for display post from these category.', 'barristar-core' ),
            ),
            array(
              'name' => 'pagination',
              'type' => 'toggle',
              'label' => esc_html__( 'Pagination', 'barristar-core' ),
              'description' => esc_html__('Turn On if you want to Show Blog Date.', 'barristar-core'),
            ),
           array(
              'name' => 'blog_date',
              'label' => esc_html__('Blog Date','barristar-core'),
              'type' => 'toggle',
              'value'     => 'yes',
              'admin_label' => true,
              'description' => esc_html__('Turn On if you want to Hide Blog Date.', 'barristar-core')
            ),
           array(
              'name' => 'blog_author',
              'label' => esc_html__('Blog Author','barristar-core'),
              'type' => 'toggle',
              'value'     => 'yes',
              'admin_label' => true,
              'description' => esc_html__('Turn On if you want to Hide Blog Author.', 'barristar-core')
            ),
            array(
              'name' => 'class',
              'type' => 'text',
              'label' => esc_html__( 'Custom Class', 'barristar-core' ),
            ),
          ),
          esc_html__( 'Style', 'barristar-core' ) => array(
            array(
              "name"  => "title_color",
              "type"        =>'color_picker',
              "label"     =>esc_html__('Title Color', 'barristar-core'),
            ),
            array(
              "name"  => "title_size",
              "type"        =>'number_slider',
              "label"     =>esc_html__('Title Size', 'barristar-core'),
              'options' => array(
                'min' => 10,
                'max' => 40,
                'unit' => 'px',
                'show_input' => true
              )
            ),
            array(
              'name' => 'date_color',
              'label' => esc_html__('Date Color', 'barristar-core'),
              'type' => 'color_picker',
            ),
            array(
              'name' => 'meta_color',
              'label' => esc_html__('Post Meta Color', 'barristar-core'),
              'type' => 'color_picker',
            ),

          ),
        ),
      ),
      )
    ); // End add map
    }
} // End if