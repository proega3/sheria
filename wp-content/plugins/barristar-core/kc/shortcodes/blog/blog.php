<?php
/* ==========================================================
Blog Post
=========================================================== */
if ( !function_exists('barristar_blogs_function')) {
  function barristar_blogs_function( $atts, $content = NULL ) {
  	extract( $atts );
		$uniqid = uniqid( '-', false);
		$inline_style = '';

		if ( $title_color || $title_size ) {
			$inline_style .= '.blog-area'.$uniqid.'.blog-area .blog-item .blog-content h3 a {';
			$inline_style .= ( $title_color ) ? 'color: '.$title_color.';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
			$inline_style .= '} ';
		}
		if ( $date_color ) {
			$inline_style .= '.blog-area'.$uniqid.'.blog-area .blog-item .post-meta li {';
			$inline_style .= ( $date_color ) ? 'color: '.$date_color.';' : '';
			$inline_style .= '} ';
		}
		if ( $meta_color  ) {
			$inline_style .= '.blog-area'.$uniqid.'.blog-area .meta ul li {';
			$inline_style .= ( $date_color ) ? 'color: '.$date_color.';' : '';
			$inline_style .= '} ';
		}

		if ( $blog_colunm == 'col_2' ) {
			$blog_col = 'col-md-6 col-sm-6';
		} elseif ( $blog_colunm == 'col_4' ) {
			$blog_col = 'col-md-3 col-sm-6 ';
		} else {
			$blog_col = 'col-md-4 col-sm-6 ';
		}

		add_inline_style( $inline_style );
		$styled_class = ' blog-area'.$uniqid;

  	$blog_order = $blog_order ? $blog_order : 'DESC';
  	$blog_orderby = $blog_orderby ? $blog_orderby : 'none';

		// Columns

  	$aparticular_item = explode(',', $particular_item);
    $perticular_items = array();
    foreach ( $aparticular_item as $item ) {
      $perticular_items[] = substr($item, 0, strpos($item, ":"));;
    }
    $perticular_items = ($particular_item) ? $perticular_items : '';
	  	global $paged;
	    if( get_query_var( 'paged' ) )
	      $my_page = get_query_var( 'paged' );
	    else {
	      if( get_query_var( 'page' ) )
	        $my_page = get_query_var( 'page' );
	      else
	        $my_page = 1;
	      set_query_var( 'paged', $my_page );
	      $paged = $my_page;
	    }
    $args = array(
    	'paged' => $paged,
      'post_type' => 'post',
      'posts_per_page' => (int)$blog_limit,
      'orderby' => $blog_orderby,
      'order' => $blog_order,
      'cat' => $perticular_items,
			'ignore_sticky_posts' => true,
    );

    $query_blog = new WP_Query( $args );
    ob_start();
    $post_count = 0;
    if ( $query_blog->have_posts() ) : ?>
			<div class="blog-area blog-shortcode <?php echo esc_attr( $class . $styled_class ); ?>">
				<div class="row">
					<?php
					while ( $query_blog->have_posts() ) :
						$query_blog-> the_post();
						$post_count++;
						$post_options = get_post_meta( get_the_ID(), 'post_options', true );
						$grid_image = isset( $post_options['grid_image'] ) ? $post_options['grid_image'] : '';
						$image_url = wp_get_attachment_url( $grid_image );
            $image_alt = get_post_meta( $grid_image , '_wp_attachment_image_alt', true); ?>
				  <div class="<?php echo esc_attr( $blog_col ); ?> col-12">
	            <div class="blog-item b-0">
	                <div class="blog-img">
	                    <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
	                </div>
	                <div class="blog-content">
	                    <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_title(); ?></a></h3>
	                    <ul class="post-meta">
	                    		<li><?php echo get_avatar( get_the_author_meta( 'ID' ), 125 ); ?></li>
	                    		<li>
	                       <?php
					                    if ( $blog_author == 'yes') {
					                 		printf( esc_html__(' By:','barristar') .' <a href="%1$s" rel="author">%2$s</a>',
							                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
							                get_the_author()
							             ); 
					                } ?>
				                 </li>
				                <?php 
			               		 if ( $blog_date == 'yes' ) { ?>
                        	<li><?php echo get_the_date('M d Y'); ?></li>
									      <?php } ?>
	                    </ul>
	                </div>
	            </div>
	        </div>
				<?php
					endwhile; wp_reset_postdata(); ?>
				</div>
				<?php
				if( $pagination ){
					echo '<nav class="pagination_area"><div class="pagination">';
							$big = 999999999;
							echo paginate_links( array(
                'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                'format'    => '?paged=%#%',
                'total'     => $query_blog->max_num_pages,
                'show_all'  => false,
                'current'   => max( 1, $my_page ),
								'prev_text'    => '<div class="fa fa-angle-left"></div>',
								'next_text'    => '<div class="fa fa-angle-right"></div>',
                'mid_size'  => 1,
                'type'      => 'list'
              ) );
	        echo '</div></div>';
				}
				?>
			</div>
			<!--End blog section-->
    <?php
  	endif;
    return ob_get_clean();
  }
}
add_shortcode( 'blog_section', 'barristar_blogs_function' );