<?php
if (!function_exists( 'barristar_button_kc_map' ) ) {
  add_action('init', 'barristar_button_kc_map', 99 );
  function barristar_button_kc_map() {
    kc_add_map(
      array(
        'barristar_button' => array(
          'name' => esc_html__('Barristar Button', 'barristar-core'),
          'description' => esc_html__( 'Button Styles', 'barristar-core' ),
          'category' => BarristarLibrary::barristar_kc_cat_name(),
          'icon' => 'cpicon kc-icon-button',
          'title' => esc_html__('Button Settings', 'barristar-core'),
          'params' => array(
            esc_html__( 'General', 'barristar-core' ) => array(
              array(
                  'name' => 'button_style',
                  'label' => esc_html__('Button Style', 'barristar-core'),
                  'type' => 'select',
                  'options' => array(
                    'standard' => esc_html__( 'Standard', 'barristar-core' ),
                    'classic' => esc_html__( 'Classic', 'barristar-core' ),
                  ),
                ),
              array(
                'name' => 'title',
                'label' => esc_html__('Button Title', 'barristar-core'),
                'type' => 'text',
                'admin_label' => true
              ),
              array(
                'name' => 'link',
                'label' => esc_html__('Button Link', 'barristar-core'),
                'type' => 'link',
                'admin_label' => true,
                'description' => esc_html__( 'Add your relative URL. Each URL contains link, anchor text and target attributes.', 'barristar-core' ),
              ),
              array(
                'name' => 'class',
                'label' => esc_html__('Extra class name', 'barristar-core'),
                'type' => 'text',
                'description' => esc_html__(' ', 'barristar-core')
              )
            ),
            esc_html__( 'Style', 'barristar-core' ) => array(
              array(
                'name' => 'button_color',
                'label' => esc_html__('Button Text Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'button_size',
                'label' => esc_html__('Button Text Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 14,
                  'max' =>24,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
              array(
                'name' => 'background',
                'label' => esc_html__('Background Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'hover_color',
                'label' => esc_html__('Hover Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'hover_bg',
                'label' => esc_html__('Hover Background', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'border_color',
                'label' => esc_html__('Border Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'border_hover_color',
                'label' => esc_html__('Border Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'border_size',
                'label' => esc_html__('Border Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 2,
                  'max' => 5,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
            ),
          )
        ),
      )
    ); // End add map
  }
} // End if