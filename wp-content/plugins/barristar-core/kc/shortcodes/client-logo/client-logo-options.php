<?php
add_action('init', 'barristar_med_client_kc_map', 99 );
function barristar_med_client_kc_map() {
  if (function_exists('kc_add_map')){
      kc_add_map(
        array(
        'med_client' => array(
          'name' => esc_html__('Client Logo', 'barristar-core'),
          'description' => esc_html__( 'Client Logo', 'barristar-core' ),
          'category' => BarristarLibrary::barristar_kc_cat_name(),
          'title' => esc_html__('Carousel Settings', 'barristar-core'),
          'is_container' => true,
          'icon' => 'cpicon kc-icon-image',
          'params' => array(
            esc_html__( 'General', 'barristar-core' ) => array(
              array(
                'name'      => 'logo_items',
                'type'      => 'group',
                'label'     => esc_html__('Counter Items', 'barristar'),
                'options'   => array('add_text' => esc_html__('Add Item', 'barristar')),
                'params' => array(
                  array(
                    'name' => 'logo_image',
                    'type' => 'attach_image',
                    'label' => esc_html__('Client Image', 'barristar'),
                    'admin_label' => true,
                  ),
                  array(
                    'name' => 'link',
                    'type' => 'text',
                    'label' => esc_html__('Client Link', 'barristar'),
                  ),
                )
              ),
              array(
                'name' => 'class',
                'label' => esc_html__('Extra Class', 'barristar-core'),
                'type' => 'text',
              )
            ),
          ),
        ),
      )
    ); // End add map
  }
} // End if