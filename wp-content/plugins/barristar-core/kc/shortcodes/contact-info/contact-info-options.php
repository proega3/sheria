<?php
add_action('init', 'barristar_contact_info_kc_map', 99 );
function barristar_contact_info_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'contact_info' => array(
	                'name' => esc_html__('Contact Info','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-creative-button',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                     array(
                          'name' => 'title',
                          'label' => esc_html__( 'Title','barristar-core'),
                          'type' => 'text',
                          'admin_label' => true,
                          'description' => esc_html__('Write Contact Info  Title ', 'barristar-core'),
                        ),
                      array(
                          'name' => 'desc',
                          'label' => esc_html__( 'Description','barristar-core'),
                          'type' => 'textarea',
                          'admin_label' => true,
                          'description' => esc_html__('Write Contact Info  Description ', 'barristar-core'),
                        ),
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__(' Options', 'barristar-core'),
                        'name'          => 'contact_items',
                        'description'   => esc_html__( 'Contact Info Iteams Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__(' Add new Contact item', 'barristar-core')),
                        'params' => array(
                          array(
                              'name' => 'icon',
                              'label' => esc_html__( 'Title','barristar-core'),
                              'type' => 'icon_picker',
                              'admin_label' => true,
                              'description' => esc_html__('Select Icon for Contact Info', 'barristar-core'),
                            ),
                          array(
                              'name' => 'title',
                              'label' => esc_html__( 'Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Contact Info Title ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'desc',
                              'label' => esc_html__( 'Description','barristar-core'),
                              'type' => 'textarea',
                              'admin_label' => true,
                              'description' => esc_html__('Write Contact Info  Description ', 'barristar-core'),
                            ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'desc_size',
                        'label' => esc_html__('Contact Info Description Size','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter font-size for Contact Info Description such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
                      array(
                        'name' => 'desc_color',
                        'label' => esc_html__('Contact Info Description Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Contact Info Description', 'barristar-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon
	        )
	    ); // End add map
	} // End if
}
