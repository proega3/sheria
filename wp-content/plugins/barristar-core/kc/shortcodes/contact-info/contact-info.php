<?php
/* ==========================================================
  Service Info
=========================================================== */
if ( !function_exists('barristar_contact_info_function')) {
  function barristar_contact_info_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_size || $title_color ) {
      $inline_style .= '.contact-page-area-'.$e_uniqid .'.contact-page-area .contact-page-item h2, contact-page-area-'.$e_uniqid .'.contact-page-area .contact-page-item h3  {';
      $inline_style .= $title_size ? 'font-size: '. barristar_plugin_check_px($title_size) .';': '';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= '}';
    }
    if ( $desc_size || $desc_color ) {
      $inline_style .= '.contact-page-area-'. $e_uniqid .'.contact-page-area .contact-page-item p, .contact-page-area-'. $e_uniqid .'.contact-page-area .contact-page-item span  {';
      $inline_style .= $desc_size ? 'font-size: '. barristar_plugin_check_px($desc_size) .';': '';
      $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
      $inline_style .= '}';
    }
  
    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = 'contact-page-area-'. $e_uniqid.' ';
    $contact_items = ( $contact_items ) ? (array) $contact_items : array();

    ob_start(); ?>
    <div class="contact-page-area <?php echo esc_attr( $styled_class.$class); ?> ">
       <div class="row">
          <div class="contact-page-item">
             <h2><?php echo esc_html( $title ); ?></h2>
             <p><?php echo esc_html( $desc ); ?></p>
             <?php   if ( $contact_items ) {
              foreach ( $contact_items as $key => $contact_item ) { ?>
                <div class="single-info">
                  <div class="info-con">
                    <i class="<?php echo esc_attr( $contact_item->icon ); ?>"></i>
                  </div>
                 <div class="phone">
                    <h3><?php echo esc_html( $contact_item->title ); ?></h3>
                    <?php echo wp_kses_post( $contact_item->desc ); ?>
                 </div>
               </div>
             <?php } } ?>
          </div>
       </div>
    </div>
    <?php return ob_get_clean();
  }
}
add_shortcode( 'contact_info', 'barristar_contact_info_function' );
