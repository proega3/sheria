<?php
add_action('init', 'barristar_contact7_kc_map', 99 );
function barristar_contact7_kc_map() {
   $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
    $contact_forms = array();
    if ( $cf7 ) {
      foreach ( $cf7 as $cform ) {
        $contact_forms[ $cform->ID ] = $cform->post_title;
      }
    } else {
      $contact_forms[ esc_html__( 'No contact forms found', 'barristar-core' ) ] = 0;
    }
  if (function_exists('kc_add_map')){
      kc_add_map(
          array(
              'reg_ontact7' => array(
                  'name' =>  esc_html__('Contact 7','barristar-core'),
                  'description' => esc_html__('Display single icon', 'barristar-core'),
                  'icon' => 'cpicon fas fa-envelope',
                  'category' => BarristarLibrary::barristar_kc_cat_name(),
                  'params' => array(
                  'Content' => array(
                  array(
                      'name' => 'contact_style',
                      'label' => esc_html__('Contact Style', 'barristar-core'),
                      'type' => 'select',
                      'options' => array(
                        'standard' => esc_html__( 'Standard', 'barristar-core' ),
                        'classic' => esc_html__( 'Classic', 'barristar-core' ),
                      ),
                    ),
                    array(
                      'name' => 'contact_id',
                      'label' => esc_html__('Contact Style','barristar-core'),
                      'type' => 'select',
                        'options' => $contact_forms,
                      'admin_label' => true,
                      'description' => esc_html__('Select your Contact style. ', 'barristar-core'),
                     ),
                    array(
                      'name' => 'toptitle',
                      'label' => esc_html__( 'Top Title ','barristar-core'),
                      'type' => 'text',
                      'description' => esc_html__('Contact Top Title', 'barristar-core'),
                       'relation' => array(
                        'parent'    => 'contact_style',
                        'show_when' => 'standard'
                      ),
                    ),
                    array(
                      'name' => 'title',
                      'label' => esc_html__( 'Title ','barristar-core'),
                      'type' => 'text',
                      'description' => esc_html__('Contact Title', 'barristar-core'),
                    ),
                    array(
                      'name' => 'number',
                      'label' => esc_html__( 'Contact Number Title ','barristar-core'),
                      'type' => 'text',
                      'description' => esc_html__('Contact Number', 'barristar-core'),
                       'relation' => array(
                        'parent'    => 'contact_style',
                        'show_when' => 'standard'
                      ),
                    ),
                    array(
                      'name' => 'desc',
                      'label' => esc_html__( 'description','barristar-core'),
                      'type' => 'textarea',
                      'description' => esc_html__('Contact description', 'barristar-core'),
                       'relation' => array(
                        'parent'    => 'contact_style',
                        'show_when' => 'standard'
                      ),
                    ),
                    ),
                    'Style' => array(
                      array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                       'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                       array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('subtitle Size','barristar-core'),
                       'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 30,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for subtitle such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                          'name' => 'subtitle_color',
                          'label' => esc_html__('subtitle Color','barristar-core'),
                          'type' => 'color_picker',
                          'admin_label' => true,
                          'description' => esc_html__('Set color for subtitle', 'barristar-core')
                        ),
                      array(
                          'name' => 'desc_size',
                          'label' => esc_html__('About Description Size','barristar-core'),
                          'type' => 'number_slider',
                          'options' => array(
                            'min' => 15,
                            'max' => 60,
                            'unit' => 'px',
                            'show_input' => true
                          ),
                          'description' => esc_html__('Enter font-size for Description such as: 15px, 1em ..etc..', 'barristar-core')
                        ),
                      array(
                          'name' => 'desc_color',
                          'label' => esc_html__('About Description Color','barristar-core'),
                          'type' => 'color_picker',
                          'admin_label' => true,
                          'description' => esc_html__('Set color for Description', 'barristar-core')
                      ),
                      array(
                        'name' => 'info_size',
                        'label' => esc_html__('Contact info Size','barristar-core'),
                       'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Contact Info such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'info_color',
                        'label' => esc_html__('Contact info Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Contact Info', 'barristar-core')
                      ),
                      array(
                        'name' => 'border_color',
                        'label' => esc_html__('Contact form input border Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Contact form input border Color', 'barristar-core')
                      ),
                    )
                  )
              ),  // End of elemnt kc_icon

          )
      ); // End add map

  } // End if

}
