<?php
/* ==========================================================
  Contact form 7
=========================================================== */
if ( !function_exists('barristar_contact7_function')) {
  function barristar_contact7_function( $atts, $content = NULL ) {
    extract($atts);

      $e_uniqid       = uniqid();
      $inline_style   = '';

      if ( $title_size || $title_color  ) {
        $inline_style .= '.contact-'.$e_uniqid .'.contact .contact-text .title h2 {';
        $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
        $inline_style .= '}';
      }
       if ( $subtitle_size || $subtitle_color ) {
        $inline_style .= '.contact-'.$e_uniqid .'.contact .contact-text .title span {';
        $inline_style .= $subtitle_size ? 'font-size: '. barristar_plugin_check_px($subtitle_size) .';': '';
        $inline_style .= ( $subtitle_color ) ? 'color:'. $subtitle_color .';' : '';
        $inline_style .= '}';
      }
      if ( $desc_size || $desc_color  ) {
        $inline_style .= '.contact-'.$e_uniqid .'.contact .contact-text p {';
        $inline_style .= ( $desc_size ) ? 'font-size:'.barristar_core_check_px($desc_size) .';' : '';
        $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
        $inline_style .= '}';
      }

      if ( $info_size || $info_color  ) {
        $inline_style .= '.contact-'.$e_uniqid .'.contact .contact-text .date {';
        $inline_style .= ( $info_size ) ? 'font-size:'.barristar_core_check_px($info_size) .';' : '';
        $inline_style .= ( $info_color ) ? 'color:'. $info_color .';' : '';
        $inline_style .= '}';
      }
      if ( $border_color ) {
        $inline_style .= '.contact-'.$e_uniqid .'.contact .contact-form input, .contact-'.$e_uniqid .'.contact .contact-form  textarea, .contact-'.$e_uniqid .'.contact .contact-form .wpcf7-form-control.wpcf7-submit {';
        $inline_style .= ( $border_color ) ? 'border-color:'. $border_color .';' : '';
        $inline_style .= '}';
      }
    
      // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' contact-'.$e_uniqid.' ';
    $contact_id = ( $contact_id ) ? $contact_id : 0;

  ob_start(); ?>
  <?php if ( $contact_style == 'standard' ) { ?>
  <div class="contact contact-area <?php echo esc_attr( $class.$styled_class ); ?>">
    <div class="row">
        <div class="col-lg-5 col-md-12">
            <div class="contact-text">
               <?php if ( $title || $toptitle || $desc || $number ) { ?>
               <div class="title">
                   <span><?php echo esc_html( $toptitle ) ?></span>
                    <h2><?php echo esc_html( $title ) ?></h2>
               </div>
               <span class="date"><?php echo esc_html( $number ); ?></span>
                <p><?php echo esc_html( $desc ) ?></p>
             <?php } ?>
            </div>
        </div>
        <div class="col col-lg-7 col-md-12 col-sm-12">
            <div class="contact-content">
                <div class="contact-form">
                    <?php echo do_shortcode( '[contact-form-7 id="'. $contact_id .'"]' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
  <?php } else { ?>
  <div class="contact contact-page-area">
      <div class="row">
          <div class="contact-area contact-area-2 contact-area-3">
            <h2><?php echo esc_html( $title ); ?></h2>
            <div class="contact-form">
              <?php echo do_shortcode( '[contact-form-7 id="'. $contact_id .'"]' ); ?>
            </div>
          </div>
      </div>
  </div>
  <?php } ?>
  <?php return ob_get_clean();
  }
}
add_shortcode( 'reg_ontact7', 'barristar_contact7_function' );
