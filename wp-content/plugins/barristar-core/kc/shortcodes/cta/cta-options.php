<?php
add_action('init', 'barristar_cta_kc_map', 99 );
function barristar_cta_kc_map() {

  if (function_exists('kc_add_map')){
      kc_add_map(
          array(
              'reg_cta' => array(
                  'name' => esc_html__('Barristar CTA','barristar-core'),
                  'description' => esc_html__('Display single icon', 'barristar-core'),
                  'icon' => 'cpicon kc-icon-call-action',
                  'category' => BarristarLibrary::barristar_kc_cat_name(),
                  'params' => array(
                  'Content' => array(
                     array(
                          'name' => 'cta_style',
                          'label' => esc_html__('Slide Style', 'barristar-core'),
                          'type' => 'select',
                          'options' => array(
                            'style_one' => esc_html__( 'Style One', 'barristar-core' ),
                            'style_two' => esc_html__( 'Style Two', 'barristar-core' ),
                          ),
                          'admin_label' => true,
                      ),
                      array(
                        'name' => 'title',
                        'label' => esc_html__( 'CTA Title','barristar-core'),
                        'type' => 'textarea',
                        'admin_label' => true,
                        'description' => esc_html__('Write Contact Info  Title ', 'barristar-core'),
                      ),
                      array(
                        'name' => 'desc',
                        'label' => esc_html__( 'CTA Sub Title','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write Sub Title ', 'barristar-core'),
                         'relation' => array(
                          'parent'    => 'cta_style',
                          'hide_when' => 'style_two'
                        ),
                      ),
                      array(
                        'name' => 'link',
                        'label' => esc_html__( 'CTA Link','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write CTA Link ', 'barristar-core'),
                      ),
                      array(
                        'name' => 'link_text',
                        'label' => esc_html__( 'CTA Link Text','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write CTA Link Text', 'barristar-core'),
                      ),
                      array(
                        'name' => 'tell',
                        'label' => esc_html__( 'CTA Mobile Number ','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write CTA Mobile Number', 'barristar-core'),
                        'relation' => array(
                          'parent'    => 'cta_style',
                          'hide_when' => 'style_two'
                        ),
                      ),
                    ),
                    'Style' => array(
                      array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                      array(
                        'name' => 'desc_size',
                        'label' => esc_html__('Description Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Description Title Size such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'desc_color',
                        'label' => esc_html__('Description Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Description', 'barristar-core')
                      ),
                      array(
                        'name' => 'btn_size',
                        'label' => esc_html__('Button Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Button such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'btn_color',
                        'label' => esc_html__('Button Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Button', 'barristar-core')
                      ),
                      array(
                        'name' => 'btn_bg',
                        'label' => esc_html__('Button Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Background for Button such as: #fff ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'btn_hover_bg',
                        'label' => esc_html__('Button Background Hover Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Background Hover Color for Button', 'barristar-core')
                      ),
                      array(
                        'name' => 'number_size',
                        'label' => esc_html__('Number Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Number such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'number_color',
                        'label' => esc_html__('Number Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Number', 'barristar-core')
                      ),
                    )
                  )
              ),  // End of elemnt kc_icon
          )
      ); // End add map
  } // End if
}
