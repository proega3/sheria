<?php
/* ==========================================================
  Accordion Info
=========================================================== */
if ( !function_exists('barristar_cta_shortcode_function')) {
  function barristar_cta_shortcode_function( $atts, $content = true ) {
  	extract($atts);
  	$e_uniqid       = uniqid();
    $inline_style   = '';

  if ( $title_color || $title_size ) {
      $inline_style .= '.cta-section-'. $e_uniqid .'.ctasection .cta-text h2 {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
  if ( $number_color || $number_size ) {
      $inline_style .= '.cta-section-'. $e_uniqid .'.ctasection .contact-info h4 {';
      $inline_style .= ( $number_color ) ? 'color:'. $number_color .';' : '';
      $inline_style .= ( $number_size ) ? 'font-size:'.barristar_core_check_px($number_size) .';' : '';
      $inline_style .= '}';
    }
  if ( $desc_color || $desc_size ) {
      $inline_style .= '.cta-section-'. $e_uniqid .'.ctasection .cta-text p {';
      $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
      $inline_style .= ( $desc_size ) ? 'font-size:'.barristar_core_check_px($desc_size) .';' : '';
      $inline_style .= '}';
    }

  if ( $btn_size || $btn_color || $btn_bg) {
      $inline_style .= '.cta-section-'. $e_uniqid .'.ctasection .contact-info .theme-btn {';
      $inline_style .= ( $btn_color ) ? 'color:'. $btn_color .';' : '';
      $inline_style .= ( $btn_bg ) ? 'background-color:'. $btn_bg .';' : '';
      $inline_style .= ( $btn_size ) ? 'font-size:'.barristar_core_check_px($btn_size) .';' : '';
      $inline_style .= '}';
    }
  if ( $btn_hover_bg ) {
      $inline_style .= '.cta-section-'. $e_uniqid .'.ctasection .contact-info .theme-btn:hover {';
      $inline_style .= ( $btn_hover_bg ) ? 'background-color:'. $btn_hover_bg .';' : '';
      $inline_style .= '}';
    }

    // add inline style
  add_inline_style( $inline_style );
  $styled_class  = ' cta-section-'. $e_uniqid.' ';
  if ( $cta_style == 'style_one' ) {
    $section_class = ' cta-section ';
    $col_left = 'col col-lg-7 col-sm-8';
    $col_right = 'col col-lg-3 col-lg-offset-2 col-sm-4';
  } else {
    $section_class = ' cta-s2-section';
    $col_left = 'col col-sm-9';
    $col_right = 'col col-sm-3';
  }
   $output ='<section class="ctasection '.esc_attr( $section_class.$styled_class.$class ).'">';
   $output .='<div class="row">';
   $output .='<div class="'.esc_attr( $col_left ).'">';
   $output .='<div class="cta-text">';
   $output .='<h2>'. wp_kses_post( $title ).'</h2>';
   if ( $desc ) {
     $output .='<p>'. esc_html( $desc ).'</p>';
   }
   $output .='</div>';
   $output .='</div>';
   $output .='<div class="'.esc_attr( $col_right ).'">';
   $output .='<div class="contact-info">';
   if ( $tell ) {
    $output .='<h4>'. esc_html( $tell ).'</h4>';
   }
   $output .='<a href="'.esc_url( $link ).'" class="theme-btn">'.esc_html( $link_text ).'</a>';
   $output .='</div></div></div></section>';
  return $output;
  }
}
add_shortcode( 'reg_cta', 'barristar_cta_shortcode_function' );