<?php
add_action('init', 'barristar_features_kc_map', 99 );
function barristar_features_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'reg_features' => array(
	                'name' => esc_html__('Features','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon cpicon kc-icon-box-alert',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                    array(
                      'name' => 'feature_style',
                      'label' => esc_html__('Feature Style', 'barristar-core'),
                      'type' => 'select',
                      'options' => array(
                        'standard' => esc_html__( 'Standard', 'barristar-core' ),
                        'classic' => esc_html__( 'Classic', 'barristar-core' ),
                      ),
                    ),
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__(' Options', 'barristar-core'),
                        'name'          => 'feature_items',
                        'description'   => esc_html__( 'features Iteams Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__(' Add new features Iteam', 'barristar-core')),
                        'params' => array(
                          array(
                              'name' => 'icon',
                              'label' => esc_html__( 'Icon','barristar-core'),
                              'type' => 'icon_picker',
                              'description' => esc_html__('Chose features  icon', 'barristar-core'),
                            ),
                          array(
                              'name' => 'title',
                              'label' => esc_html__( 'Title','barristar-core'),
                              'type' => 'text',
                              'description' => esc_html__('Write features title', 'barristar-core'),
                            ),
                          array(
                              'name' => 'subtitle',
                              'label' => esc_html__( 'Sub Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write features Sub Title ', 'barristar-core'),
                            ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
	                    ),
                       array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('features subtitle Size','barristar-core'),
                       'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 30,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for features subtitle such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'subtitle_color',
                        'label' => esc_html__('features subtitle Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for features subtitle', 'barristar-core')
                      ),
                      array(
                        'name' => 'icon_size',
                        'label' => esc_html__('features Icon Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 20,
                          'max' => 50,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for features Icon such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'icon_color',
                        'label' => esc_html__('features Icon Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for features Icon', 'barristar-core')
                      ),
                      array(
                        'name' => 'border_color',
                        'label' => esc_html__('features Border Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for features Border', 'barristar-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon
	        )
	    ); // End add map
	} // End if
}
