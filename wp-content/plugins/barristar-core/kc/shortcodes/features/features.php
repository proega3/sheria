<?php
/* ==========================================================
  Service Info
=========================================================== */
if ( !function_exists('barristar_features_function')) {
  function barristar_features_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_size || $title_color ) {
      $inline_style .= '.service-area-'. $e_uniqid .'.service-area .service-item .service-text h3 {';
      $inline_style .= $title_size ? 'font-size: '. barristar_plugin_check_px($title_size) .';': '';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= '}';
    }
    if ( $subtitle_size || $subtitle_color ) {
      $inline_style .= '.service-area-'. $e_uniqid .'.service-area .service-item .service-text span {';
      $inline_style .= $subtitle_size ? 'font-size: '. barristar_plugin_check_px($subtitle_size) .';': '';
      $inline_style .= ( $subtitle_color ) ? 'color:'. $subtitle_color .';' : '';
      $inline_style .= '}';
    }
    if ( $icon_size || $icon_color ) {
      $inline_style .= '.service-area-'. $e_uniqid .'.service-area .service-item .service-icon i:before  {';
      $inline_style .= $icon_size ? 'font-size: '. barristar_plugin_check_px($icon_size) .';': '';
      $inline_style .= ( $icon_color ) ? 'color:'. $icon_color .';' : '';
      $inline_style .= '}';
    }

    if ( $border_color ) {
      $inline_style .= '.service-area-'. $e_uniqid .'.service-area .service-item .service-text, .service-area-'. $e_uniqid .'.service-area .service-item{';
      $inline_style .= ( $border_color ) ? 'border-color:'. $border_color .';' : '';
      $inline_style .= '}';
    }


    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' service-area-'. $e_uniqid.' ';
    $feature_items = ( $feature_items ) ? (array) $feature_items : array();

    if ( $feature_style == 'standard' ) {
      $section_class = ' service-area2 ';
      $col_class = ' icon-c';
    } else {
      $section_class = ' service-area3 ';
      $col_class = ' icon-b';
    }

    $feature_style = $feature_style ? $feature_style : 'standard';
    $feature_items = ( $feature_items ) ? (array) $feature_items : array();

    ob_start(); ?>
    <div class="service-area <?php echo esc_attr( $section_class.$styled_class.''.$class ) ?>">
      <div class="row">
       <?php if ( $feature_items ) {
          foreach ( $feature_items as $key => $feature_item ) { ?>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 feature-col">
              <div class="service-item">
                  <div class="row">
					  <div class="grid">
						  <div class="col-lg-3 col-md-3 col-sm-3 col-3 <?php echo esc_attr( $col_class ); ?>">
							  <div class="service-icon">
								  <i class="<?php echo esc_attr( $feature_item->icon ) ?>"></i>
							  </div>
						  </div>
						  <div class="col-lg-9 col-md-9 col-sm-9 col-9">
							  <div class="service-text">
								  <span><?php echo esc_html( $feature_item->subtitle ); ?></span>
								  <h3><?php echo esc_html( $feature_item->title ); ?></h3>
							  </div>
						  </div>
					  </div>
                  </div>
              </div>
          </div>
          <?php } } ?>
        </div>
    </div>
    <?php return ob_get_clean();
  }
}
add_shortcode( 'reg_features', 'barristar_features_function' );
