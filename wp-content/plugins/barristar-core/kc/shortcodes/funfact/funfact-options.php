<?php
add_action('init', 'barristar_funfact_kc_map', 99 );
function barristar_funfact_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'reg_funfact' => array(
	                'name' => esc_html__('Barristar Funfact','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-coundown',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                    array(
                        'name' => 'funfact_style',
                        'label' => esc_html__('Funfact Style', 'barristar-core'),
                        'type' => 'select',
                        'options' => array(
                          'standard' => esc_html__( 'Standard', 'barristar-core' ),
                          'classic' => esc_html__( 'Classic', 'barristar-core' ),
                        ),
                      ),
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__(' Options', 'barristar-core'),
                        'name'          => 'funfact_items',
                        'description'   => esc_html__( 'Funfact Iteams Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__(' Add new Funfact Iteam', 'barristar-core')),
                        'params' => array(
                             array(
                              'name' => 'title',
                              'label' => esc_html__( 'Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Funfact  Title ', 'barristar-core'),
                            ),
                            array(
                              'name' => 'number',
                              'label' => esc_html__('Number','barristar-core'),
                              'type' => 'text',
                              'admin_label' => false,
                              'description' => esc_html__('Write Funfact  Number ', 'barristar-core'),
                            ),
                            array(
                              'name' => 'percent',
                              'label' => esc_html__('Percent','barristar-core'),
                              'type' => 'text',
                              'admin_label' => false,
                              'description' => esc_html__('Write Funfact  Percent ', 'barristar-core'),
                            ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte .', 'barristar-core')
                      )
                      ,array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em .etc.', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
	                    ),
                      array(
                        'name' => 'number_size',
                        'label' => esc_html__('Number Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Funfact Number such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'number_color',
                        'label' => esc_html__('Number Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Funfact Number', 'barristar-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon
	        )
	    ); // End add map
	} // End if
}
