<?php
/* ==========================================================
  Counter
=========================================================== */
if ( !function_exists('barristar_funfact_function')) {
  function barristar_funfact_function( $atts, $content = NULL ) {

    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_size || $title_color  ) {
      $inline_style .= '.counter-area-'.$e_uniqid .'.counter-area .counter-grids .grid p {';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= '}';
    }
    if ( $number_size || $number_color  ) {
      $inline_style .= '.counter-area-'.$e_uniqid .'.counter-area .counter-grids .grid h2 {';
      $inline_style .= ( $number_size ) ? 'font-size:'.barristar_core_check_px( $number_size) .';' : '';
      $inline_style .= ( $number_color ) ? 'color:'. $number_color .';' : '';
      $inline_style .= '}';
    }
    
     // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' counter-area-'.$e_uniqid.' ';
    $funfact_items = ( $funfact_items ) ? (array) $funfact_items : array();

    if ( $funfact_style == 'standard' ) {
      $wrap_col = '';
      $col_class = 'col-lg-6';
    } else {
      $col_class = 'col-lg-12';
      $wrap_col = ' counter-area2';
    }
    ob_start(); ?>
    <div class="counter-area <?php echo esc_attr(  $wrap_col.$class.$styled_class ) ?>">
      <div class="row">
          <div class="<?php echo esc_attr( $col_class ); ?>">
              <div class="counter-grids">
              <?php
                foreach ( $funfact_items as $key => $funfact_item ) {
                  if ( $funfact_item->number ||  $funfact_item->title ) { ?>
                  <div class="grid">
                      <div>
                         <h2>
                          <span class="odometer" data-count="<?php echo esc_attr( $funfact_item->number ) ?>"><?php echo esc_html__( '00','barristar-core' ); ?>
                          </span>
                          <?php if ( $funfact_item->percent) {
                           echo esc_html( $funfact_item->percent );
                          } ?>
                        </h2>
                      </div>
                      <p><?php echo esc_html( $funfact_item->title ) ?></p>
                  </div>
              <?php } } ?>
              </div>
          </div>
      </div>
    </div>
   <?php return ob_get_clean();
  }
}
add_shortcode( 'reg_funfact', 'barristar_funfact_function' );
