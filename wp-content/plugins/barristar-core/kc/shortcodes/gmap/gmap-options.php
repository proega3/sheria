<?php
add_action('init', 'barristar_gmaps_kc_map', 99 );
function barristar_gmaps_kc_map() {
	if (function_exists('kc_add_map')){
	    kc_add_map(
        array(
          'brstr_gmap' => array(
            'name' => 'Barristar Google Map',
            'description' => esc_html__('Display single icon', 'barristar-core'),
            'icon' => 'cpicon kc-icon-map',
            'category' => BarristarLibrary::barristar_kc_cat_name(),
            'params' => array(
            'Content' => array(
              array(
                  'name' => 'gmap_id',
                  'label' => esc_html__( 'Map ID','barristar-core'),
                  'type' => 'text',
                  'admin_label' => true,
                  'description'   => wp_kses( __('Enter google map ID. If you\'re using this in <strong>Map Tab</strong> shortcode, enter unique ID for each map tabs. Else leave it as blank. <strong>Note : This should same as Tab ID in Map Tabs shortcode.</strong> ', 'barristar-core'), array( 'strong' => array() ) ),
                ),
              array(
                  'name' => 'gmap_api',
                  'label' => esc_html__( 'Google Map API ','barristar-core'),
                  'type' => 'text',
                  'admin_label' => true,
                  "description" => wp_kses( __( 'New Google Maps usage policy dictates that everyone using the maps should register for a free API key. <br />Please create a key for "Google Static Maps API" and "Google Maps Embed API" using the <a href="https://console.developers.google.com/project" target="_blank">Google Developers Console</a>.<br /> Or follow this step links : <br /><a href="https://console.developers.google.com/flows/enableapi?apiid=maps_embed_backend&keyType=CLIENT_SIDE&reusekey=true" target="_blank">1. Step One</a> <br /><a href="https://console.developers.google.com/flows/enableapi?apiid=static_maps_backend&keyType=CLIENT_SIDE&reusekey=true" target="_blank">2. Step Two</a><br /> If you still receive errors, please check following link : <a href="https://churchthemes.com/2016/07/15/page-didnt-load-google-maps-correctly/" target="_blank">How to Fix?</a>', 'barristar-core'), array( 'br' => array(), 'a' => array( 'href' => array() ) ) ),
                ),
               array(
                  'name' => 'gmap_type',
                  'label' => esc_html__( 'Google Map Type','barristar-core'),
                  'type' => 'select',
                    'options' => array(
                      '' => 'Select Type',
                      'ROADMAP' => 'ROADMAP',
                      'SATELLITE' => 'SATELLITE',
                      'HYBRID' => 'HYBRID',
                      'TERRAIN' => 'TERRAIN',
                    ),
                  'admin_label' => true,
                  'description' => esc_html__('Select Google Map Type ', 'barristar-core'),
                ),
               array(
                  'name' => 'gmap_style',
                  'label' => esc_html__( 'Google Map Type','barristar-core'),
                  'type' => 'select',
                  'options' => array(
                    '' => esc_html__( 'Select Style', 'industrial-plugin' ),
                    "gray-scale"  =>  esc_html__( 'Gray Scale', 'industrial-plugin' ),
                    "mid-night" => esc_html__( 'Mid Night', 'industrial-plugin' ),
                    'blue-water' => esc_html__( 'Blue Water', 'industrial-plugin' ) ,
                    'light-dream' =>  esc_html__( 'Light Dream', 'industrial-plugin' ) ,
                    'pale-dawn' => esc_html__( 'Pale Dawn', 'industrial-plugin' ),
                    'apple-maps' =>  esc_html__( 'Apple Maps-esque', 'industrial-plugin' ),
                    'blue-essence' => esc_html__( 'Blue Essence', 'industrial-plugin' ),
                    'unsaturated-browns' => esc_html__( 'Unsaturated Browns', 'industrial-plugin' ) ,
                    'paper' => esc_html__( 'Paper', 'industrial-plugin' ),
                    'midnight-commander' => esc_html__( 'Midnight Commander', 'industrial-plugin' ),
                    'light-monochrome' => esc_html__( 'Light Monochrome', 'industrial-plugin' ) ,
                    'flat-map' => esc_html__( 'Flat Map', 'industrial-plugin' ) ,
                    'retro' => esc_html__( 'Retro', 'industrial-plugin' ) ,
                    'becomeadinosaur' => esc_html__( 'becomeadinosaur', 'industrial-plugin' ) ,
                    'neutral-blue' => esc_html__( 'Neutral Blue', 'industrial-plugin' ),
                    'subtle-grayscale' => esc_html__( 'Subtle Grayscale', 'industrial-plugin' ),
                    'ultra-light-labels' => esc_html__( 'Ultra Light with Labels', 'industrial-plugin' ),
                    'shades-grey' => esc_html__( 'Shades of Grey', 'industrial-plugin' ),
                    ),
                  'relation' => array(
                      'parent'    => 'gmap_type',
                      'show_when' => 'ROADMAP'
                  ),
                  'admin_label' => true,
                  'description' => esc_html__('Select Google Map Type ', 'barristar-core'),
                ),
                array(
                  'name' => 'gmap_common_marker',
                  'label' => esc_html__( 'Map Marker','barristar-core'),
                  'type' => 'attach_image',
                  'admin_label' => true,
                  'description' => esc_html__('Enter Map Marker ', 'barristar-core'),
                ),
                array(
                  'name' => 'gmap_height',
                  'label' => esc_html__( 'Map Height','barristar-core'),
                  'type' => 'text',
                  'admin_label' => true,
                  'description' => esc_html__('Enter the px value for map height. This will not work if you add this shortcode into the Map Tab shortcode. ', 'barristar-core'),
                ),
                array(
                  'name' => 'gmap_scroll_wheel',
                  'label' => esc_html__( 'Map Scroll Wheel','barristar-core'),
                  'type' => 'toggle',
                  'value' => false,
                  'description' => 'Turn this ON to Map Scroll Wheel',
                ),
                array(
                  'name' => 'gmap_street_view',
                  'label' => esc_html__( 'Street View Control','barristar-core'),
                  'type' => 'toggle',
                  'value' => false,
                  'description' => 'Turn this ON to Street View Control',
                ),
                array(
                  'name' => 'gmap_maptype_control',
                  'label' => esc_html__( 'Map Type Control','barristar-core'),
                  'type' => 'toggle',
                  'value' => false,
                  'description' => 'Turn this ON toMap Type Control',
                ),
              array(
                  'type'          => 'group',
                  'label'         => esc_html__('Map Locations', 'barristar-core'),
                  'name'          => 'locations',
                  'description'   => esc_html__( 'Map Locations', 'barristar-core' ),
                  'options'       => array('add_text' => esc_html__(' Add new Map Locations', 'barristar-core')),
                  'params' => array(
                    array(
                        'name' => 'latitude',
                        'label' => esc_html__( 'Latitude','barristar-core'),
                        'type' => 'text',
                        'description' => wp_kses( __( 'Find Latitude : <a href="http://www.latlong.net/" target="_blank">latlong.net</a>', 'barristar-core' ), array( 'a' => array( array( 'href' => array(), 'target' => array() ) ) ) ),
                      ),
                    array(
                        'name' => 'longitude',
                        'label' => esc_html__( 'Longitude','barristar-core'),
                        'type' => 'text',
                        'description' => wp_kses( __( 'Find Longitude : <a href="http://www.latlong.net/" target="_blank">latlong.net</a>', 'barristar-core' ), array( 'a' => array(  array( 'href' => array(), 'target' => array()  ) ) ) ),
                      ),
                    array(
                      'name' => 'custom_marker',
                      'label' => esc_html__( 'Custom Marker','barristar-core'),
                      'type' => 'attach_image',
                      'admin_label' => true,
                      'description' => esc_html__('Upload your unique map marker if your want to differentiate from others. ', 'barristar-core'),
                    ),
                    array(
                      'name' => 'location_heading',
                      'label' => esc_html__( 'Heading','barristar-core'),
                      'type' => 'text',
                      'admin_label' => true,
                    ),
                    array(
                      'name' => 'location_text',
                      'label' => esc_html__( 'Content','barristar-core'),
                      'type' => 'textarea',
                      'admin_label' => true,
                    ),
                  ),
                ),
                array(
                  'name' => 'class',
                  'label' => esc_html__('Extra Class','barristar-core'),
                  'type' => 'text',
                  'admin_label' => true,
                  'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                ),
              ),
            )
        ),  // End of elemnt kc_icon
      )
    ); // End add map
	} // End if
}
