<?php
add_action('init', 'barristar_pricing_kc_map', 99 );
function barristar_pricing_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'brs_pricing' => array(
	                'name' => esc_html__('Pricing ','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-pricing',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                    array(
                      'type'          => 'group',
                      'label'         => esc_html__(' Options', 'barristar-core'),
                      'name'          => 'pricing_items',
                      'description'   => esc_html__( 'Pricing  Iteams Group Field', 'barristar-core' ),
                      'options'       => array('add_text' => esc_html__(' Add new Pricing  Iteam', 'barristar-core')),
                      'params' => array(
                        array(
                            'name' => 'price_icon',
                            'label' => esc_html__( 'Select Price Icon here','barristar-core'),
                            'type' => 'icon_picker',
                            'description' => esc_html__('Select Price Icon', 'barristar-core'),
                          ),
                        array(
                            'name' => 'price_type',
                            'label' => esc_html__( 'Price Type lite or Professional','barristar-core'),
                            'type' => 'text',
                            'description' => esc_html__('Write Type lite or Professional ', 'barristar-core'),
                          ),
                        array(
                          'name' => 'price_number',
                          'label' => esc_html__( 'Price Amount','barristar-core'),
                          'type' => 'text',
                          'description' => esc_html__('Write Price Amount In Number ', 'barristar-core'),
                          ),
                         array(
                            'name' => 'price_curency',
                            'label' => esc_html__( 'Price Currency','barristar-core'),
                            'type' => 'text',
                            'description' => esc_html__('Write Price Currency ', 'barristar-core'),
                          ),
                         array(
                            'name' => 'item',
                            'label' => esc_html__( 'Price Field Iteam','barristar-core'),
                            'type' => 'editor',
                            'description' => esc_html__('Write Price Field Iteam ', 'barristar-core'),
                          ),
                         array(
                            'name' => 'price_link',
                            'label' => esc_html__( 'Price Button Link','barristar-core'),
                            'type' => 'text',
                            'description' => esc_html__('Write Price Button Link ', 'barristar-core'),
                          ),
                         array(
                            'name' => 'button_text',
                            'label' => esc_html__( 'Price Button Text','barristar-core'),
                            'type' => 'text',
                            'description' => esc_html__('Write Price Button Text ', 'barristar-core'),
                          ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                       array(
                        'name' => 'icon_color',
                        'label' => esc_html__('Price Icon Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Icon color for Price', 'barristar-core')
                      ),
                      array(
                        'name' => 'price_size',
                        'label' => esc_html__('Price Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Price such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'price_color',
                        'label' => esc_html__('Price Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Price', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
	                    array(
                        'name' => 'desc_size',
                        'label' => esc_html__('Pricing  Description Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Pricing  Description such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
                      array(
                        'name' => 'desc_color',
                        'label' => esc_html__('Pricing  Description Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing  Description', 'barristar-core')
                      ),
                      array(
                        'name' => 'button_size',
                        'label' => esc_html__('Pricing  Button Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Pricing  Button such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'button_color',
                        'label' => esc_html__('Pricing  Button Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing  Button', 'barristar-core')
                      ),
                      array(
                        'name' => 'button_bg',
                        'label' => esc_html__('Pricing  Button Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing  Button Background', 'barristar-core')
                      ),
                      array(
                        'name' => 'hover_color',
                        'label' => esc_html__('Pricing  Button Hover Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing Button Hover', 'barristar-core')
                      ),
                      array(
                        'name' => 'hover_bg',
                        'label' => esc_html__('Pricing  Button Hover Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing  Button Hover Background', 'barristar-core')
                      ),
                      array(
                        'name' => 'price_hover_bg',
                        'label' => esc_html__('Pricing Hover Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Pricing Hover Background', 'barristar-core')
                      ),

		                )
	                )
	            ),  // End of elemnt kc_icon
	        )
	    ); // End add map
	} // End if
}
