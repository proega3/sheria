<?php
/* ==========================================================
  Pricing Info
=========================================================== */
if ( !function_exists('barristar_pricing_function')) {
  function barristar_pricing_function( $atts, $content = NULL ) {
    extract($atts);

 $e_uniqid       = uniqid();
  $inline_style   = '';
 if ( $icon_color ) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .pricing-icon .fi {';
    $inline_style .= ( $icon_color ) ? 'color:'. $icon_color .';' : '';
    $inline_style .= '}';
  }
 if ( $price_size || $price_color ) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .pricing-text h2 {';
    $inline_style .= ( $price_size ) ? 'font-size:'.barristar_core_check_px( $price_size ) .';' : '';
    $inline_style .= ( $price_color ) ? 'color:'. $price_color .';' : '';
    $inline_style .= '}';
  }
  if ( $title_size || $title_color ) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .pricing-icon span {';
    $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px( $title_size ) .';' : '';
    $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
    $inline_style .= '}';
  }
  if ( $desc_size || $desc_color ) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .pricing-text p {';
    $inline_style .= ( $desc_size ) ? 'font-size:'.barristar_core_check_px( $desc_size ) .';' : '';
    $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
    $inline_style .= '}';
  }
  if ( $button_size || $button_color || $button_bg) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .btn-style a  {';
    $inline_style .= ( $button_size ) ? 'font-size:'.barristar_core_check_px( $button_size ) .';' : '';
    $inline_style .= ( $button_color ) ? 'color:'. $button_color .';' : '';
    $inline_style .= ( $button_bg ) ? 'background-color:'. $button_bg .';' : '';
    $inline_style .= '}';
  }
  if ( $hover_color || $hover_bg) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .btn-style a:hover {';
    $inline_style .= ( $hover_color ) ? 'color:'. $hover_color .';' : '';
    $inline_style .= ( $hover_bg ) ? 'background-color:'. $hover_bg .';' : '';
    $inline_style .= '}';
  }
  if ( $price_hover_bg ) {
    $inline_style .= '.prising-area-'.$e_uniqid .'.prising-area .pricing-item:hover {';
    $inline_style .= ( $hover_color ) ? 'color:'. $hover_color .';' : '';
    $inline_style .= ( $hover_bg ) ? 'background-color:'. $hover_bg .';' : '';
    $inline_style .= '}';
  }
 // add inline style
  add_inline_style( $inline_style );
  $styled_class  = ' prising-area-'.$e_uniqid.' ';

  $pricing_items = ( $pricing_items ) ? (array) $pricing_items : array();
  ob_start(); ?>
    <div class="prising-area <?php echo esc_attr( $styled_class.$class ); ?>">
        <div class="row">
           <?php if ( $pricing_items ) {
            foreach ( $pricing_items as $key => $pricing_item ) { ?>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="pricing-item">
                    <div class="pricing-icon">
                        <i class="<?php echo esc_attr( $pricing_item->price_icon ); ?>"></i>
                        <span><?php echo esc_html( $pricing_item->price_type ); ?></span>
                    </div>
                    <div class="pricing-text">
                        <h2><span><?php echo esc_html( $pricing_item->price_curency ); ?></span><?php echo esc_html( $pricing_item->price_number ); ?></h2>
                        <p><?php echo wp_kses_post( $pricing_item->item ); ?></p>         
                        <div class="btns text-center">
                            <div class="btn-style">
                                <a href="<?php echo esc_url( $pricing_item->price_link ); ?>">
                                  <?php echo esc_html( $pricing_item->button_text ); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <?php }
           } ?>
        </div>
    </div>
    <?php return ob_get_clean();
  }
}
add_shortcode( 'brs_pricing', 'barristar_pricing_function' );
