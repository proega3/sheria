<?php
add_action('init', 'barrister_project_filter_kc_map', 99 );
function barrister_project_filter_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'project_filter' => array(
	                'name' => esc_html__('Project Filter','barrister-core'),
	                'description' => esc_html__('Display Project', 'barrister-core'),
	                'icon' => 'cpicon kc-icon-image-hover',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                   array(
                        'name' => 'project_style',
                        'label' => esc_html__('Project Style', 'barristar-core'),
                        'type' => 'select',
                        'options' => array(
                          'full_width' => esc_html__( 'Ful Width', 'barristar-core' ),
                          'box_width' => esc_html__( 'Box Width', 'barristar-core' ),
                        ),
                        'admin_label' => true,
                    ),
                  array(
                      'name' => 'all_project',
                      'label' => esc_html__( 'Write All Project Title','barrister-core'),
                      'type' => 'text',
                      'description' => esc_html__('Write All Project Title ', 'barrister-core'),
                    ),
                    array(
                        'name' => 'project_limit',
                        'label' => esc_html__( 'Project Limit','barrister-core'),
                        'type' => 'text',
                        'description' => esc_html__('Write Project Limit ', 'barrister-core'),
                      ),
                    array(
                        'name' => 'particular_item',
                        'type' => 'autocomplete',
                        'label' => esc_html__( 'Particular Project', 'barrister-core' ),
                        'options'       => array(
                          'multiple'      => true,
                          'post_type'     => 'project_filter',
                        ),
                        'description' => esc_html__('Type Project Name, and select after auto detect', 'barrister-core'),
                      ),
                    array(
                        'name' => 'project_show_category',
                        'label' => esc_html__( 'Show Certain Categories','barrister-core'),
                        'type' => 'text',
                        'description' => esc_html__('Show only certain categories ', 'barrister-core'),
                      ),
                    array(
                        'name' => 'project_hide_post',
                        'label' => esc_html__( 'Hide Certain Project','barrister-core'),
                        'type' => 'text',
                        'description' => esc_html__('Hide only certain Project ', 'barrister-core'),
                      ),
                    array(
                        'name' => 'project_filter',
                        'label' => esc_html__( 'Project Filter','barrister-core'),
                        'type' => 'toggle',
                        'admin_label' => true,
                        'description' => esc_html__('Turn Off if you want to Hide Project Filter', 'barrister-core'),
                      ),
                    array(
                        'name' => 'project_order',
                        'label' => esc_html__( 'Orderby','barrister-core'),
                        'type' => 'select',
                          'options' => array(
                            '' => esc_html__('Order','barrister-core'),
                            'ASC' => esc_html__('Asending','barrister-core'),
                            'DESC' => esc_html__('Desending','barrister-core'),
                          ),
                        'admin_label' => true,
                        'description' => esc_html__('Select Project Orderby ', 'barrister-core'),
                      ),
                    array(
                        'name' => 'project_orderby',
                        'label' => esc_html__( 'Order By','barrister-core'),
                        'type' => 'select',
                          'options' => array(
                            'none' => esc_html__('None','barrister-core'),
                            'ID' => esc_html__('ID','barrister-core'),
                            'author' => esc_html__('Author','barrister-core'),
                            'title' => esc_html__('Title','barrister-core'),
                            'date' => esc_html__('Date','barrister-core'),
                            'menu_order' => esc_html__('Menu order','barrister-core'),
                          ),
                        'admin_label' => true,
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barrister-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barrister-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barrister-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barrister-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barrister-core')
	                    ),
	                    array(
                        'name' => 'subtitle_color',
                        'label' => esc_html__('Sub Title Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Sub title', 'barrister-core')
	                    ),
                      array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('Sub Title Size','barrister-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Sub title such as: 15px, 1em ..etc..', 'barrister-core')
                      ),
                      array(
                        'name' => 'bg_color',
                        'label' => esc_html__('Project Background Hover Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Hover Background', 'barrister-core')
                      ),
                      array(
                        'name' => 'btn_bgcolor',
                        'label' => esc_html__('Project Slider Button Background Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set button Background color for Project Slider', 'barrister-core')
                      ),
                      array(
                        'name' => 'btn_color',
                        'label' => esc_html__('Project Slider Button Color','barrister-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set button color for Project Slider', 'barrister-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon

	        )
	    ); // End add map

	} // End if

}
