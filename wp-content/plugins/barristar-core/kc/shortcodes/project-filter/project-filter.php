<?php
/* ==========================================================
  Case
=========================================================== */
if ( !function_exists('barrister_project_filter_function')) {
  function barrister_project_filter_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_color || $title_size ) {
      $inline_style .= '.case-project-'.$e_uniqid .'.case-project .grid .details h3 a {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barrister_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $subtitle_color || $subtitle_size ) {
      $inline_style .= '.case-project-'.$e_uniqid .'.case-project .grid .details h3 p {';
      $inline_style .= ( $subtitle_color ) ? 'color:'. $subtitle_color .';' : '';
      $inline_style .= ( $subtitle_size ) ? 'font-size:'.barrister_core_check_px($subtitle_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $bg_color ) {
      $inline_style .= '.case-project-'.$e_uniqid .'.case-project .grid .hover-content {';
      $inline_style .= ( $bg_color ) ? 'background:'. $bg_color .';' : '';
      $inline_style .= '}';
    }

    if ( $btn_color ) {
      $inline_style .= '.case-project-'.$e_uniqid .'.case-project .projects-menu li a {';
      $inline_style .= ( $btn_color ) ? 'color:'. $btn_color .';' : '';
      $inline_style .= '}';
    }
    if ( $btn_bgcolor ) {
      $inline_style .= '.case-project-'.$e_uniqid .'.case-project .projects-menu li a.current {';
      $inline_style .= ( $btn_bgcolor ) ? 'background-color:'. $btn_bgcolor .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' case-project-'.$e_uniqid.' ';

    $aparticular_item = explode(',', $particular_item);
    $perticular_items = array();
    foreach ( $aparticular_item as $item ) {
      $perticular_items[] = substr($item, 0, strpos($item, ":"));;
    }
    $perticular_items = ($particular_item) ? $perticular_items : '';
    $all_project = $all_project ? $all_project : esc_html__( 'All Project' );


    if ( $project_style == 'full_width' ) {
      $project_col = ' col-c ';
    } else{
      $project_col = ' col-lg-4 col-md-6 col-sm-6 ';
    }
    ob_start();
    $args = array(
      'post_type' => 'project',
      'posts_per_page' => (int) $project_limit,
      'orderby' => $project_orderby,
      'order' => $project_order,
      'post__in' => $perticular_items,
      'project_category' => esc_attr($project_show_category),
      'post__not_in' => array( $project_hide_post )
    );
    $barrister_project = new WP_Query( $args );
    if ($barrister_project->have_posts()) :
     $terms = get_terms( 'project_category' );
      ?>
     <div class="case-project <?php echo esc_attr( $styled_class.$class ) ?>">
       <div class="col col-xs-12 sortable-gallery">
        <div class="row">
         <div class="case-filters case-menu">
            <?php if ( $project_filter ) { ?>
              <ul>
                  <li><a data-filter="*" href="#" class="current"><?php echo esc_html( $all_project ) ?></a></li>
                   <?php foreach( $terms as $term ):  ?>
                     <li><a data-filter=".<?php echo esc_attr( $term->slug ); ?>" href="#"><?php echo esc_html(  $term->name ); ?></a></li>
                   <?php  endforeach; ?>
              </ul>
              <?php } ?>
          </div>
        <div class="case-grids gallery-container clearfix">
        <?php
          while ( $barrister_project->have_posts()) : $barrister_project->the_post();
         $project_options = get_post_meta( get_the_ID(), 'project_options', true );
          $project_title = isset( $project_options['project_title']) ? $project_options['project_title'] : '';
          $project_subtitle = isset( $project_options['project_subtitle']) ? $project_options['project_subtitle'] : '';
          $slide_image = isset( $project_options['project_image']) ? $project_options['project_image'] : '';
          global $post;
          $slide_url = wp_get_attachment_url( $slide_image );
          $slide_alt = get_post_meta( $slide_image , '_wp_attachment_image_alt', true);
          $terms = wp_get_post_terms( get_the_ID(), 'project_category');

          foreach ($terms as $term) {
            $cat_class = $term->slug;
          }
          $count = count($terms);
          $i=0;
          $cat_class = '';
          if ($count > 0) {
            foreach ($terms as $term) {
              $i++;
              $cat_class .= $term->slug .' ';
              if ($count != $i) {
                $cat_class .= '';
              } else {
                $cat_class .= '';
              }
            }
          }
        ?>
          <div class="grid <?php echo esc_attr( $project_col.$cat_class ); ?>">
               <div class="studies-item">
                  <div class="studies-single">
                     <img src="<?php echo esc_url( $slide_url ); ?>" alt="<?php echo esc_attr( $slide_alt ); ?>">
                  </div>
                  <div class="overlay-text">
                      <div class="text-inner">
                          <p class="sub"><?php echo esc_html( $project_subtitle ) ?></p>
                          <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $project_title ) ?></a></h3>
                      </div>
                  </div>  
              </div>
          </div>
         <?php endwhile;
          wp_reset_postdata(); ?>
          </div>
        </div>
      </div>
    </div>
     <?php endif;
    return ob_get_clean();
  }
}
add_shortcode( 'project_filter', 'barrister_project_filter_function' );
