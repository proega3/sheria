<?php
add_action('init', 'barristar_service_kc_map', 99 );
function barristar_service_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'brstr_service' => array(
	                'name' => esc_html__('Barristar Service','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-box-alert',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                   array(
                    'name' => 'service_style',
                    'label' => esc_html__('Service Style', 'barristar-core'),
                    'type' => 'select',
                    'options' => array(
                      'standard' => esc_html__( 'Standard', 'barristar-core' ),
                      'classic' => esc_html__( 'Classic', 'barristar-core' ),
                      'normal' => esc_html__( 'Normal', 'barristar-core' ),
                    ),
                  ),
                  array(
                      'name' => 'service_colunm',
                      'label' => esc_html__('Service Column','barristar-core'),
                      'type' => 'select',
                        'options' => array(
                          '' => esc_html__('Service Column Style','barristar-core'),
                          'two_col' => esc_html__('Two Colunm','barristar-core'),
                          'three_col' => esc_html__('Three Colunm','barristar-core'),
                          'four_col' => esc_html__('Four Colunm','barristar-core'),
                        ),
                      'admin_label' => true,
                    ),
                    array(
                        'name' => 'service_limit',
                        'label' => esc_html__( 'Service Limit','barristar-core'),
                        'type' => 'text',
                        'description' => esc_html__('Write Service Limit ', 'barristar-core'),
                      ),
                    array(
                        'name' => 'service_order',
                        'label' => esc_html__( 'Service Orderby','barristar-core'),
                        'type' => 'select',
                          'options' => array(
                            '' => esc_html__('Service Order','barristar-core'),
                            'ASC' => esc_html__('Asending','barristar-core'),
                            'DESC' => esc_html__('Desending','barristar-core'),
                          ),
                        'admin_label' => true,
                        'description' => esc_html__('Select Service Orderby ', 'barristar-core'),
                      ),
                    array(
                        'name' => 'service_orderby',
                        'label' => esc_html__( 'Order By','barristar-core'),
                        'type' => 'select',
                          'options' => array(
                            'none' => esc_html__('None','barristar-core'),
                            'ID' => esc_html__('ID','barristar-core'),
                            'author' => esc_html__('Author','barristar-core'),
                            'title' => esc_html__('Title','barristar-core'),
                            'date' => esc_html__('Date','barristar-core'),
                          ),
                        'admin_label' => true,
                      ),
                    array(
                        'name' => 'read_more_text',
                        'label' => esc_html__( 'Read More Text','barristar-core'),
                        'type' => 'text',
                        'description' => esc_html__('Write Read More Text ', 'barristar-core'),
                      ),
                    array(
                        "name"  => "short_content",
                        "type"        =>'text',
                        "label"     =>esc_html__('Content Length', 'barristar-core'),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                      array(
                        'name' => 'desc_size',
                        'label' => esc_html__('Desctription Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 15,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for desctription such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'desc_color',
                        'label' => esc_html__('Desctription Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for desctription', 'barristar-core')
                      ),
                       array(
                        'name' => 'icon_color',
                        'label' => esc_html__('Service Icon Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Service Icon', 'barristar-core')
                      ),
                       array(
                        'name' => 'border_color',
                        'label' => esc_html__('Service Icon Border Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Border color for Service Icon', 'barristar-core')
                      ),
                       array(
                        'name' => 'service_bg',
                        'label' => esc_html__('Service Background Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Service Background', 'barristar-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon

	        )
	    ); // End add map

	} // End if

}
