<?php
/* ==========================================================
  Team Info
=========================================================== */
if ( !function_exists('barristar_service_function')) {
  function barristar_service_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';


    if ( $title_color || $title_size ) {
      $inline_style .= '.practice-area-'.$e_uniqid .'.practice-area .service-item .service-text h3 a {';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= '}';
    }

    if ( $desc_color || $desc_size ) {
      $inline_style .= '.practice-area-'.$e_uniqid .'.practice-area .service-item .service-text p {';
      $inline_style .= ( $desc_size ) ? 'font-size:'.barristar_core_check_px($desc_size) .';' : '';
      $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
      $inline_style .= '}';
    }

    if ( $icon_color ) {
      $inline_style .= '.practice-area-'.$e_uniqid .'.practice-area .service-item .service-icon i:before {';
      $inline_style .= ( $icon_color ) ? 'background-color:'. $icon_color .';' : '';
      $inline_style .= '}';
    }

    if ( $border_color ) {
      $inline_style .= '.practice-area-'.$e_uniqid .'.practice-area.practice-style-1 .service-icon, .practice-area-'.$e_uniqid .'.practice-area.practice-style-1 .service-text h3:before, .practice-area-'.$e_uniqid .'.practice-area.practice-area2 .service-item , .practice-area-'.$e_uniqid .'.practice-area.practice-area2 .service-icon:after {';
      $inline_style .= ( $border_color ) ? 'background-color:'. $border_color .';' : '';
      $inline_style .= ( $border_color ) ? 'border-color:'. $border_color .';' : '';
      $inline_style .= '}';
    }

    if ( $service_bg ) {
      $inline_style .= '.practice-area-'.$e_uniqid .'.practice-area .service-item {';
      $inline_style .= ( $service_bg ) ? 'background-color:'. $service_bg .';' : '';
      $inline_style .= '}';
    }
   
      // add inline style
      add_inline_style( $inline_style );
      $styled_class  = ' practice-area-'.$e_uniqid.' ';
      $short_content = $short_content ? $short_content : '5';

      if ( $service_style == 'standard' ) {
        $service_wrapper = ' practice-area2 practice-area-3 ';
      } elseif( $service_style == 'classic' ) {
        $service_wrapper = 'practice-style-1 ';
      } else {
        $service_wrapper = '';
      }

    ob_start();
    $args = array(
      'post_type' => 'service',
      'posts_per_page' => (int) $service_limit,
      'orderby' => $service_orderby,
      'order' => $service_order,
    );
    $barristar_service = new WP_Query( $args );
    if ($barristar_service->have_posts()) : ?>
    <div class="practice-area <?php echo esc_attr( $service_wrapper.$styled_class.' '.$class ) ?>">
      <div class="row clearfix">
      <?php
      while ($barristar_service->have_posts()) : $barristar_service->the_post();
      $service_options = get_post_meta( get_the_ID(), 'service_options', true );
      $service_title = isset($service_options['service_title']) ? $service_options['service_title'] : '';
      $service_icon = isset($service_options['service_icon']) ? $service_options['service_icon'] : '';
      $read_more_text = $read_more_text ? $read_more_text : esc_html__( 'Get Details', 'barristar' );

        if ( $service_style == 'standard' ) { ?>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="service-item">
              <div class="row">
                  <div class="service-icon">
                      <i class="<?php echo esc_attr( $service_icon ); ?>"></i>
                  </div>
                  <div class="service-text">
                      <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $service_title ); ?></a></h3>
                      <p><?php echo wp_trim_words( get_the_content(), $short_content ) ?></p>
                  </div>
              </div>
          </div>
        </div>
      <?php } else { ?>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                        <div class="service-icon">
                            <i class="<?php echo esc_attr( $service_icon ); ?>"></i>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                        <div class="service-text">
                            <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $service_title ); ?></a></h3>
                            <p><?php echo wp_trim_words( get_the_content(), $short_content ) ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php } 
         endwhile;
         wp_reset_postdata();
        ?>
      </div>
     </div>
    <?php endif;
    return ob_get_clean();
  }
}
add_shortcode( 'brstr_service', 'barristar_service_function' );
