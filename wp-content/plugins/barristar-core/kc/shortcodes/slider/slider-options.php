<?php
add_action('init', 'barristar_slider_kc_map', 99 );
function barristar_slider_kc_map() {

    if (function_exists('kc_add_map')){
        kc_add_map(
            array(
                'reg_slider' => array(
                    'name' =>  esc_html__('Barristar Slider','barristar-core'),
                    'description' => esc_html__('Display single icon', 'barristar-core'),
                    'icon' => 'cpicon kc-icon-icarousel',
                    'category' => BarristarLibrary::barristar_kc_cat_name(),
                    'params' => array(
                    'Content' => array(
                     array(
                          'name' => 'slide_style',
                          'label' => esc_html__('Slide Style', 'barristar-core'),
                          'type' => 'select',
                          'options' => array(
                            'style_one' => esc_html__( 'Style One', 'barristar-core' ),
                            'style_two' => esc_html__( 'Style Two', 'barristar-core' ),
                            'style_three' => esc_html__( 'Style Three', 'barristar-core' ),
                          ),
                          'admin_label' => true,
                      ),
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__(' Options', 'barristar-core'),
                        'name'          => 'slider_items',
                        'description'   => esc_html__( 'Slider Iteams Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__(' Add new Slider  Iteam', 'barristar-core')),
                        'params' => array(
                         array(
                              'name' => 'subtitle',
                              'label' => esc_html__( 'Top Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Slider Top Title ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'title',
                              'label' => esc_html__( 'Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Slider Title ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'button_text',
                              'label' => esc_html__( 'Button Text','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Slider Button Text ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'button_link',
                              'label' => esc_html__( 'Button Link','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Slider Button Link ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'slider_image',
                              'label' => esc_html__( 'Slider Image','barristar-core'),
                              'type' => 'attach_image',
                              'admin_label' => true,
                              'description' => esc_html__('Attach Slider Image ', 'barristar-core'),
                            ),
                        ),
                      ),
                    ),
                  'Style' => array(
                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                    array(
                        'name' => 'toptitle_size',
                        'label' => esc_html__('Slider Top Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 25,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Top title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                    array(
                        'name' => 'toptitle_color',
                        'label' => esc_html__('Slider Top Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Top title', 'barristar-core')
                      ),
                    array(
                        'name' => 'title_size',
                        'label' => esc_html__('Slider Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 25,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                    array(
                        'name' => 'title_color',
                        'label' => esc_html__('Slider Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                    array(
                        'name' => 'btn_bg_color',
                        'label' => esc_html__('Slider Button Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Slider Button Background', 'barristar-core')
                      ),
                    array(
                        'name' => 'btn_color',
                        'label' => esc_html__('Slider Button Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Slider Button', 'barristar-core')
                      ),
                   array(
                        'name' => 'btn_size',
                        'label' => esc_html__('Slider Button Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 25,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Button such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                    array(
                        'name' => 'btn_hover_bg',
                        'label' => esc_html__('Slider Button Hover Background','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Slider Button Hover Background', 'barristar-core')
                      ),
                    array(
                        'name' => 'btn_hover',
                        'label' => esc_html__('Slider Button Hover Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Hover color for Slider Button', 'barristar-core')
                      ),
                    )
                  )
                ),  // End of elemnt kc_icon

            )
        ); // End add map

    } // End if

}
