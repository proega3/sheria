<?php
if (!function_exists('barristar_slider_function')) {
    add_shortcode( 'reg_slider', 'barristar_slider_function' );
    function barristar_slider_function($atts, $content ){
      extract($atts);

      $e_uniqid       = uniqid();
      $inline_style   = '';
      if ( $toptitle_size || $toptitle_color ) {
        $inline_style .= '.hero-'.$e_uniqid .'.hero .hero-slider .slide-caption > p {';
        $inline_style .= ( $toptitle_size ) ? 'font-size:'.barristar_core_check_px($toptitle_size) .';' : '';
        $inline_style .= ( $toptitle_color ) ? 'color:'. $toptitle_color .';' : '';
        $inline_style .= '}';
      }
      if ( $title_size || $title_color ) {
        $inline_style .= '.hero-'.$e_uniqid .'.hero .hero-slider .slide-caption  > h2 {';
        $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
        $inline_style .= '}';
      }
      if ( $btn_bg_color ) {
        $inline_style .= '.page-wraper .hero-'.$e_uniqid .'.hero .slick-prev , .page-wraper .hero-'.$e_uniqid .'.hero .slick-next  {';
        $inline_style .= ( $btn_bg_color ) ? 'background-color:'. $btn_bg_color .';' : '';
        $inline_style .= '}';
      }
	  if ( $btn_color ) {
        $inline_style .= '.page-wraper .hero-'.$e_uniqid .'.hero .slick-prev:before , .page-wraper .hero-'.$e_uniqid .'.hero .slick-next:before  {';
        $inline_style .= ( $btn_color ) ? 'color:'. $btn_color .';' : '';
        $inline_style .= '}';
      }
	
      if ( $btn_hover_bg ) {
        $inline_style .= '.page-wraper .hero-'.$e_uniqid .'.hero .slick-prev:hover , .page-wraper .hero-'.$e_uniqid .'.hero .slick-next:hover {';
        $inline_style .= ( $btn_hover_bg ) ? 'background-color:'. $btn_hover_bg .';' : '';
		    $inline_style .= ( $btn_hover_bg ) ? 'border-color:'. $btn_hover_bg .';' : '';
        $inline_style .= '}';
      }
		
      if ( $slide_style == 'style_three' ) {
        $slide_class = ' hero-2 hero-style-2 hero-style-3';
      } elseif ( $slide_style == 'style_two' ) {
        $slide_class = ' hero-style-2';
      } else {
        $slide_class = ' hero-style-1';
      }
    $slider_items = ( $slider_items ) ? (array) $slider_items : array();
    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' hero-'.$e_uniqid.' ';

    ob_start(); ?>
    <div class="hero hero-slider-wrapper <?php echo esc_attr( $styled_class.$class.$slide_class ); ?>">
      <div class="hero-slider">
        <?php  foreach ( $slider_items as $key => $slider_item ) {
          $image_url = wp_get_attachment_url( $slider_item->slider_image );
          $image_alt = get_post_meta($slider_item->slider_image, '_wp_attachment_image_alt', true);
          if ( isset( $slider_item->slider_image ) && !empty( $slider_item->slider_image ) ) { ?>
          <div class="slide">
              <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" class="slider-bg">
              <div class="container">
                  <div class="row">
                      <div class="col col-lg-8 slide-caption">
                          <p><?php echo esc_html( $slider_item->subtitle ); ?></p>
                          <h2><?php echo wp_kses_post( $slider_item->title ); ?></h2>
                          <div class="btns">
                              <div class="btn-style btn-style-3">
                                <a href="<?php echo esc_url( $slider_item->button_link ); ?>"><?php echo esc_html( $slider_item->button_text ); ?></a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <?php } } ?>
        </div>
    </div>
   <?php return ob_get_clean();
  }
}