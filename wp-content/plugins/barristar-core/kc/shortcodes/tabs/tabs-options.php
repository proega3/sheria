<?php
add_action('init', 'barristar_tabs_kc_map', 99 );
function barristar_tabs_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'reg_tabs' => array(
	                'name' => esc_html__('Tabs','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-tabs',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__(' Options', 'barristar-core'),
                        'name'          => 'tabs_items',
                        'description'   => esc_html__( 'Items Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__(' Add new Accordion', 'barristar-core')),
                        'params' => array(
                        array(
                            'name' => 'tab_title',
                            'label' => esc_html__( 'Tabs Title','barristar-core'),
                            'type' => 'text',
                            'admin_label' => true,
                            'description' => esc_html__('Write Tabs Title Here', 'barristar-core'),
                          ),
                        array(
                            'name' => 'tab_desc',
                            'label' => esc_html__( 'Tabs Description','barristar-core'),
                            'type' => 'editor',
                            'admin_label' => true,
                            'description' => esc_html__('Write Tabs Description Here', 'barristar-core'),
                          ),
                          array(
                            'name' => 'active_tabs',
                            'type' => 'toggle',
                            'label' => esc_html__( 'Active Tab', 'barristar-core' ),
                            'description' => esc_html__('Please Turn On to active Accordion, Please Only One Accordion turn on, Otherwise tab stop working', 'barristar-core'),
                          ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                     array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Text Size', 'barristar-plugin'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 10,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                       ),
                     array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for title', 'barristar-core')
                     ),
                    array(
                        'name' => 'desc_color',
                        'label' => esc_html__('Accordion Description Text Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set color for Accordion Description Text', 'barristar-core')
                      ),
                     array(
                        'name' => 'title_active',
                        'label' => esc_html__('Title Background Active Color','barristar-core'),
                        'type' => 'color_picker',
                        'admin_label' => true,
                        'description' => esc_html__('Set Background active color for title', 'barristar-core')
                     ),
		                )
	                )
	            ),  // End of elemnt kc_icon
	        )
	    ); // End add map
	} // End if
}
