<?php
/* ==========================================================
  Workflow Info
=========================================================== */
if ( !function_exists('barristar_tabs_function')) {
  function barristar_tabs_function( $atts, $content = NULL ) {
    extract($atts);

    $e_uniqid       = uniqid();
    $inline_style   = '';
    if ( $title_color || $title_size  ) {
      $inline_style .= '.case-single-section-'. $e_uniqid .'.case-single-section .case-single-tab .nav a {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'.barristar_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $desc_color  ) {
      $inline_style .= '.case-single-section-'. $e_uniqid .'.case-single-section .tab-content .tab-pane p {';
      $inline_style .= ( $desc_color ) ? 'color:'. $desc_color .';' : '';
      $inline_style .= '}';
    }
    if ( $title_active ) {
      $inline_style .= '.case-single-section-'. $e_uniqid .'.case-single-section .case-single-tab .nav .active a {';
      $inline_style .= ( $title_active ) ? 'background-color:'. $title_active .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    add_inline_style( $inline_style );
    $styled_class  = ' case-single-section-'. $e_uniqid.' ';
    $tabs_items = ( $tabs_items ) ? (array) $tabs_items : array();

    $output = '<div class="case-single-section '.esc_attr( $styled_class.''.$class ).'">';
    $output .= '<div class="case-single-tab">';
    $output .= '<ul class="nav">';
    if ( $tabs_items ) {
       $id = 1;
      foreach ( $tabs_items as $key => $tabs_item ) {
         $id++;
        if ( $tabs_item->active_tabs == 'yes') {
          $active_class = 'active in';
        } else {
          $active_class = '';
        }
    $output .= '<li class="'.esc_attr( $active_class ).'">';
    $output .= '<a href="#'.esc_attr( $id ).'" data-toggle="tab" aria-expanded="true">'.esc_attr( $tabs_item->tab_title ).'</a>';
    $output .= '</li>';
       }
    }
    $output .= '</ul>';
    $output .= '<div class="tab-content">';
     if ( $tabs_items ) {
       $id = 1;
      foreach ( $tabs_items as $key => $tabs_item ) {
         $id++;
        if ( $tabs_item->active_tabs == 'yes') {
          $active_class = 'active in';
        } else {
          $active_class = '';
        }
    $output .= '<div class="tab-pane fade '.esc_attr( $active_class ).' in" id="'.esc_attr( $id ).'">';
    $output .= wp_kses_post( $tabs_item->tab_desc );
    $output .= '</div>';
        }
    }
    $output .= '</div></div></div>';
     return $output;
  }
}
add_shortcode( 'reg_tabs', 'barristar_tabs_function' );
