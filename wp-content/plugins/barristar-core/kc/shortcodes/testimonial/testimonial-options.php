<?php
add_action('init', 'barristar_testimonial_kc_map', 99 );
function barristar_testimonial_kc_map() {

	if (function_exists('kc_add_map')){
	    kc_add_map(
	        array(
	            'reg_testimonial' => array(
	                'name' =>  esc_html__('Barristar Testimonial','barristar-core'),
	                'description' => esc_html__('Display single icon', 'barristar-core'),
	                'icon' => 'cpicon kc-icon-testi',
	                'category' => BarristarLibrary::barristar_kc_cat_name(),
	                'params' => array(
                  'Content' => array(
                    array(
                        'name' => 'image_url',
                        'type' => 'attach_image',
                        'label' => esc_html__('Set Testimonial Image ', 'barristar'),
                      ),
                    array(
                        'name' => 'title',
                        'label' => esc_html__('Testimonial Title','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write Testimonial  Title ', 'barristar-core'),
                      ),
                    array(
                        'name' => 'subtitle',
                        'label' => esc_html__('Testimonial Sub Title','barristar-core'),
                        'type' => 'text',
                        'admin_label' => true,
                        'description' => esc_html__('Write Testimonial  Title ', 'barristar-core'),
                      ),
                    array(
                        'type'          => 'group',
                        'label'         => esc_html__('Options', 'barristar-core'),
                        'name'          => 'testimonial_items',
                        'description'   => esc_html__('Lis Iteams Group Field', 'barristar-core' ),
                        'options'       => array('add_text' => esc_html__('Add new Lis Iteam', 'barristar-core')),
                        'params' => array(
                          array(
                              'name' => 'title',
                              'label' => esc_html__('Testimonial Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Testimonial  Title ', 'barristar-core'),
                            ),
                          array(
                              'name' => 'subtitle',
                              'label' => esc_html__('Testimonial Sub Title','barristar-core'),
                              'type' => 'text',
                              'admin_label' => true,
                              'description' => esc_html__('Write Testimonial  Title ', 'barristar-core'),
                            ),
                            array(
                              'name' => 'testi_image',
                              'label' => esc_html__('Testimonial Image','barristar-core'),
                              'type' => 'attach_image',
                              'description' => esc_html__('Chose Testimonial  Image ', 'barristar-core'),
                            ),
                            array(
                              'name' => 'desc',
                              'label' => esc_html__('Description','barristar-core'),
                              'type' => 'textarea',
                              'admin_label' => false,
                              'description' => esc_html__('Write Testimonial  Description ', 'barristar-core'),
                            ),
                        ),
                      ),
                    ),
                    'Style' => array(
	                    array(
                        'name' => 'class',
                        'label' => esc_html__('Extra Class','barristar-core'),
                        'type' => 'text',
                        'description' => esc_html__('Enter Extra Class for Titlte ..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_size',
                        'label' => esc_html__('Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for title such as: 15px, 1em ..etc..', 'barristar-core')
                      ),
                      array(
                        'name' => 'title_color',
                        'label' => esc_html__('Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'description' => esc_html__('Set color for title', 'barristar-core')
                      ),
                      array(
                        'name' => 'subtitle_size',
                        'label' => esc_html__('Sub Title Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Sub title such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'subtitle_color',
                        'label' => esc_html__('Sub Title Color','barristar-core'),
                        'type' => 'color_picker',
                        'description' => esc_html__('Set color for Sub title', 'barristar-core')
	                    ),
	                    array(
                        'name' => 'desc_size',
                        'label' => esc_html__('description Size','barristar-core'),
                        'type' => 'number_slider',
                        'options' => array(
                          'min' => 16,
                          'max' => 60,
                          'unit' => 'px',
                          'show_input' => true
                        ),
                        'description' => esc_html__('Enter font-size for Sub Testimonial such as: 15px, 1em ..etc..', 'barristar-core')
	                    ),
                      array(
                        'name' => 'desc_color',
                        'label' => esc_html__('description Color','barristar-core'),
                        'type' => 'color_picker',
                        'description' => esc_html__('Set color for Testimonial description', 'barristar-core')
                      ),
		                )
	                )
	            ),  // End of elemnt kc_icon

	        )
	    ); // End add map

	} // End if

}
