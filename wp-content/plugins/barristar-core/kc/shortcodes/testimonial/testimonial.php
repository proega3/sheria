<?php
/* ==========================================================
  Testimonials
=========================================================== */
if ( !function_exists('barristar_testimonial_function')) {
  function barristar_testimonial_function( $atts, $content = NULL ) {
    extract($atts);

    if ( $title_size || $title_color ) {
      $title_style = ' style="';
      $title_style .= $title_size ? 'font-size: '. barristar_plugin_check_px($title_size) .';': '';
      $title_style .= $title_color ? 'color: '.$title_color : '';
      $title_style .= '"';
    } else {
      $title_style = '';
    }
    if ( $subtitle_size || $subtitle_color ) {
      $subtitle_style = ' style="';
      $subtitle_style .= $subtitle_size ? 'font-size: '. barristar_plugin_check_px($subtitle_size) .';': '';
      $subtitle_style .= $subtitle_color ? 'color: '.$subtitle_color : '';
      $subtitle_style .= '"';
    } else {
      $subtitle_style = '';
    }
    if ( $subtitle_color ) {
      $image_style = ' style="';
      $image_style .= $subtitle_color ? 'border-color: '.$subtitle_color : '';
      $image_style .= '"';
    } else {
      $image_style = '';
    }

    if ( $desc_size || $desc_color ) {
      $desc_style = ' style="';
      $desc_style .= $desc_size ? 'font-size: '. barristar_plugin_check_px($desc_size) .';': '';
      $desc_style .= $desc_color ? 'color: '.$desc_color : '';
      $desc_style .= '"';
    } else {
      $desc_style = '';
    }

     $image_url = wp_get_attachment_url( $image_url );
     $image_alt = get_post_meta( $image_url, '_wp_attachment_image_alt', true);
 
     ob_start(); ?>
    <div class="testimonial-area <?php echo esc_attr( $class ); ?>">
      <div class="row">
          <div class="col-lg-4 col-md-6">
              <div class="testimonial-img">
                 <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
              </div>
          </div>
          <div class="col-lg-7 col-lg-offset-1 col-md-6">
              <div class="testimonial-text">
                 <div class="title">
                     <span><?php echo esc_html( $subtitle ); ?></span>
                     <h2><?php echo esc_html( $title ); ?></h2>
                 </div>
                 <div class="testimonial-slide owl-carousel">
                 <?php foreach ( $testimonial_items as $key => $testimonial_item ) {
                  $image_url = wp_get_attachment_url( $testimonial_item->testi_image );
                  $image_alt = get_post_meta($testimonial_item->testi_image, '_wp_attachment_image_alt', true);
                  if ($testimonial_item->testi_image) { ?>
                    <div class="slide-item">
                       <?php  if ( $testimonial_item->desc ) { ?>
                          <p <?php echo wp_kses_post( $desc_style ); ?> ><?php echo esc_html( $testimonial_item->desc ) ?></p>
                       <?php } 
                       if ( $testimonial_item->testi_image ) { ?>
                       <div class="thumb-img">
                        <img <?php echo wp_kses_post( $image_style ); ?> src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ) ?>">
                       </div>
                       <?php } 
                        if ( $testimonial_item->title || $testimonial_item->subtitle ) { ?>
                       <div class="img-content">
                          <h4 <?php echo wp_kses_post( $title_style ); ?>><?php echo esc_html( $testimonial_item->title ); ?></h4>
                          <span <?php echo wp_kses_post( $subtitle_style ); ?>><?php echo esc_html( $testimonial_item->subtitle ); ?></span>
                       </div>
                       <?php } ?>
                  </div>
                  <?php } } ?>
                 </div>
              </div>
          </div>
      </div>
    </div> 
    <?php return ob_get_clean();
  }
}
add_shortcode( 'reg_testimonial', 'barristar_testimonial_function' );
