<?php
if (!function_exists( 'barristar_timeline_kc_map' ) ) {
  add_action('init', 'barristar_timeline_kc_map', 99 );
  function barristar_timeline_kc_map() {
    kc_add_map(
      array(
        'section_timeline' => array(
          'name' => esc_html__('Barristar Time line', 'barristar-core'),
          'description' => esc_html__( 'Barristar Timeline Styles', 'barristar-core' ),
          'category' => BarristarLibrary::barristar_kc_cat_name(),
          'icon' => 'cpicon kc-icon-progress',
          'title' => esc_html__('Time line Settings', 'barristar-core'),
          'is_container' => true,
          'priority'  => 130,
          'params' => array(
            esc_html__( 'General', 'barristar-core' ) => array(
              array(
                'type'          => 'group',
                'label'         => esc_html__('Options', 'barristar-core'),
                'name'          => 'timeline_items',
                'description'   => esc_html__('Lis Items Group Field', 'barristar-core' ),
                'options'       => array('add_text' => esc_html__('Add new Lis Iteam', 'barristar-core')),
                'params' => array(
                    array(
                      'name' => 'time_date',
                      'label' => esc_html__('Time line Date', 'barristar-core'),
                      'type' => 'text',
                      'admin_label' => true
                      ),
                    array(
                      'name' => 'title',
                      'label' => esc_html__('Sub Title', 'barristar-core'),
                      'type' => 'text',
                      'admin_label' => true,
                    ),
                    array(
                      'name' => 'desc',
                      'label' => esc_html__('Time line Description', 'barristar-core'),
                      'type' => 'textarea',
                      'admin_label' => true,
                    ),
                ),
              ),
              array(
                'name' => 'class',
                'label' => esc_html__(' Extra class name', 'barristar-core'),
                'type' => 'text',
                'description' => esc_html__(' ', 'barristar-core')
              )
            ),
            esc_html__( 'Style', 'barristar-core' ) => array(
            array(
                'name' => 'date_color',
                'label' => esc_html__(' Date Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'date_size',
                'label' => esc_html__(' Date Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
              array(
                'name' => 'title_color',
                'label' => esc_html__(' Title Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'title_size',
                'label' => esc_html__(' Title Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
             array(
                'name' => 'desc_color',
                'label' => esc_html__('Description Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'desc_size',
                'label' => esc_html__('Description Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
              array(
                'name' => 'border_color',
                'label' => esc_html__(' Border Color', 'barristar-core'),
                'type' => 'color_picker',
              ),
            ),
          )
        ),
      )
    ); // End add map
  }
} // End if