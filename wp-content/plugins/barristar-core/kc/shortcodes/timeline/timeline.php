<?php
/* ==========================================================
  Accordion Info
=========================================================== */
if ( !function_exists('barristar_timeline_shortcode_function')) {
  function barristar_timeline_shortcode_function( $atts, $content = true ) {
  	extract($atts);
  	$uniqid = uniqid( '-', false);
  	$inline_style = '';
    if ( $date_color || $date_size ) {
      $inline_style .= '.about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid span {';
      $inline_style .= $date_color ? 'color: '.$date_color.'; ' : '';
      $inline_style .= $date_size ? 'font-size: '.$date_size.'; ' : '';
      $inline_style .= '}';
    }
    if ( $title_color || $title_size ) {
      $inline_style .= '.about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid h3 {';
      $inline_style .= $title_color ? 'color: '.$title_color.'; ' : '';
      $inline_style .= $title_size ? 'font-size: '.$title_size.'; ' : '';
      $inline_style .= '}';
    }
    if ( $desc_color || $desc_size ) {
      $inline_style .= '.about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid p {';
      $inline_style .= $desc_color ? 'color: '.$desc_color.'; ' : '';
      $inline_style .= $desc_size ? 'font-size: '.$desc_size.'; ' : '';
      $inline_style .= '}';
    }
 	  if ( $border_color ) {
      $inline_style .= '.about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid:before, .about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid:after, .about-pg-history'.$uniqid.'.about-pg-history .history-grids .grid h3:before {';
      $inline_style .= $border_color ? 'background-color: '.$border_color.'; ' : '';
      $inline_style .= '}';
    }


  	// integrate css
  	add_inline_style( $inline_style );
  	$inline_class = ' about-pg-history'.$uniqid;
    $timeline_items = ( $timeline_items ) ? (array) $timeline_items : array();
    ob_start(); ?>
    <div class="row about-pg-history <?php echo esc_attr( $inline_class.$class ); ?>">
      <div class="history-grids clearfix">
       <?php if ( $timeline_items ) {
          foreach ( $timeline_items as $key => $timeline_item ) { ?>
          <div class="grid">
              <span class="date"><?php echo esc_html( $timeline_item->time_date ); ?></span>
              <h3><?php echo esc_html( $timeline_item->title ); ?></h3>
              <p><?php echo esc_html( $timeline_item->desc ); ?></p>
          </div>
        <?php } } ?>
      </div>
    </div>
   <?php return ob_get_clean();
  }
}
add_shortcode( 'section_timeline', 'barristar_timeline_shortcode_function' );