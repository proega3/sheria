<?php
if (!function_exists( 'barristar_section_title_kc_map' ) ) {
  add_action('init', 'barristar_section_title_kc_map', 99 );
  function barristar_section_title_kc_map() {
    kc_add_map(
      array(
        'section_title' => array(
          'name' => esc_html__('Barristar Title', 'barristar-core'),
          'description' => esc_html__( 'Barristar Title Styles', 'barristar-core' ),
          'category' => BarristarLibrary::barristar_kc_cat_name(),
          'icon' => 'cpicon kc-icon-title',
          'title' => esc_html__('Title Settings', 'barristar-core'),
          'is_container' => true,
          'priority'  => 130,
          'params' => array(
            esc_html__( 'General', 'barristar-core' ) => array(
              array(
                'name' => 'title_style',
                'label' => esc_html__('Title Style', 'barristar-core'),
                'type' => 'select',
                'options' => array(
                  'left' => esc_html__( 'Left', 'barristar-core' ),
                  'center' => esc_html__( 'Center', 'barristar-core' ),
                ),
                'admin_label' => true,
              ),
              array(
                'name' => 'toptitle',
                'label' => esc_html__('Title', 'barristar-core'),
                'type' => 'text',
                'admin_label' => true
              ),
              array(
                'name' => 'title',
                'label' => esc_html__('Sub Title', 'barristar-core'),
                'type' => 'text',
                'admin_label' => true,
              ),
              array(
                'name' => 'desc',
                'label' => esc_html__('Title Description', 'barristar-core'),
                'type' => 'textarea',
                'admin_label' => true,
              ),
              array(
                'name' => 'class',
                'label' => esc_html__(' Extra class name', 'barristar-core'),
                'type' => 'text',
                'description' => esc_html__(' ', 'barristar-core')
              )
            ),
            esc_html__( 'Style', 'barristar-core' ) => array(
            array(
                'name' => 'title_color',
                'label' => esc_html__(' Title Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'title_size',
                'label' => esc_html__(' Title Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
             array(
                'name' => 'toptitle_color',
                'label' => esc_html__('Top Title Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'toptitle_size',
                'label' => esc_html__('Top Title Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),

            array(
                'name' => 'use_underline',
                'label' => esc_html__('Hide Underline', 'barristar-core'),
                'type' => 'toggle',
                'value' => 'yes',
                'description' => esc_html__('Turn on if you want to hide underline', 'barristar-core')
              ),
              array(
                'name' => 'line_color',
                'label' => esc_html__(' Underline Color', 'barristar-core'),
                'type' => 'color_picker',
                'relation' => array(
                  'parent' => 'icon_option',
                  'show_when' => 'yes'
                )
              ),
            ),
          )
        ),
      )
    ); // End add map
  }
} // End if