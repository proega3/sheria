<?php
/* ==========================================================
  Accordion Info
=========================================================== */
if ( !function_exists('barristar_title_shortcode_function')) {
  function barristar_title_shortcode_function( $atts, $content = true ) {
  	extract($atts);
  	$uniqid = uniqid( '-', false);
  	$inline_style = '';
  	if ( $title_color || $title_size ) {
      $inline_style .= '.section-title'.$uniqid.'.section-title h2{';
      $inline_style .= $title_color ? 'color: '.$title_color.'; ' : '';
      $inline_style .= $title_size ? 'font-size: '.$title_size.'; ' : '';
      $inline_style .= '}';
    }
  if ( $toptitle_color || $toptitle_size ) {
  		$inline_style .= '.section-title'.$uniqid.'.section-title span {';
  		$inline_style .= $toptitle_color ? 'color: '.$toptitle_color.'; ' : '';
  		$inline_style .= $toptitle_size ? 'font-size: '.$toptitle_size.'; ' : '';
  		$inline_style .= '}';
  	}

    if ($use_underline != 'yes') {
      $inline_style .= '.section-title'.$uniqid.'.section-title h2:after {';
      $inline_style .= 'content: none;';
      $inline_style .= '}';
    } else {
      if ( $line_color ) {
        $inline_style .= '.section-title'.$uniqid.'.section-title h2:after {';
        $inline_style .= $line_color? 'background-color: '.$line_color.'; ' : '';
        $inline_style .= '}';
      }
    }

  	// integrate css
  	add_inline_style( $inline_style );
  	$inline_class = ' section-title'.$uniqid;
    if ( $title_style == 'center') {
      $title_class = ' text-center';
    } else {
      $title_class = ' ';
    }
    ob_start(); ?>
  <div class="section-title row <?php echo esc_attr( $class.$inline_class.$title_class ); ?>">
      <span><?php echo esc_html( $toptitle ); ?></span>
      <h2><?php echo esc_html( $title ); ?></h2>
      <?php if ($use_underline != 'yes') {  ?>
      <span class="title-line clearfix"></span>
      <?php }
      if ( $desc ) { ?>
        <p><?php echo esc_html( $desc ); ?></p>
      <?php } ?>
  </div>
   <?php return ob_get_clean();
  }
}
add_shortcode( 'section_title', 'barristar_title_shortcode_function' );