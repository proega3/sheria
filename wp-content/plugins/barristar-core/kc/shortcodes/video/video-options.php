<?php
add_action('init', 'barristar_video_kc_map', 99 );
function barristar_video_kc_map() {
  if (function_exists('kc_add_map')){
      kc_add_map(
        array(
        'regvideo' => array(
          'name' => esc_html__('Video', 'barristar-core'),
          'description' => esc_html__( 'Video', 'barristar-core' ),
          'category' => BarristarLibrary::barristar_kc_cat_name(),
          'title' => esc_html__('Carousel Settings', 'barristar-core'),
          'is_container' => true,
          'icon' => 'cpicon kc-icon-play',
          'params' => array(
            esc_html__( 'General', 'barristar-core' ) => array(
            array(
                'name' => 'video_style',
                'label' => esc_html__('Video Style', 'barristar-core'),
                'type' => 'select',
                'options' => array(
                  'standard' => esc_html__( 'Standard', 'barristar-core' ),
                  'classic' => esc_html__( 'Classic', 'barristar-core' ),
                ),
              ),
              array(
                  'name' => 'video_image',
                  'type' => 'attach_image',
                  'label' => esc_html__('Video Image', 'barristar'),
                  'admin_label' => true,
                ),
                array(
                  'name' => 'video_icon',
                  'type' => 'icon_picker',
                  'label' => esc_html__('Video Icon', 'barristar'),
                ),
                array(
                  'name' => 'link',
                  'type' => 'text',
                  'label' => esc_html__('Video Link', 'barristar'),
                ),
              array(
                'name' => 'class',
                'label' => esc_html__('Extra Class', 'barristar-core'),
                'type' => 'text',
              )
            ),
           esc_html__( 'Style', 'barristar-core' ) => array(
            array(
                'name' => 'bg_color',
                'label' => esc_html__('Background Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
             array(
                'name' => 'icon_color',
                'label' => esc_html__('Icon Color', 'barristar-core'),
                'type' => 'color_picker'
              ),
              array(
                'name' => 'icon_size',
                'label' => esc_html__('Icon Size', 'barristar-core'),
                'type' => 'number_slider',
                'options' => array(
                  'min' => 15,
                  'max' => 60,
                  'unit' => 'px',
                  'show_input' => true
                ),
              ),
            array(
              'name' => 'icon_border',
              'label' => esc_html__('Icon Border Color', 'barristar-core'),
              'type' => 'color_picker'
            ),
            ),
          ),
        ),
      )
    ); // End add map
  }
} // End if