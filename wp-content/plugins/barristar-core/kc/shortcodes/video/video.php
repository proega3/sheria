<?php
if (!function_exists('barristar_video_shortcode_function')) {
	function barristar_video_shortcode_function( $atts, $content = null ){
		extract($atts);
  extract($atts);
    $uniqid = uniqid( '-', false);
    $inline_style = '';
    if ( $icon_color || $icon_size ) {
      $inline_style .= '.video-section'.$uniqid.'.video-section .video-holder a i:before {';
      $inline_style .= $icon_color ? 'color: '.$icon_color.'; ' : '';
      $inline_style .= $icon_size ? 'font-size: '.$icon_size.'; ' : '';
      $inline_style .= '}';
    }
    if ( $icon_border  ) {
      $inline_style .= '.video-section'.$uniqid.'.video-section .video-holder a {';
      $inline_style .= $icon_border ? 'border-color: '.$icon_border.'; ' : '';
      $inline_style .= '}';
    }
    if ( $bg_color ) {
      $inline_style .= '.video-section'.$uniqid.'.video-section .video-holder:before {';
      $inline_style .= $bg_color ? 'border-color: '.$bg_color.'; ' : '';
      $inline_style .= '}';
    }

    // integrate css
    add_inline_style( $inline_style );
    $inline_class = ' video-section'.$uniqid;
    $image_url = wp_get_attachment_url( $video_image );
    $image_alt = get_post_meta( $video_image, '_wp_attachment_image_alt', true);
    if ( $video_style == 'standard' ) {
      $video_class = 'about-section ';
    } else {
      $video_class = 'services-section-s2 ';
    }
		ob_start(); ?>

  <div class="video-section <?php echo esc_attr( $video_class.$inline_class.$class ); ?>">
    <div class="video-holder">
        <div class="img-holder">
            <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
        </div>
        <a href="<?php echo esc_url( $link ); ?>" class="video-btn" data-type="iframe">
          <i class="<?php echo esc_attr( $video_icon ); ?>"></i>
        </a>
    </div>
</div>
	<?php	return ob_get_clean();
	}
}
add_shortcode( 'regvideo', 'barristar_video_shortcode_function' );