<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



function pdfp_block_type($myBlockName, $pdfp_BlockOption = array()) {
	register_block_type(
		'pdfp-kit/' . $myBlockName,
		array_merge(
			array(
				'editor_script' => 'pdfp-kit-editor-script',
				'editor_style' => 'pdfp-kit-editor-style',
				'script' => 'pdfp-kit-front-script',
				'style' => 'pdfp-kit-front-style'
			),
			$pdfp_BlockOption
		)
	);
}

function pdfp_blocks_script() {
	wp_register_script(
		'pdfp-kit-editor-script',
		plugins_url('dist/js/editor-script.js', __FILE__),
		array(
			'wp-blocks',
			'wp-i18n',
			'wp-element',
			'wp-editor',
			'wp-components',
			'wp-compose',
			'wp-data',
			'wp-autop',
		)
	);	
	pdfp_block_type('kahf-banner-k27f', array(
		'render_callback' => 'pdfp_block_custom_post_fun',
		'attributes' => array(
			'postName' => array(	
				'type' => 'string',
				'source' => 'html',
			),
		)
	));
	
}
add_action('init', 'pdfp_blocks_script');



function pdfp_block_custom_post_fun ( $attributes, $content ) {
	
	return wpautop( $content );
}