=== PDF Poster - PDF Embedder Plugin for Wordpress ===
Tags: PDF Embedder, embed pdf, pdf viewer, pdf, pdf plugin, document, google
Donate link: https://gum.co/wpdonate
Requires at least: 3.0
Tested up to: 5.3.2
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Embed/post Pdf files in your wordpress website/blog. Very easy to use, user friendly & lite weight plugin. 

== Description ==

Pdf poster is a awesome plugin which you can use to display /embed a pdf file in your wordpress website/blog easily. 


Wanna see how it works? [Click Here To see demo ](http://1infosoft.com/products/pdf-poster-demo/ "See demo") 

= How to use  =
- After install you can see a sidebar menu in the dashboare called "PDF Poster"
- Add one or more Document from there. 
- You will get Shortcode for every PDF In The Editor Screen and PDF Lists.
- Copy Shortcode 
- Past the shortcode in post, page, widget areas To publish them. if you want to publish a player in template file use <?php echo do_shortcode('PLAYER_SHORTCODE') ?>
- Enjoy !

= Gutenberg Block =
- This plugin add a Gutenberg Block Called PDF Poster Under Common Category 
- Go to your WordPress Admin interface and open a post or page editor.
- Click the plus button in the top left corner or in the body of the post/page.
- Search or See in Common Block Category and select PDF Poster.
- Click the Icon to add it.
- Select A PDF  
- Publish and Enjoy ! 


= Pro version features = 

- No ads.

- Control over download button.

- Control Over View full screen Button.

- Prevent Right Click to protect your file content.

- Set Jump to page number to show a specific page of pdf file.

- Added Shortcode generator in post / page editor.

- Improved performance. 

Get The PRO here:  [BUY The PDF Poster PRO ](https://gum.co/zUvK "BUY NOW")  

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `plugin-directory` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use shortcode in page, post or in widgets.
4. If you want news ticker in your theme php, Place `<?php echo do_shortcode('YOUR_SHORTCODE'); ?>` in your templates


== Frequently Asked Questions ==

= How much Pdf file i can embed using the plugin? =

There are no limitation ! you can embed unlimited pdf file.

= Is the plugin contain any ad? =

No the plugin does not show any ad.

= Can I embed a video using the plugin ? =

No, you only can embed pdf file.



== Screenshots ==

1. Sidebar menu
2. Adding a pdf file in dashboard area.
3. Output / Frontend preview
4. Full Screen preview 

== Changelog ==

= 1.0 =
* Initial Release

= 1.1 =
* fix an issue with pdf positions

= 1.2 =
* removed an ad.
* fix issue which causes centralize the content.
* Improved performance. 

= 1.3 =
* Fix an issue  
= 1.4 =
* Add support for Gutenberg Block